# Extra calculational tools for galaxy formation pipeline project
# (C) 2019-2020 Marius Peper, Boud Roukema GPL-3+

# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import numpy as np

class calculations:

    def move_coordinates(self, x, y, z, centre, box_length):
        """Take into account that the simulation is run for a 3-torus topology,
        i.e. R^3/ZxZxZ = S^1 x S^1 x S^1.
        The voids may cross the faces of the default choice of fundamental
        domain. The fundamental domain is often popularly called the 'box'.

        If only the positions of particles and void centres in one
        position of the fundamental domain are used, then some particles
        that are in reality close to a void centre will be interpreted as
        being far from it. This will lead to missing some particles that
        are 'in' voids.
        The strategy here is the following:

        (1) Given the default fundamental domain F_0 with the ranges

        0 \le (x | y | z) \le box_length

        and a void v at position (x_v, y_v, z_v);
        and a particle p at position (x_p, y_p, z_p);

        we want to use the topological image of p that is closest in
        the covering space to v. This effectively means that we want
        to choose the coordinates of the topological image of point p
        that is located, within the covering space, in the copy of
        the fundamental domain F_v that is centred on the void.

        (2) We choose to output the coordinates within F_v as offsets
        from (x_v, y_v, z_v).

        Calculate the distance of the particle from the void centre by
        a (spatial) geodesic within F_0:

        Parameters
        ----------
        x, y, z: floats (numpy arrays)
           Positions that have to be changed.
        centre: (numpy array, 3, type=float)
           The coordinates of the centre. The positions get moved so that the
           centre is at (0,0,0) afterwards.
        box_length: float
           The size of the simlated box.

        returns
        -------
        out : np.array(array, 3, dtype=float)
        The moved positions according to the given centre.

        """

        x_mov=x-centre[0]
        y_mov=y-centre[1]
        z_mov=z-centre[2]

        half_box = box_length / 2.0

        if hasattr(x,"__len__"):
            indices_over_half = (np.array(np.where(abs(x_mov) > half_box)))[0]
            x_mov[indices_over_half] -= (box_length *
                                         np.sign(x_mov[indices_over_half]))
            indices_over_half = (np.array(np.where(abs(y_mov) > half_box)))[0]
            y_mov[indices_over_half] -= (box_length *
                                         np.sign(y_mov[indices_over_half]))
            indices_over_half = (np.array(np.where(abs(z_mov) > half_box)))[0]
            z_mov[indices_over_half] -= (box_length *
                                         np.sign(z_mov[indices_over_half]))
            out=np.empty((len(x_mov),3))
            out[:,0]=x_mov
            out[:,1]=y_mov
            out[:,2]=z_mov
            return out
        else:
            # Individual particle: if the particle is not in F_v, then shift it to F_v.
            if (abs(x_mov) > half_box):
                x_mov = x_mov - box_length * np.sign(x_mov)
            if (abs(y_mov) > half_box):
                y_mov = y_mov - box_length * np.sign(y_mov)
            if (abs(z_mov) > half_box):
                z_mov = z_mov - box_length * np.sign(z_mov)

            out=np.empty((3))
            out[0]=x_mov
            out[1]=y_mov
            out[2]=z_mov
            return out


    def gradient(self, p, linInter, void_radius):
        """
        This function calculate the gradient of the potential at a given
        position in a void using scipy's LinearNDInterpolator and numpy's gradient.

        Parameters
        ----------
        p: (array, 3, type=float)
           The coordinates in a void at which the force will be calculated.
        linInter:
           The interpolated potential.
        void_radius: float
           The effective void radius.

        returns
        -------
        grad : (array, 3, type=float)
           The moved positions according to the given centre.
        """

        # the aim is to calculate the 3d grad at point p

        x = np.linspace((p[0]-void_radius/10), (p[0]+void_radius/10), 21)
        y = np.linspace((p[1]-void_radius/10), (p[1]+void_radius/10), 21)
        z = np.linspace(p[2]-void_radius/10, p[2]+void_radius/10, 21)
        x, y, z = np.meshgrid(x, y, z, indexing = 'ij', sparse = False)

        #print("time before interpolating on the mesh ", time.clock())
        g = linInter(x,y,z)
        #print("time in between interpolation and calc force",time.clock())
        f_x,f_y,f_z=np.gradient(g,void_radius/10)
        #print("time after calculating the force ",time.clock())
        grad = [-f_x[10,10,10]/3.086e+19, -f_y[10,10,10]/3.086e+19, -f_z[10,10,10]/3.086e+19]
        g=None
        # we provide our potential to be in km**2/s**2 but our derivative is done in 1/Mpc
        return grad



if __name__ == '__main__':
    # benchmarking of move_coordinates

    calc=calculations()

    box_length = 10.5

    N = 30000000
    x = np.arange(N) % box_length
    y = np.arange(N) % box_length
    z = np.arange(N) % box_length
    centre = np.array([3.2, 5.7, 6.8])

    moved_array = calc.move_coordinates(x,y,z, centre, box_length)
