#!/usr/bin/env python

# allresults_LSBG.py - Analyse the outputs from sage and revolver.
# This routine calculates key quantities of galaxies, i.e. the
# median infall rate and the galaxy size. It will also estimate
# which galaxies are in a void and compare the void population
# with the non-void population. Several figures will be created
# to visualise the results.
# (C) 2019-2020 Marius Peper, Boud Roukema GPL-3+

# This script is a heavily modified version of allresults.py
# from sage. It adds the whole analysis of void galaxies and
# compares them to non-void galaxies. The plot routines of
# allresults.py are only modified slightly to plot the LSBG
# population as well.
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.


# system level
import sys
print("allresults_LSBGs: sys.path = ",sys.path)
import os
from os.path import getsize as getFileSize
import ctypes
import glob
from operator import eq
import warnings

# standard science packages
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
matplotlib.use('Agg')
from itertools import product, combinations
#import h5py as h5
import numpy as np
import matplotlib.pyplot as plt

macOS_Darwin = False
if os.environ.get('LD_LIBRARY_PATH'):
    print("os.environ.get('LD_LIBRARY_PATH') was set as an evironment variable and passed to python.")
    print("No macOS/Darwin hack is needed: os.environ.get('LD_LIBRARY_PATH') = ",
          os.environ.get('LD_LIBRARY_PATH'))
else:
    print("os.environ.get('LD_LIBRARY_PATH') was either *not* set as an evironment variable or *not* passed to python.")
    print("os.environ.get('LD_LIBRARY_PATH_PYTHON') = ",
          os.environ.get('LD_LIBRARY_PATH_PYTHON'))
    os.environ['LD_LIBRARY_PATH'] = os.environ.get('LD_LIBRARY_PATH_PYTHON')
    io_gadget_with_path = os.environ.get('IO_GADGET_WITH_PATH')
    macOS_Darwin = True
    print("After macOS/Darwin hack: os.environ.get('LD_LIBRARY_PATH') = ",
          os.environ.get('LD_LIBRARY_PATH'),
          " io_gadget_with_path = ",io_gadget_with_path)

if(macOS_Darwin):
    read_gadget = ctypes.CDLL(io_gadget_with_path)
else:
    read_gadget = ctypes.CDLL("io_gadget.so")

from numpy.ctypeslib import ndpointer
#import matplotlib.pyplot as maplt

# TODO: do not use the old style 'random' unless numpy is too old;
# update parts of this file that rely on the unnamed 'random' module.
import random
from random import sample, seed, shuffle

# Use modern random number generator (rng) if possible.
from setuptools import version
if (version.pkg_resources.parse_version(np.__version__) >= version.pkg_resources.parse_version("1.17.0")):
    np_modern = True
    # as recommended at https://numpy.org/devdocs/reference/random/index.html
    from numpy.random import default_rng
else:
    np_modern = False

# elaphrocentre package
from exptest import infall_fit
from infall_stat import plot_histogram, median_stats
from extra_calculations import calculations
from plot_voidgals_properties import voidgals_props_read_data
from plot_voidgals_properties import voidgals_props_plot


# ================================================================================
# Basic variables
# ================================================================================

# Set up some basic attributes of the run

whichsimulation = 0
whichimf = 1        # 0=Slapeter; 1=Chabrier
dilute = 7500       # Number of galaxies to plot in scatter plots
sSFRcut = -11.0     # Divide quiescent from star forming galaxies (when plotmags=0)

#rad_small_large_void = 3.0 # Boundary between "small" and "large" voids

split_walls_filaments = False # True

## Format, file related variables

## tree_filename format:
# Number of rows to skip before getting number of
trees_file_skip1 = 48
trees_file_skip2 = 50

convert_1010Msun_Myr_to_Msun_yr = 1e4
maximum_allowed_fit_amplitude = 1000.0
convert_Myr_to_Gyr = 1e-3

## These options are especially useful for the script plot_void_galaxies.py
## . They are not intended for big runs (128^3 or bigger). Enabling
## print_xyz_particle_ids will lead to very big text files of particle xyz
## positions and identity numbers.
print_xyz_particle_ids = False
print_particle_ids_of_haloes_of_gals_in_voids = True
particle_ids_of_haloes_of_gals_in_voids_filename = 'particle_ids_of_haloes_of_gals_in_voids.txt'

## Debug only
debug = False
verbose = False

OutputFormat = '.png'

infall_fit_do_plot = False
gals_sfr_do_plot = False
gals_infall_do_plot = False


OutputList = []
LSBG_ID = []

class Results:

    def __init__(self):
        """Here we set up some of the variables which will be global to this
        class."""

        if whichsimulation == 0:    # Mini-Millennium
            if os.environ.get('HUBBLE0'):
                self.Hubble_h = float(os.environ.get('HUBBLE0'))/100
            else:
                self.Hubble_h = 0.73

            if os.environ.get('LBOX'):
                self.BoxSize = float(os.environ.get('LBOX'))
            else:
                self.BoxSize = 62.5       # Mpc/h

            self.MaxTreeFiles = 1     # FilesPerSnapshot

            if os.environ.get('NCROOT'):
                self.Ncroot = float(os.environ.get('NCROOT'))
            else:
                self.Ncroot = 32      # cube root of particle number in simulation

            if os.environ.get('TREEFILE'):
                self.tree_filename = os.environ.get('TREEFILE')
            else:
                self.tree_filename = 'Filename_TREEFILE_is_missing'

            if(os.environ.get('MASSGAP_LOWER_LIMIT') and os.environ.get('MASSGAP_UPPER_LIMIT')):
                self.mass_gap = [float(os.environ.get('MASSGAP_LOWER_LIMIT')),float(os.environ.get('MASSGAP_UPPER_LIMIT'))]
            else:
                self.tree_filename = self.mass_gap = [1.e11,1.e12]

            if os.environ.get('HALO_IS_IN_VOID_MIN_FRACTION'):
                self.halo_is_in_void_min_fraction = os.environ.get('HALO_IS_IN_VOID_MIN_FRACTION')
            else:
                self.halo_is_in_void_min_fraction = 0.5

            if os.environ.get('MASS_DM_PARTICLE'):
                self.massDM = float(os.environ.get('MASS_DM_PARTICLE'))
            else:
                G=6.67408e-11
                Mass_Sun=2.e30
                self.massDM = 3.086e22 * 1000 * 1000 * 0.3 * self.BoxSize**3 * 3/(8 * np.pi * G) * 100 * 100 / Mass_Sun / self.Ncroot**3

            if os.environ.get('INNER_RAD_THRESHOLD'):
                self.inner_rad_threshold = float(os.environ.get('INNER_RAD_THRESHOLD'))
            else:
                self.inner_rad_threshold = 0.0

            if os.environ.get('OUTER_RAD_THRESHOLD'):
                self.outer_rad_threshold = float(os.environ.get('OUTER_RAD_THRESHOLD'))
            else:
                self.outer_rad_threshold = 1.3

            if os.environ.get('RAD_FRAC'):
                self.rad_frac = float(os.environ.get('RAD_FRAC'))
            else:
                self.rad_frac = 1.0

            if os.environ.get('SPLIT_POSITION_IN_VOID'):
                split=os.environ.get('SPLIT_POSITION_IN_VOID')
                if(split=="YES"):
                    self.split_position_in_void = True
                else:
                    self.split_position_in_void = False

            if os.environ.get('CENTRE'):
                which_centre = str(os.environ.get('which_centre'))
                if(which_centre=="elaphro"):
                    self.id_file_filename="Gadget_Ids_within_rad_elaphro.txt"
                elif(which_centre=="circ"):
                    self.id_file_filename="Gadget_Ids_within_rad_circ.txt"
                elif(which_centre=="geom"):
                    self.id_file_filename="Gadget_Ids_within_rad_geom.txt"
                else:
                    self.id_file_filename="Gadget_Ids_within_rad_elaphro.txt"
                print("id_file_filename=",self.id_file_filename)

            print("allresults_LSBGs.py: self.Hubble_h, self.BoxSize = ",self.Hubble_h, self.BoxSize)

        elif whichsimulation == 1:  # Full Millennium
            if os.environ.get('HUBBLE0'):
                self.Hubble_h = float(os.environ.get('HUBBLE0'))/100
            else:
                self.Hubble_h = 0.75

            if os.environ.get('LBOX'):
                self.BoxSize = float(os.environ.get('LBOX'))
            else:
                self.BoxSize = 500      # Mpc/h

            self.MaxTreeFiles = 512   # FilesPerSnapshot

        else:
            print("Please pick a valid simulation!")
            exit(1)

        if os.environ.get('METADATA_CREATOR'):
            self.metadata_creator = os.environ.get('METADATA_CREATOR')
        else:
            self.metadata_creator = 'elaphrocentre'
        self.metadata = {"Creator": self.metadata_creator}



    def read_gals(self, model_name, first_file, last_file):
        """
        Read in the data about all galaxies generated by sage.
        """

        # The input galaxy structure:
        Galdesc_full = [
            ('SnapNum'                      , np.int32),
            ('Type'                         , np.int32),
            ('GalaxyIndex'                  , np.int64),
            ('CentralGalaxyIndex'           , np.int64),
            ('SAGEHaloIndex'                , np.int32),
            ('SAGETreeIndex'                , np.int32),
            ('SimulationHaloIndex'          , np.int64),
            ('mergeType'                    , np.int32),
            ('mergeIntoID'                  , np.int32),
            ('mergeIntoSnapNum'             , np.int32),
            ('dT'                           , np.float32),
            ('Pos'                          , (np.float32, 3)),
            ('Vel'                          , (np.float32, 3)),
            ('Spin'                         , (np.float32, 3)),
            ('Len'                          , np.int32),
            ('Mvir'                         , np.float32),
            ('CentralMvir'                  , np.float32),
            ('Rvir'                         , np.float32),
            ('Vvir'                         , np.float32),
            ('Vmax'                         , np.float32),
            ('VelDisp'                      , np.float32),
            ('ColdGas'                      , np.float32),
            ('StellarMass'                  , np.float32),
            ('BulgeMass'                    , np.float32),
            ('HotGas'                       , np.float32),
            ('EjectedMass'                  , np.float32),
            ('BlackHoleMass'                , np.float32),
            ('IntraClusterStars'            , np.float32),
            ('MetalsColdGas'                , np.float32),
            ('MetalsStellarMass'            , np.float32),
            ('MetalsBulgeMass'              , np.float32),
            ('MetalsHotGas'                 , np.float32),
            ('MetalsEjectedMass'            , np.float32),
            ('MetalsIntraClusterStars'      , np.float32),
            ('SfrDisk'                      , np.float32),
            ('SfrBulge'                     , np.float32),
            ('SfrDiskZ'                     , np.float32),
            ('SfrBulgeZ'                    , np.float32),
            ('DiskRadius'                   , np.float32),
            ('Cooling'                      , np.float32),
            ('Heating'                      , np.float32),
            ('QuasarModeBHaccretionMass'    , np.float32),
            ('TimeOfLastMajorMerger'         , np.float32),
            ('TimeOfLastMinorMerger'         , np.float32),
            ('OutflowRate'                  , np.float32),
            ('infallMvir'                   , np.float32),
            ('infallVvir'                   , np.float32),
            ('infallVmax'                   , np.float32)
            ]
        names = [Galdesc_full[i][0] for i in range(len(Galdesc_full))]
        formats = [Galdesc_full[i][1] for i in range(len(Galdesc_full))]
        Galdesc = np.dtype({'names':names, 'formats':formats}, align=True)


        # Initialize variables.
        TotNTrees = 0
        TotNGals = 0
        FileIndexRanges = []

        print("Determining array storage requirements.")

        # Read each file and determine the total number of galaxies to be read in
        goodfiles = 0
        for fnr in range(first_file,last_file+1):
            fname = model_name+'_'+str(fnr)  # Complete filename

            if not os.path.isfile(fname):
              # print "File\t%s  \tdoes not exist!  Skipping..." % (fname)
              continue

            if getFileSize(fname) == 0:
                print("File\t%s  \tis empty!  Skipping..." % (fname))
                continue

            fin = open(fname, 'rb')  # Open the file
            Ntrees = np.fromfile(fin,np.dtype(np.int32),1)  # Read number of trees in file
            NtotGals = np.fromfile(fin,np.dtype(np.int32),1)[0]  # Read number of gals in file.
            TotNTrees = TotNTrees + Ntrees  # Update total sim trees number
            TotNGals = TotNGals + NtotGals  # Update total sim gals number
            goodfiles = goodfiles + 1  # Update number of files read for volume calculation
            fin.close()

        print()
        print("Input files contain:\t%d trees ;\t%d galaxies ." % (TotNTrees, TotNGals))
        print()

        # Initialize the storage array
        G = np.empty(TotNGals, dtype=Galdesc)

        offset = 0  # Offset index for storage array

        # Open each file in turn and read in the preamble variables and structure.
        print("Reading in files.")
        for fnr in range(first_file,last_file+1):
            fname = model_name+'_'+str(fnr)  # Complete filename

            if not os.path.isfile(fname):
              continue

            if getFileSize(fname) == 0:
                continue

            print("read_gals: Will read: ",fname)
            fin = open(fname, 'rb')  # Open the file
            Ntrees = np.fromfile(fin, np.dtype(np.int32), 1)  # Read number of trees in file
            NtotGals = np.fromfile(fin, np.dtype(np.int32), 1)[0]  # Read number of gals in file.
            np.fromfile(fin, np.dtype((np.int32, Ntrees)),1) # skip over GalsPerTree = number of gals in each tree
            print(":   Reading N=", NtotGals, "   \tgalaxies from file: ", fname)
            GG = np.fromfile(fin, Galdesc, NtotGals)  # Read in the galaxy structures

            FileIndexRanges.append((offset,offset+NtotGals))

            # Slice the file array into the global array
            # The copy() operation is required! Otherwise, we point to
            # the GG data, which changes from file to file, rather
            # than making a new copy of the data.
            # This comes from python's strategies for fast vector operations.
            #
            G[offset:offset+NtotGals]=GG[0:NtotGals].copy()

            del(GG)
            offset = offset + NtotGals  # Update the offset position for the global array

            fin.close()  # Close the file


        print()
        print("Total galaxies considered:", TotNGals)

        # Convert the Galaxy array into a recarray
        G = G.view(np.recarray)

        w = np.where(G.StellarMass > 1.0)[0]
        print("Galaxies more massive than 10^10Msun/h:", len(w))

        print()

        # Calculate the volume given the first_file and last_file
        self.volume = self.BoxSize**3.0 * goodfiles / self.MaxTreeFiles

        return G

    # This routine is not used yet. It will be modidied and used in the future after Pégase
    # is added to the pipeline.
    def identify_lsbgs(self, G):
        """
        Identify all LSBGs in our galaxy population. The LSBGs are identified
        with the help of the stellar sythesis code Pégase

        Parameter
        ------------
        G: The full galaxy data. It is the default output of sage.

        returns
        ------------
        w_lsbg: list
        w_lsbg hold the indices of all LSBGs in the galaxy data
        """
        ls = open("LSBGs_IDs.dat", "r")
        for line in ls:
            #values = [float(s) for s in line.split( )]
            s = line.split()
            if s:
                this_LSBG_ID = np.fromstring(s[0],dtype=np.int64,sep=' ')
                LSBG_ID.append(this_LSBG_ID)

        w_lsbg = [] # needed by default
        for i in range(len(LSBG_ID)):
            w_lsbg.extend(np.where(G.GalaxyIndex == LSBG_ID[i])[0])

        return w_lsbg


    def void_gals(self, G, void_file, void_dir, haloes_dir, last_snap_number):
        """
        Find every galaxy that is in a void.

        Parameter
        ------------
        G: the galaxy data
        void_file: string
             The name of the output file from Revolver that contains all information
             about the voids that we want to analyse. There are different files for
             different choices of the centre.
        void_dir: string
             The path to the void directory.
        haloes_dir: string
             The path to the haloes directory.
        last_snap_number: int
             Last snapshot number of the simulation. Providing this information makes the
            code more flexible.

        Return value
        ------------
        Create and return a list 'w_void' with the G indices of galaxies
        that are in voids.
        Create and return a list 'w_inner_void' and 'w_outer_void' with the G indices of galaxies
        that are in the inner void/ that are in the outer region of the void.

        Side effect
        -----------
        gal_in_void_ID_file: file
        Add a line for every galaxy in a void indicating the G index of the galaxy, the
        void-centric distance of the galaxy, the void-centric distance as a fraction of
        the void radius, and the void identity (assigned by revolver).
        """
        print("identify void galaxies...")

        calc=calculations()

        if(np_modern):
            rng = default_rng()

        # This must be greater than any possible void ID:
        min_void_ID_initial = 1000000
        # This must be less than any possible void ID:
        max_void_ID_initial = -1

        # This must be an invalid void ID, that is permitted by the type
        # of the void IDs, but does not correctly identify any void.
        invalid_voidID = -1

        position_check_void_gals = True

        w_void =  [] # holds the indices for void galaxies
        w_filament, w_wall = [], [] # holds the indices for galaxies assumed to be in walls/filaments
        w_inner_void, w_outer_void = [], [] # holds the indices for void galaxies in the inner/outer region of the void
        fraction = [] # r/R_eff of a galaxy in a void
        gal_void_fraction = [] # How many particles are in a void # not used so far
        void_id = [] # Void ID read from our additional output - should be the same array as VoidID
        dm_in_void_id = [] # *indices* of DM particles in the void, read from our additional output

        ## These two arrays should be of exactly the same size.
        ## The i-th elements of the two arrays should together give
        ## the DM particle number and the halo number for a given particle.
        # DM particle IDs in a halo - this array only know particle numbers.
        particle_in_halo = np.array([],dtype=np.int64)
        # Halo Ids - this array only know halo IDs.
        halo_id_of_particle_in_halo = np.array([],dtype=np.int)

        # read in which DM particles are inside a void:
        id_file = void_dir + '/' + self.id_file_filename
        print("allresults_LSBGs.py: void_gals: id_file = ",id_file)
        k=0
        N_particles = (np.int(self.Ncroot))**3
        print("N_particles = ",N_particles)
        # Initialise the void identities of the DM particles.
        # Use N_particles+1 to match the gadget numbering, from 1 to N^3.
        void_IDs_of_dm_particles = invalid_voidID * np.ones(N_particles+1,dtype=np.int64)
        for line in open(id_file,'r'):
            if not line.startswith('#'):
                if(k%2 == 0):
                    # add a scalar void identity to the void_id list
                    this_voidID=np.fromstring(line, dtype=np.int, sep=' ')
                    void_id.append(this_voidID[0])
                else:
                    # associate a list of DM particle identities with the void
                    dm_particles_of_this_void=np.fromstring(line, dtype=np.int64, sep=' ')
                    ## Replace the invalid_voidIDs by of these DM particles
                    ## by their void number =  this_voidID
                    void_IDs_of_dm_particles[dm_particles_of_this_void] = this_voidID

                    dm_in_void_id.append(dm_particles_of_this_void)  # to be deprecated?
                k += 1

        max_void_ID = np.maximum(max_void_ID_initial, np.max(void_id))
        min_void_ID = np.minimum(min_void_ID_initial, np.min(void_id))
        print("min_void_ID = ",min_void_ID)
        print("max_void_ID = ",max_void_ID)
        if(min_void_ID <= invalid_voidID and invalid_voidID <= max_void_ID):
            raise RuntimeError('void_gals: Error: invalid_void_ID = ',
                               invalid_void_ID,
                               ' lies in the range ',min_void_ID,' .. ',
                               max_void_ID,' of void IDs.')
        if(len(void_id) < 1):
            raise RuntimeError('void_gals: Error: There are no detected voids in ', id_file)

        if(debug):
            print("void_id=",void_id)
            #print("dm_in_void_id=",dm_in_void_id)
            print("void_IDs_of_dm_particles = ",void_IDs_of_dm_particles)


        #######################################################################
        if(debug):
            print("void_file=",void_file)
            read_gadget.get_particles.argtypes = [ctypes.c_char_p]
            read_gadget.read_gadget.argtypes = [ctypes.c_char_p]
            if(not os.path.isfile(tracer_file.encode('utf-8'))):
                raise RuntimeError("allresults_LSBGs: void_gals: debug error: The gadget format tracer_file ",
                      tracer_file, " cannot be found.")
            else:
                num_tracers=read_gadget.get_particles(tracer_file.encode('utf-8'))
                tracers = np.empty((N_particles, 3))
                gadget_ids = np.empty(N_particles)
                read_gadget.read_gadget.restype = ndpointer(dtype=ctypes.c_float, shape=(N_particles,4))
                returnValue=read_gadget.read_gadget(tracer_file.encode('utf-8'))
                tracers[:,0]=returnValue[:,0]
                tracers[:,1]=returnValue[:,1]
                tracers[:,2]=returnValue[:,2]
                gadget_ids = np.empty(N_particles)
                read_gadget.read_ids.restype = ndpointer(dtype=ctypes.c_long, shape=(N_particles))
                returnValue=read_gadget.read_gadget(tracer_file.encode('utf-8'))
                returnValue=read_gadget.read_ids(tracer_file.encode('utf-8'))
                gadget_ids[:]=returnValue[:]
                test_dist = open("particle_centre_dist.dat","w+")
                test_revolver = open("test_revolver.dat","w+")
        #######################################################################


        VoidID = np.loadtxt(void_file, comments='#', usecols=0, dtype=np.int64)
        VoidX = np.loadtxt(void_file, comments='#', usecols=1, dtype=np.float)
        VoidY = np.loadtxt(void_file, comments='#', usecols=2, dtype=np.float)
        VoidZ = np.loadtxt(void_file, comments='#', usecols=3, dtype=np.float)
        VoidRadius = np.loadtxt(void_file, comments='#', usecols=4, dtype=np.float)
        if(VoidID.size < 1):
            raise RuntimeError('void_gals: Error: There are no detected voids in ', void_file)


        rockstar_out_file = haloes_dir + '/out_' + last_snap_number + '.list'
        rockstar_halo_id = np.loadtxt(rockstar_out_file, comments='#', usecols=0, dtype=np.int64)
        rockstar_halo_x = np.loadtxt(rockstar_out_file, comments='#', usecols=8, dtype=np.float)
        rockstar_halo_y = np.loadtxt(rockstar_out_file, comments='#', usecols=9, dtype=np.float)
        rockstar_halo_z = np.loadtxt(rockstar_out_file, comments='#', usecols=10, dtype=np.float)

        # To find every galaxy that is in a void, first make an array 'G_indices'
        # of all the galaxies at the final time step:
        G_indices = np.where(G.StellarMass > 0.0)[0]

        # Find the relation between Rockstar halo IDs and consistent-tree halo IDs
        print("Will try to read self.tree_filename: ",self.tree_filename)
        # Number of haloes (at final output time)
        trees_file_n_halos_final = np.loadtxt(self.tree_filename,
                                              usecols=0,
                                              dtype=np.int64,
                                              skiprows=trees_file_skip1,
                                              max_rows=1)
        print("Trees file: Number of haloes at final output time = ",
              trees_file_n_halos_final)
        with open("N_haloes.dat","w+") as N_halos_file:
            N_halos_file.write("N_haloes_final_time = %i\n" % trees_file_n_halos_final)
        N_halos_file.close()

        trees_file_ctree_halo_indices = np.loadtxt(self.tree_filename,
                                                   usecols=1,
                                                   dtype=np.int64,
                                                   skiprows=trees_file_skip2)
        trees_file_rockstar_halo_indices = np.loadtxt(self.tree_filename,
                                                      usecols=30,
                                                      dtype=np.int64,
                                                      skiprows=trees_file_skip2)
        if(debug):
            print("trees_file_ctree_halo_indices = ",trees_file_ctree_halo_indices)
            print("trees_file_rockstar_halo_indices = ",trees_file_rockstar_halo_indices)

        # Read the halo ID and the DM particle IDs from other rockstar output files:
        for filename in glob.iglob(haloes_dir + "/halos_" + last_snap_number + '**.*.particles', recursive=True):
            this_dm_id = np.loadtxt(filename, comments='#', usecols=6, dtype=np.int64)
            this_halo_id = np.loadtxt(filename, comments='#', usecols=9, dtype=np.int)
            halo_id_debug = 0 # for debugging only
            if( ((np.where( halo_id_debug == this_halo_id ))[0]).any() ):
                print("filename=",filename)
                print("this_dm_id=",this_dm_id)
                print("this_halo_id=",this_halo_id)
            # Append the two arrays to the corresponding 1D halo ID and
            # particle ID arrays. These two arrays should contain
            # matching particle and halo IDs.
            halo_id_of_particle_in_halo = np.append(halo_id_of_particle_in_halo,this_halo_id)
            particle_in_halo = np.append(particle_in_halo,this_dm_id)

        if(debug):
            print("halo_id_of_particle_in_halo=",halo_id_of_particle_in_halo)
            print("particle_in_halo=",particle_in_halo)


        print("np.max(halo_id_of_particle_in_halo)=", np.max(halo_id_of_particle_in_halo))
        print("np.min(halo_id_of_particle_in_halo)=", np.min(halo_id_of_particle_in_halo))

        # Set up bins for finding which void a halo is mostly
        # associated with:
        # - floating point half-integers for histogram; endpoint included:
        bins_void_IDs = np.linspace(min_void_ID-0.5, max_void_ID+0.5,
                                    num=(max_void_ID-min_void_ID+2),
                                    endpoint=True)
        # - integers - endpoint excluded:
        bins_void_IDs_mid = np.arange(min_void_ID, max_void_ID+1)
        if(debug):
            print("bins_void_IDs = ",bins_void_IDs)
            print("bins_void_IDs_mid = ",bins_void_IDs_mid)
            ## The _mid array - with integers - should be one shorter than
            ## the floating point array.
            print("bins_void_IDs.size = ",bins_void_IDs.size)
            print("bins_void_IDs_mid.size = ",bins_void_IDs_mid.size)

        gal_in_void_ID_file = open("gal_in_void_ID.dat","w+")
        if(split_walls_filaments):
            gal_in_wall_ID_file = open("gal_in_wall_ID.dat","w+")
            gal_in_filament_ID_file = open("gal_in_filament_ID.dat","w+")
        if(print_particle_ids_of_haloes_of_gals_in_voids):
            gal_in_void_particles_file = (
                open(particle_ids_of_haloes_of_gals_in_voids_filename,'w+') )


        # Loop over each galaxy 'l' at the final time step:
        for l_G_index in G_indices:
            print("l_G_index=",l_G_index)

            # Convert from a consistent-trees halo index to a rockstar halo index
            ctree_halo_index = (G.SimulationHaloIndex)[l_G_index]
            if(ctree_halo_index < 0):
                warnings.warn('allresults_LSBGs: void_gals: Warning: using ctree_halo_index of a non-primary halo = '+str(ctree_halo_index))
                ctree_halo_index = -ctree_halo_index

            #ctree_halo_index_repeated = ctree_halo_index * np.ones(trees_file_ctree_halo_indices.size)
            i_list = (np.where(trees_file_ctree_halo_indices == ctree_halo_index))[0]
            if(i_list.size < 1):
                raise RuntimeError('allresults_LSBGs: void_gals: Error: ctree_halo_index = ',ctree_halo_index,' not found.')

            if(i_list.size > 1):
                raise RuntimeError('allresults_LSBGs: void_gals: Error: multiple values of ctree_halo_index = ',ctree_halo_index)

            i = i_list[0]
            sim_halo_index = trees_file_rockstar_halo_indices[i]
            print("sim_halo_index= ", sim_halo_index)

            # Check if the position of the identified galaxies agrees with the position of the halo
            if(position_check_void_gals):
                error_pos_x = False
                error_pos_y = False
                error_pos_z = False
                # Set a tolerance of 0.01% of the BoxSize as a sanity check
                tol_position_check = 1.e-4 * self.BoxSize
                this_rockstar_halo = np.where(rockstar_halo_id == sim_halo_index)[0]
                if(np.abs(rockstar_halo_x[this_rockstar_halo] - G.Pos[l_G_index,0]) > tol_position_check):
                    error_pos_x = True
                if(np.abs(rockstar_halo_y[this_rockstar_halo] - G.Pos[l_G_index,1]) > tol_position_check):
                    error_pos_y = True
                if(np.abs(rockstar_halo_z[this_rockstar_halo] - G.Pos[l_G_index,2]) > tol_position_check):
                    error_pos_z = True

                # if one of them is true return an error
                if(any([ error_pos_x,error_pos_y,error_pos_z ])):
                    raise RuntimeError('The position of the identified halo and the position of the galaxy do not agree.',
                                       'Position halo = (%.3f %.3f %.3f)' %
                                       (rockstar_halo_x[this_rockstar_halo],
                                        rockstar_halo_y[this_rockstar_halo],
                                        rockstar_halo_z[this_rockstar_halo]),
                                       'Position galaxy = (%.3f %.3f %.3f)' %
                                       (G.Pos[l_G_index,0], G.Pos[l_G_index,1], G.Pos[l_G_index,2]))


            # Find the index of this galaxy's halo
            if(debug):
                print("G.GalaxyIndex = ",G.GalaxyIndex)
                print("G.CentralGalaxyIndex = ",G.CentralGalaxyIndex)
                print("G.SAGEHaloIndex = ",G.SAGEHaloIndex)
                print("G.SimulationHaloIndex = ",G.SimulationHaloIndex)


            # Find the locations (in the two matching arrays
            # particle_in_halo and halo_id_of_particle_in_halo)
            # of the halo_id:
            where_is_halo_id = (np.where(halo_id_of_particle_in_halo == sim_halo_index))[0]
            if(where_is_halo_id.any()):
                # Find the DM particles associated with the halo:
                dm_particle_list = particle_in_halo[where_is_halo_id]
                if(debug):
                    print("where_is_halo_id = ",where_is_halo_id)
                    print("dm_particle_list = ",dm_particle_list)


                # Create a list of the void IDs of those particles.
                # Some of the IDs will be invalid, because the halo
                # is not in a void.
                this_galaxy_void_IDs = void_IDs_of_dm_particles[dm_particle_list]
                if(debug):
                    print("this_galaxy_void_IDs = ",this_galaxy_void_IDs)

                # Find if any of the halo's particles are in a void.
                # ASSUMPTION: any integer in the range from the minimum to
                # maximum void ID is a valid void ID.
                i_valid = (np.where( (min_void_ID <= this_galaxy_void_IDs) &
                                     (this_galaxy_void_IDs <= max_void_ID) ))[0]
                valid_void_indices = this_galaxy_void_IDs[i_valid]

                i_valid_size = i_valid.size

                # Do not continue further if there are no halo particles in
                # any void, or if the fraction of halo particles in the "best"
                # void is certain to fail the test criterion.
                if(i_valid_size > 0 and
                   float(i_valid_size)/float(dm_particle_list.size) > np.float(self.halo_is_in_void_min_fraction) ):
                    # Find the most common void index for this halo:
                    # * Make a histogram:
                    hist_void_indices = (np.histogram(valid_void_indices,bins_void_IDs))[0]
                    # * Find the most popular void indices:
                    n_most_popular = np.max(hist_void_indices)
                    # * Find the (possibly multiple) most popular indices:
                    i_bins = (np.where(hist_void_indices == n_most_popular))[0]
                    N_i_bins = i_bins.size


                    fraction_in_void = (
                        hist_void_indices/np.float(dm_particle_list.size) )

                    fraction_in_best_void = (
                        np.float(n_most_popular)/np.float(dm_particle_list.size) )
                    # Decide if the halo associated with the galaxy "is in" a
                    # single dominating void.
                    # Only continue further if this the case.
                    # It remains possible that the halo is on a wall or a knot
                    # that divides two or more voids; this depends on how
                    # many void boundary particles are included in the definition
                    # of the void.

                    how_many_voids=((np.array(np.where(hist_void_indices > 0))).shape)[1]
                    if( how_many_voids > 0):


                        if(fraction_in_best_void > np.float(self.halo_is_in_void_min_fraction) or
                           (split_walls_filaments and how_many_voids > 1)):

                            if(debug and N_i_bins > 1):
                                print("n_most_popular = ",n_most_popular)
                                print("i_bins = ",i_bins)
                                print("N_i_bins = ",N_i_bins)

                            # Randomly choose one of the "tied winners" in popularity
                            # if there's a tie.
                            if(1 == N_i_bins):
                                best_void_index = bins_void_IDs_mid[i_bins[0]]
                            elif(1 < N_i_bins):
                                if(np_modern):
                                    best_void_index = (
                                        bins_void_IDs_mid[ i_bins[rng.integers(0,N_i_bins)] ] )
                                else:
                                    best_void_index = (
                                        bins_void_IDs_mid[ i_bins[np.random.randint(0,N_i_bins)] ])
                            else:
                                raise RuntimeError('void_gals: Software error. Please check the code. N_i_bins = ',N_i_bins)

                            if(verbose):
                                print("n_most_popular = ",n_most_popular)
                                print("i_valid_size = ",i_valid_size)
                                print("i_bins = ",i_bins)
                                print("N_i_bins = ",N_i_bins)
                            print("best_void_index = ",best_void_index)

                            # Find place in the arrays  VoidID, Void(X|Y|Z), VoidRadius
                            # of the void with identity 'best_void_index'.
                            # ASSUME that this occurs only once.
                            void_cat_index = ((np.where(VoidID == best_void_index))[0])[0]
                            if(debug):
                                print("void_cat_index = ",void_cat_index)

                            ########################################################################
                            if(debug):
                                where_in_gadget = np.array([])
                                for dm_id in dm_particle_list:
                                    where_in_gadget = np.append(where_in_gadget,np.where(gadget_ids == dm_id)[0])
                                print("where_in_gadget=", where_in_gadget)
                                test_dist.write("%i\n" % G.GalaxyIndex[l_G_index])
                                test_revolver.write("%i\n" % VoidID[void_cat_index])
                                for where in where_in_gadget:
                                    print("where=",where)
                                    moved_parts = (
                                        calc.move_coordinates(tracers[np.int(where),0], tracers[np.int(where),1], tracers[np.int(where),2],
                                                              [VoidX[void_cat_index],
                                                               VoidY[void_cat_index],
                                                               VoidZ[void_cat_index]],
                                                              self.BoxSize) )
                                    distance_part = np.sqrt((moved_parts[0])**2
                                                            +(moved_parts[1])**2
                                                            +(moved_parts[2])**2)
                                    this_fraction_part = distance_part/VoidRadius[void_cat_index]
                                    if(this_fraction_part > self.rad_frac):
                                        warnings.warn('allresults_LSBGs: void_gals: Warning: DM particle is further away than it should be = '+str(this_fraction_part))

                                    test_dist.write("%f " % this_fraction_part)
                                    test_revolver.write("%i " % gadget_ids[np.int(where)])
                                test_dist.write("\n")
                                test_revolver.write("\n")
                            ###############################################################################

                            # T^3 calculation of positions, centred on this void:
                            moved_gals = (
                                calc.move_coordinates(G.Pos[l_G_index,0], G.Pos[l_G_index,1], G.Pos[l_G_index,2],
                                                      [VoidX[void_cat_index],
                                                       VoidY[void_cat_index],
                                                       VoidZ[void_cat_index]],
                                                      self.BoxSize) )
                            distance = np.sqrt((moved_gals[0])**2
                                               +(moved_gals[1])**2
                                               +(moved_gals[2])**2)

                            this_fraction = distance/VoidRadius[void_cat_index]
                            fraction.append(this_fraction)


                            if(split_walls_filaments):
                                this_galaxy_in_how_many_voids_filament=0
                                this_galaxy_in_how_many_voids_wall=0
                                for dm_fraction_in_void in fraction_in_void:
                                    if(dm_fraction_in_void > 0.2):
                                        this_galaxy_in_how_many_voids_filament += 1
                                    if(dm_fraction_in_void > 0.3):
                                        this_galaxy_in_how_many_voids_wall += 1


                                if( how_many_voids > 2 and this_galaxy_in_how_many_voids_filament >= 3 ):
                                    w_filament.append(l_G_index)
                                    # Print a line to a file indicating the G index of the galaxy, the
                                    # void-centric distance of the galaxy, the void-centric
                                    # distance as a fraction of the void radius, and the
                                    # void identity (assigned by revolver).
                                    gal_in_filament_ID_file.write("%i %f %f %f %i %f %f\n" %
                                                                  (G.GalaxyIndex[l_G_index],
                                                                   distance,
                                                                   this_fraction,
                                                                   VoidRadius[void_cat_index],
                                                                   best_void_index,
                                                                   fraction_in_best_void,
                                                                   G.Rvir[l_G_index]
                                                                  ))


                                elif( how_many_voids == 2 and this_galaxy_in_how_many_voids_wall == 2 ):
                                    w_wall.append(l_G_index)
                                    # Print a line to a file indicating the G index of the galaxy, the
                                    # void-centric distance of the galaxy, the void-centric
                                    # distance as a fraction of the void radius, and the
                                    # void identity (assigned by revolver).
                                    gal_in_wall_ID_file.write("%i %f %f %f %i %f %f\n" %
                                                              (G.GalaxyIndex[l_G_index],
                                                               distance,
                                                               this_fraction,
                                                               VoidRadius[void_cat_index],
                                                               best_void_index,
                                                               fraction_in_best_void,
                                                               G.Rvir[l_G_index]
                                                              ))

                                if( fraction_in_best_void > np.float(self.halo_is_in_void_min_fraction) ):
                                    w_void.append(l_G_index)

                                    if(self.split_position_in_void):
                                        if(this_fraction > self.inner_rad_threshold and this_fraction < self.outer_rad_threshold):
                                            w_inner_void.append(l_G_index)
                                        if(this_fraction > self.outer_rad_threshold and this_fraction < 3.0):
                                            w_outer_void.append(l_G_index)


                                    # Print a line to a file indicating the G index of the galaxy, the
                                    # void-centric distance of the galaxy, the void-centric
                                    # distance as a fraction of the void radius, and the
                                    # void identity (assigned by revolver).
                                    gal_in_void_ID_file.write("%i %f %f %f %i %f %f\n" %
                                                              (G.GalaxyIndex[l_G_index],
                                                               distance,
                                                               this_fraction,
                                                               VoidRadius[void_cat_index],
                                                               best_void_index,
                                                               fraction_in_best_void,
                                                               G.Rvir[l_G_index]
                                                              ))

                                    # Write halo particle IDs (rockstar/gadget) to a
                                    # corresponding file.
                                    if(print_particle_ids_of_haloes_of_gals_in_voids):
                                        for id in dm_particle_list:
                                            gal_in_void_particles_file.write("%d " % id)
                                        gal_in_void_particles_file.write("\n")

                            elif( fraction_in_best_void > np.float(self.halo_is_in_void_min_fraction) ):
                                w_void.append(l_G_index)

                                if(self.split_position_in_void):
                                    if(this_fraction > self.inner_rad_threshold and this_fraction < self.outer_rad_threshold):
                                        w_inner_void.append(l_G_index)
                                    if(this_fraction > self.outer_rad_threshold and this_fraction < 3.0):
                                        w_outer_void.append(l_G_index)


                                # Print a line to a file indicating the G index of the galaxy, the
                                # void-centric distance of the galaxy, the void-centric
                                # distance as a fraction of the void radius, and the
                                # void identity (assigned by revolver).
                                gal_in_void_ID_file.write("%i %f %f %f %i %f %f\n" %
                                                          (G.GalaxyIndex[l_G_index],
                                                           distance,
                                                           this_fraction,
                                                           VoidRadius[void_cat_index],
                                                           best_void_index,
                                                           fraction_in_best_void,
                                                           G.Rvir[l_G_index]
                                                          ))

                                # Write halo particle IDs (rockstar/gadget) to a
                                # corresponding file.
                                if(print_particle_ids_of_haloes_of_gals_in_voids):
                                    for id in dm_particle_list:
                                        gal_in_void_particles_file.write("%d " % id)
                                    gal_in_void_particles_file.write("\n")


        gal_in_void_ID_file.close()
        if(split_walls_filaments):
            gal_in_wall_ID_file.close()
            gal_in_filament_ID_file.close()
        #####################################################
        if(debug):
            test_dist.close()
            test_revolver.close()
        ######################################################

        if(print_particle_ids_of_haloes_of_gals_in_voids):
            gal_in_void_particles_file.close()

        # bins = np.linspace(0,1,10)
        #N_bins = 20
        delta_max = 0.1
        if(np.array(fraction).size > 0):
            bin_edges= np.linspace(0.0, np.max(fraction)+delta_max, 20)
        else:
            bin_edges= np.linspace(0.0, 2.0+delta_max, 20)
        if(verbose):
            print("bin_edges = ",bin_edges)
        print("fraction=",fraction)

        plt.figure()
        plt.rcParams.update({'font.size': 18,
                         'font.family': 'serif'})
        plt.ylabel(r'$N$')
        plt.xlabel(r'$r/R_{\mathrm{eff}}$')
        plt.hist(fraction, bins=bin_edges, linewidth=1.0,
                 edgecolor ='k', facecolor='none')
        #ylims = plt.ylim()
        #plt.ylim((0.0, ylims[1])) # no effect
        plt.tight_layout()
        #plt.legend(loc='upper left', numpoints=1, labelspacing=0.1)
        plt.savefig("number_counts.eps", metadata=self.metadata)
        plt.close()


        if(split_walls_filaments and self.split_position_in_void):
            return w_void, w_wall, w_filament, w_inner_void, w_outer_void
        else:
            if(split_walls_filaments):
                return w_void, w_wall, w_filament
            if(self.split_position_in_void):
                return w_void, w_inner_void, w_outer_void

        return w_void


    # This routine is not used yet. It will be modidied and used in the future after Pégase
    # is added to the pipeline.
    def void_lsbgs(self, w_lsbg, w_void):
        """
        This funtion is supposed to check which LSBGs (found with Pégase) are
        in a void. The outcome will be a list of all LSBGs in voids.

        Parameter
        ------------
        w_lsbg: list
        The list of galaxy indices (in G) that are LSBGs.
        w_void: list
        The list of galaxy indices (in G) that are in voids.

        returns
        -----------
        w_void_lsbg: list
        A list that holds all indices of galaxies that are in a void
        *and* an LSBG.
        """
        with open("lsbg_in_void_ID.dat","w+") as lsbg_in_void_ID_file:

            w_void_lsbg = []
            for gal in w_lsbg:
                if(gal in w_void):
                    w_void_lsbg.append(gal)
                    lsbg_in_void_ID_file.write("%i" % G.GalaxyIndex[gal])

        lsbg_in_void_ID_file.close
        return w_void_lsbg

    def gals_infall(self, G):
        """
        Read in the information about the infalling mass for every galaxy and fit it
        to a negative exponential A*exp(-B*t). This function calls routines from
        exptest.py. The routine will create several output files.
        It also calls infall_stat.py to estimate the mean and std of the parameters.

        Paramter
        ------------
        G: galaxy data

        returns
        ------------
        exp_fit_params_void.dat, exp_fit_params_nonvoid.dat and if 'split_position_in_void =True'
        exp_fit_params_void_inner_void.dat and exp_fit_params_void_outer_void.dat. All these files
        hold the best fit parameter for the infall function for different subsamples of galaxies.
        """

        print("Starting gals_infall...")
        gal_void_file="gal_in_void_ID.dat"
        gal_voiddata = np.loadtxt(gal_void_file, usecols=0, dtype=np.int64)
        gal_rel_dist = np.loadtxt(gal_void_file, usecols=2, dtype=np.float)
        gal_frac_in_best_void = (
            np.loadtxt(gal_void_file, usecols=5, dtype=np.float) )

        if(0==len(gal_voiddata.shape)):
            gal_voiddata = np.array([gal_voiddata])
            gal_rel_dist = np.array([gal_rel_dist])
            gal_frac_in_best_void = np.array([gal_frac_in_best_void])
        print("gal_rel_dist=",gal_rel_dist)
        Ngals_non_void=0
        Ngals_void=0
        times=[]

        params_file_void = open("exp_fit_params_void.dat","w+")
        params_file_nonvoid = open("exp_fit_params_nonvoid.dat","w+")
        if(self.split_position_in_void):
            params_file_void_inner_void = open("exp_fit_params_void_inner_void.dat","w+")
            params_file_void_outer_void = open("exp_fit_params_void_outer_void.dat","w+")

        # First create a list with all times that occur in our data
        sage_outs= sage_dir + "../output/pegase_inputs/"
        for sage_infall_file in glob.iglob(sage_outs + '**/infall*.list', recursive=True):
            infall_data = np.loadtxt(sage_infall_file)
            for l in range(0,len(infall_data[:])):
                times.append(infall_data[l][0])
            time=sorted(set(times))
        del(times)
        n_time = len(time)
        print("n_time = ",n_time,"\ntime = ",time)

        halo_masses_all = np.array([])
        halo_masses_voids = np.array([])
        halo_masses_nonvoids = np.array([])

        formation_time_voids = np.array([])
        formation_time_nonvoids = np.array([])

        ## cumulators for main plottable galaxy infall characteristics
        voidgals_frac_in_void = np.array([])
        voidgals_rel_dist = np.array([])
        voidgals_infall_amp = np.array([])
        voidgals_infall_tau = np.array([])
        voidgals_size = np.array([])
        voidgals_spin = np.array([])
        voidgals_Rvir = np.array([])

        for sage_infall_file in glob.iglob(sage_outs + '**/infall*.list', recursive=True):
            k=0
            # Actually a np.empty is not enough here, we have to set all entries to zero
            if(Ngals_void==0):
                avg_infall_void=np.zeros(len(time))
            if(Ngals_non_void==0):
                avg_infall_non_void=np.zeros(len(time))
            tree_number = sage_infall_file.replace(sage_outs+'infall_', '').replace('.list', '')
            filename_add_outs_pos_rad = sage_dir + "../output/add_outs/" + "pos_rad_" + tree_number + ".list"
            print("filename_add_outs_pos_rad = ",filename_add_outs_pos_rad)
            galID = np.loadtxt(filename_add_outs_pos_rad, usecols=6, dtype=np.int64)
            if(galID.size > 1):
                this_gal_id = galID[0]
            else:
                this_gal_id = galID
            print("this_gal_id=",this_gal_id)
            infall_data = np.loadtxt(sage_infall_file)
            infall_void_id_file="infall_rate_%i.dat" % this_gal_id
            # search for the index in sage's output
            print("Index type check: type(G.GalaxyIndex), type(this_gal_id) = ",type(G.GalaxyIndex), type(this_gal_id))
            print("Index check: G.GalaxyIndex, this_gal_id = ",G.GalaxyIndex, this_gal_id)
            w = np.where(G.GalaxyIndex == this_gal_id)[0]
            print("w=",w)
            infall_mass=np.empty(len(time))
            n_time_per_file=len(infall_data[:])
            print("n_time = ",n_time,"; ",sage_infall_file,": n_time_per_file = ",n_time_per_file)
            in_void = 0
            formation_time=0.0

            with open(infall_void_id_file,'w+') as F:
                F.write("#(1) time in Gyr (2) infalling mass in M_sun/yr (3) galaxy id (4) is the galaxy at t=0 in a void; 0=no 1=yes\n")

                if(any(gal == this_gal_id for gal in gal_voiddata)):
                    in_void = 1
                    i_row_void=np.where(gal_voiddata == this_gal_id)
                    print("i_row_void=",i_row_void)
                    print("gal_voiddata =",gal_voiddata)
                    print("this_gal_id",this_gal_id)
                    print("gal_rel_dist[i_row_void]=",gal_rel_dist[i_row_void])
                for l in range(0,len(time),1):
                    if(k < n_time_per_file):
                        if(infall_data[k][0]-time[l]<1.e-5):
                            infall_mass[l] = infall_data[k][1] * convert_1010Msun_Myr_to_Msun_yr
                            k += 1
                            # save the time for the first infall event
                            if(formation_time == 0.0 and infall_mass[l] != 0.0):
                                formation_time = time[l]

                        else:
                            infall_mass[l] = 0.0 * convert_1010Msun_Myr_to_Msun_yr
                        F.write("%f %f %i %i\n" % (time[l]*convert_Myr_to_Gyr,
                                                   infall_mass[l], this_gal_id, in_void))
                        if(in_void==0 and G.Mvir[w]*1.0e10 > np.float(self.mass_gap[0]) and
                           G.Mvir[w]*1.0e10 < np.float(self.mass_gap[1])):
                            avg_infall_non_void[l] = avg_infall_non_void[l] + infall_mass[l]
                        if(in_void==1 and G.Mvir[w]*1.0e10 > np.float(self.mass_gap[0]) and
                           G.Mvir[w]*1.0e10 < np.float(self.mass_gap[1])):
                            avg_infall_void[l] = avg_infall_void[l] + infall_mass[l]

            # Create arrays of halo masses over all mass scales for void galaxies and non-void galaxies:
            if(in_void):
                halo_masses_voids = np.append(halo_masses_voids, G.Mvir[w]*1.0e10)
            else:
                halo_masses_nonvoids = np.append(halo_masses_nonvoids, G.Mvir[w]*1.0e10)
            halo_masses_all = np.append(halo_masses_all, G.Mvir[w]*1.0e10)

            if(G.Mvir[w]*1.0e10 > np.float(self.mass_gap[0]) and G.Mvir[w]*1.0e10 < np.float(self.mass_gap[1])):
                print("Will call infall_fit on: ",infall_void_id_file)
                popt = infall_fit(infall_void_id_file, do_plot=infall_fit_do_plot)
                if(in_void==0):
                    Ngals_non_void = Ngals_non_void + 1
                    params_file_nonvoid.write("%f %f\n" % (popt[0], popt[1]))
                if(in_void==1):
                    Ngals_void = Ngals_void + 1
                    params_file_void.write("%f %f\n" % (popt[0], popt[1]))
                    if(self.split_position_in_void):
                        if(gal_rel_dist[i_row_void] > self.inner_rad_threshold and
                           gal_rel_dist[i_row_void] < self.outer_rad_threshold):
                            params_file_void_inner_void.write("%f %f\n" % (popt[0], popt[1]))
                        if(gal_rel_dist[i_row_void] > self.outer_rad_threshold and
                           gal_rel_dist[i_row_void] < 3.0):
                            params_file_void_outer_void.write("%f %f\n" % (popt[0], popt[1]))


                ## print parameters for quick command line plots
                if(in_void):
                    if(verbose):
                        print("galaxy::: ",
                              in_void,
                              (gal_frac_in_best_void[i_row_void])[0],
                              (gal_rel_dist[i_row_void])[0],
                              popt[0],popt[1])
                    formation_time_voids = np.append(formation_time_voids, formation_time)
                    if( (np.isfinite(popt[0])) and (np.isfinite(popt[1])) ):
                        voidgals_frac_in_void = np.append(voidgals_frac_in_void,
                                                          (gal_frac_in_best_void[i_row_void])[0])
                        voidgals_rel_dist = np.append(voidgals_rel_dist,
                                                      (gal_rel_dist[i_row_void])[0])
                        voidgals_infall_amp = np.append(voidgals_infall_amp, popt[0])
                        voidgals_infall_tau = np.append(voidgals_infall_tau, popt[1])

                        voidgals_size = np.append(voidgals_size, G.DiskRadius[w]*1000)
                        voidgals_spin = np.append(voidgals_spin, np.sqrt(G.Spin[w,0]*G.Spin[w,0] + G.Spin[w,1]*G.Spin[w,1] +
                                                                         G.Spin[w,2]*G.Spin[w,2]) / (np.sqrt(2) * G.Vvir[w] *G.Rvir[w]))
                        voidgals_Rvir = np.append(voidgals_Rvir, G.Rvir[w]*1000)
                else:
                    print("galaxy::: ",
                          in_void,
                          0.0,
                          99.9,
                          popt[0],popt[1])
                    formation_time_nonvoids = np.append(formation_time_nonvoids, formation_time)


            # make plots
            if(gals_infall_do_plot and create_png_files):
                plt.figure()
                plt.ylabel(r'$M_\mathrm{infall} \mathrm{~~in~~ M_\odot}/h M_\mathrm{system} $')
                plt.xlabel(r'$t \mathrm{~~in~~ Myr}}$')
                if(in_void==1):
                    plt.plot(time,infall_mass,color='blue')
                else:
                    plt.plot(time,infall_mass,color='red')
                outputFile = 'infall_rate_%i.png' % this_gal_id
                plt.savefig(outputFile, metadata=self.metadata)  # Save the figure
                print('Saved file to', outputFile)
                plt.close()


        # Check host halo mass statistics
        halo_mass_voids_med, halo_mass_voids_sig, halo_mass_voids_stderr_med = (
            median_stats(halo_masses_voids))
        halo_mass_nonvoids_med, halo_mass_nonvoids_sig, halo_mass_nonvoids_stderr_med = (
            median_stats(halo_masses_nonvoids))
        formation_time_voids_med, formation_time_voids_sig, formation_time_voids_stderr_med = (
            median_stats(formation_time_voids))
        formation_time_nonvoids_med, formation_time_nonvoids_sig, formation_time_nonvoids_stderr_med = (
            median_stats(formation_time_nonvoids))

        print("halo_mass_voids_med, halo_mass_voids_sig, halo_mass_voids_stderr_med = ",
              halo_mass_voids_med, halo_mass_voids_sig, halo_mass_voids_stderr_med)
        print("halo_mass_nonvoids_med, halo_mass_nonvoids_sig, halo_mass_nonvoids_stderr_med = ",
              halo_mass_nonvoids_med, halo_mass_nonvoids_sig, halo_mass_nonvoids_stderr_med)
        print("formation_time_voids_med, formation_time_voids_sig, formation_time_voids_stderr_med = ",
              formation_time_voids_med, formation_time_voids_sig, formation_time_voids_stderr_med)
        print("formation_time_nonvoids_med, formation_time_nonvoids_sig, formation_time_nonvoids_stderr_med = ",
              formation_time_nonvoids_med, formation_time_nonvoids_sig, formation_time_nonvoids_stderr_med)

        Moutput_unit=1e11
        with open("halo_masses.dat","w+") as N_halos_file:
            N_halos_file.write("halo_mass_voids_med = %.3f\n" % (halo_mass_voids_med/Moutput_unit))
            N_halos_file.write("halo_mass_voids_stderr_med = %.3f\n" % (halo_mass_voids_stderr_med/Moutput_unit))
            N_halos_file.write("halo_mass_voids_sig = %.3f\n" % (halo_mass_voids_sig/Moutput_unit))
            N_halos_file.write("halo_mass_nonvoids_med = %.3f\n" % (halo_mass_nonvoids_med/Moutput_unit))
            N_halos_file.write("halo_mass_nonvoids_stderr_med = %.3f\n" % (halo_mass_nonvoids_stderr_med/Moutput_unit))
            N_halos_file.write("halo_mass_nonvoids_sig = %.3f\n" % (halo_mass_nonvoids_sig/Moutput_unit))
        N_halos_file.close()

        # create a plot of the mass distribution
        self.plot_halo_mass_dist(halo_masses_voids, halo_masses_nonvoids, halo_masses_all, self.Ncroot, self.massDM, self.BoxSize)

        T_formation_time_output_convert=1e3
        with open("formation_time.dat","w+") as Formation_file:
            Formation_file.write("formation_time_voids_med = %.3f\n" % (formation_time_voids_med/T_formation_time_output_convert))
            Formation_file.write("formation_time_voids_stderr_med = %.3f\n" % (formation_time_voids_stderr_med/T_formation_time_output_convert))
            Formation_file.write("formation_time_voids_sig = %.3f\n" % (formation_time_voids_sig/T_formation_time_output_convert))
            Formation_file.write("formation_time_nonvoids_med = %.3f\n" % (formation_time_nonvoids_med/T_formation_time_output_convert))
            Formation_file.write("formation_time_nonvoids_stderr_med = %.3f\n" % (formation_time_nonvoids_stderr_med/T_formation_time_output_convert))
            Formation_file.write("formation_time_nonvoids_sig = %.3f\n" % (formation_time_nonvoids_sig/T_formation_time_output_convert))
        Formation_file.close()


        voidgals_filename="voidgals_infall.dat"
        with open(voidgals_filename,"w+") as voidgals_file:
            voidgals_file.write("# Characteristics of galaxies in voids in selected mass range\n")
            voidgals_file.write("# All parameters are floats.\n")
            voidgals_file.write("# Column 1: frac_in_void   fraction of host halo particles that are in the void [1]\n")
            voidgals_file.write("# Column 2: rel_dist       elaphrocentric distance normalised by void effective radius = r/R_eff [1]\n")
            voidgals_file.write("# Column 3: infall_amp     amplitude of best-fit exponential infall rate history (0.0 or \gg 1 means a fail) [M_sun/yr]\n")
            voidgals_file.write("# Column 4: infall_tau     decay time scale of best-fit exponential infall rate history (0.0 or \gg 1 means a fail) [Gyr]\n")
            voidgals_file.write("# Column 5: size           galaxy disk scale length [kpc/h]\n")
            voidgals_file.write("# Column 6: spin           spin parameter [1]\n")
            voidgals_file.write("# Column 7: Rvir           virialisation radius of host halo [kpc/h]\n")
            voidgals_file.write("# [1] Units of '1' mean a dimensionless parameter.\n")
            for septuplet in zip(voidgals_frac_in_void,
                                  voidgals_rel_dist,
                                  voidgals_infall_amp,
                                  voidgals_infall_tau,
                                  voidgals_size,
                                  voidgals_spin,
                                  voidgals_Rvir):
                voidgals_file.write("%g %g %g %g %g %g %g\n" % septuplet)
        voidgals_file.close()

        (N_voidgals,
         voidgals_frac_in_void, voidgals_rel_dist,
         voidgals_infall_amp, voidgals_infall_tau,
         voidgals_size, voidgals_spin, voidgals_Rvir) = (
             voidgals_props_read_data(voidgals_filename) )

        voidgals_props_plot(N_voidgals,
                            voidgals_frac_in_void, voidgals_rel_dist,
                            voidgals_infall_amp, voidgals_infall_tau,
                            voidgals_size,
                            voidgals_spin, voidgals_Rvir,
                            maximum_allowed_fit_amplitude)

        # average is a too strong simplification
        avg_infall_non_void[:] = avg_infall_non_void[:]/Ngals_non_void
        avg_infall_void[:] = avg_infall_void[:]/Ngals_void
        #error_non_void = np.empty(len(avg_infall_non_void))
        #error_void = np.empty(len(avg_infall_void))
        #error_non_void[:] = np.sqrt()
        #error_void[:] = np.sqrt()

        # make plots
        ax=plt.figure()
        plt.ylabel('$\mathrm{infall-rate} \mathrm{~~in~~ M_\odot/}/h/M_\mathrm{system} $')
        plt.xlabel('$t \mathrm{~~in~~ Myr}}$')
        plt.plot(time,avg_infall_non_void,'r',label='non_void')
        #plt.fill_between(time, avg_infall_non_void-error_non_void, avg_infall_non_void+error_non_void,alpha=0.2,facecolor='red')
        plt.plot(time,avg_infall_void,'b',label='void')
        #plt.fill_between(time, avg_infall_void-error_void, avg_infall_void+error_void,alpha=0.2,facecolor='blue')
        plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
        plt.legend(loc='upper right', numpoints=1, labelspacing=0.1)
        outputFile = 'infall_rate_avg.eps'
        plt.savefig(outputFile, metadata=self.metadata)
        print('Saved file to', outputFile)

        plt.close()
        params_file_nonvoid.close()
        params_file_void.close()
        if(self.split_position_in_void):
            params_file_void_inner_void.close()
            params_file_void_outer_void.close()

        # better
        ## TODO: possible rename, or explain better -
        ## 'plot_histogram' does not only plot a histogram, but it
        ## also calculates key statistics that we use.
        plot_histogram("exp_fit_params_void.dat","exp_fit_params_nonvoid.dat","infall-rate","voids","non-voids",
                       maximum_allowed_fit_amplitude = maximum_allowed_fit_amplitude)
        if(self.split_position_in_void):
            plot_histogram("exp_fit_params_void_inner_void.dat","exp_fit_params_void_outer_void.dat","infall-rate","inner-void","outer-void",
                           maximum_allowed_fit_amplitude = maximum_allowed_fit_amplitude)


    def gals_sfr(self, G):
        """
        Read in the information about the star formation rate for every galaxy and fit it
        to a negative exponential A*exp(-B*t). This function calls routines from
        exptest.py. The routine will create several output files.
        It also calls infall_stat.py to estimate the mean and std of the parameters.

        Paramter
        ------------
        G: galaxy data

        returns
        ------------
        exp_fit_params_sfr_void.dat, exp_fit_params_sfr_nonvoid.dat and if 'split_position_in_void =True'
        exp_fit_params_sfr_void_inner_void.dat and exp_fit_params_sfr_void_outer_void.dat. All these files
        hold the best fit parameter for the infall function for different subsamples of galaxies.
        """

        print("Starting gals_sfr...")
        gal_void_file="gal_in_void_ID.dat"
        gal_voiddata = np.loadtxt(gal_void_file, usecols=0, dtype=np.int64)
        gal_rel_dist = np.loadtxt(gal_void_file, usecols=2, dtype=np.float)

        if(0==len(gal_voiddata.shape)):
            gal_voiddata = np.array([gal_voiddata])
            gal_rel_dist = np.array([gal_rel_dist])

        print("gal_rel_dist=",gal_rel_dist)
        Ngals_non_void=0
        Ngals_void=0
        times=[]

        have_gal_void_fits = False
        have_gal_non_void_fits = False
        tol_popt = 1e-20 # tolerance for checking if fit failed

        params_file_void = open("exp_fit_params_sfr_void.dat","w+")
        params_file_nonvoid = open("exp_fit_params_sfr_nonvoid.dat","w+")
        if(self.split_position_in_void):
            params_file_void_inner_void = open("exp_fit_params_sfr_void_inner_void.dat","w+")
            params_file_void_outer_void = open("exp_fit_params_sfr_void_outer_void.dat","w+")

        # First create a list with all times that occur in our data
        sage_outs= sage_dir + "../output/pegase_inputs/"
        for filename in glob.iglob(sage_outs + '**/sfr*.list', recursive=True):
            sfr_data = np.loadtxt(filename)
            for l in range(0,len(sfr_data[:])):
                times.append(sfr_data[l][0])
            time=sorted(set(times))
        del(times)
        n_time = len(time)
        print("n_time = ",n_time,"\ntime = ",time)

        for filename in glob.iglob(sage_outs + '**/sfr*.list', recursive=True):
            k=0
            # Actually a np.empty is not enough here, we have to set all entries to zero
            if(Ngals_void==0):
                avg_sfr_void=np.zeros(len(time))
            if(Ngals_non_void==0):
                avg_sfr_non_void=np.zeros(len(time))
            tree_number=filename.replace(sage_outs+'sfr_','').replace('.list','')
            filename_add_outs_pos_rad = sage_dir + "../output/add_outs/" + "pos_rad_" + tree_number + ".list"
            print("filename_add_outs_pos_rad = ",filename_add_outs_pos_rad)
            galID = np.loadtxt(filename_add_outs_pos_rad, usecols=6, dtype=np.int64)
            if(galID.size > 1):
                this_gal_id = galID[0]
            else:
                this_gal_id = galID
            print("this_gal_id=",this_gal_id)
            sfr_data = np.loadtxt(filename)
            outfile="sfr_%i.dat" % this_gal_id
            # search for the index in sage's output
            if(verbose):
                print("Index type check: type(G.GalaxyIndex), type(this_gal_id) = ",type(G.GalaxyIndex), type(this_gal_id))
                print("Index check: G.GalaxyIndex, this_gal_id = ",G.GalaxyIndex, this_gal_id)
            w = np.array(np.where(G.GalaxyIndex == this_gal_id))[0]
            print("w=",w)
            sfr=np.empty(len(time))
            n_time_per_file=len(sfr_data[:])
            print("n_time = ",n_time,"; ",filename,": n_time_per_file = ",n_time_per_file)
            with open(outfile,'w+') as F:
                F.write("#(1) time in Myr (2)sfr in M_sun/h/Myr/M_tot (3)galaxy id (4)is the galaxy at t=0 in a void; 0=no 1=yes\n")
                in_void = 0
                if(any(gal == this_gal_id for gal in gal_voiddata)):
                    in_void = 1
                    void_id=np.where(gal_voiddata == this_gal_id)
                    print("void_id=",void_id)
                    print("gal_voiddata =",gal_voiddata)
                    print("this_gal_id",this_gal_id)
                    print("gal_rel_dist[void_id]=",gal_rel_dist[void_id])
                for l in range(0,len(time),1):
                    if(k < n_time_per_file):
                        if(sfr_data[k][0]-time[l]<1.e-5):
                            sfr[l] = sfr_data[k][1]
                            k += 1
                        else:
                            sfr[l] = 0.0
                        F.write("%f %f %i %i\n" % (time[l], sfr[l],this_gal_id,in_void))
                        if(in_void==0 and G.Mvir[w]*1.0e10 > np.float(self.mass_gap[0]) and
                           G.Mvir[w]*1.0e10 < np.float(self.mass_gap[1])):
                            avg_sfr_non_void[l] = avg_sfr_non_void[l] + sfr[l]
                        if(in_void==1 and G.Mvir[w]*1.0e10 > np.float(self.mass_gap[0]) and
                           G.Mvir[w]*1.0e10 < np.float(self.mass_gap[1])):
                            avg_sfr_void[l] = avg_sfr_void[l] + sfr[l]
            if(G.Mvir[w]*1.0e10 > np.float(self.mass_gap[0]) and G.Mvir[w]*1.0e10 < np.float(self.mass_gap[1])):
                print("Will call infall_fit on: ",filename)
                popt = infall_fit(filename)
                if(in_void==0):
                    Ngals_non_void = Ngals_non_void + 1
                    params_file_nonvoid.write("%f %f\n" % (popt[0], popt[1]))
                    if((popt[0] > tol_popt) and (popt[1] > tol_popt)):
                        have_gal_non_void_fits = True
                if(in_void==1):
                    Ngals_void = Ngals_void + 1
                    params_file_void.write("%f %f\n" % (popt[0], popt[1]))
                    if((popt[0] > tol_popt) and (popt[1] > tol_popt)):
                        have_gal_void_fits = True
                    if(self.split_position_in_void):
                        if(gal_rel_dist[void_id] > 0.6 and gal_rel_dist[void_id] < 0.7):
                            params_file_void_inner_void.write("%f %f\n" % (popt[0], popt[1]))
                        if(gal_rel_dist[void_id] > 0.7 and gal_rel_dist[void_id] < 1.0):
                            params_file_void_outer_void.write("%f %f\n" % (popt[0], popt[1]))

            # make plots
            if(gals_sfr_do_plot and create_png_files):
                plt.figure()
                plt.ylabel(r'$M_\mathrm{sfr} \mathrm{~~in~~ M_\odot}/h M_\mathrm{system} $')
                plt.xlabel(r'$t \mathrm{~~in~~ Myr}}$')
                if(in_void==1):
                    plt.plot(time,sfr,color='blue')
                else:
                    plt.plot(time,sfr,color='red')
                outputFile = 'sfr_%i.png' % this_gal_id
                plt.savefig(outputFile, metadata=self.metadata)  # Save the figure
                print('Saved file to', outputFile)
                plt.close()

        # average is a too strong simplification
        avg_sfr_non_void[:] = avg_sfr_non_void[:]/Ngals_non_void
        avg_sfr_void[:] = avg_sfr_void[:]/Ngals_void
        #error_non_void = np.empty(len(avg_infall_non_void))
        #error_void = np.empty(len(avg_infall_void))
        #error_non_void[:] = np.sqrt()
        #error_void[:] = np.sqrt()

        # make plots
        ax=plt.figure()
        plt.ylabel('$\mathrm{sfr} \mathrm{~~in~~ M_\odot/}/h/M_\mathrm{system} $')
        plt.xlabel('$t \mathrm{~~in~~ Myr}}$')
        plt.plot(time,avg_sfr_non_void,'r',label='non_void')
        #plt.fill_between(time, avg_infall_non_void-error_non_void, avg_infall_non_void+error_non_void,alpha=0.2,facecolor='red')
        plt.plot(time,avg_sfr_void,'b',label='void')
        #plt.fill_between(time, avg_infall_void-error_void, avg_infall_void+error_void,alpha=0.2,facecolor='blue')
        plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
        plt.legend(loc='upper right', numpoints=1, labelspacing=0.1)
        outputFile = 'sfr_avg.eps'
        plt.savefig(outputFile, metadata=self.metadata)
        print('Saved file to', outputFile)
        plt.close()
        params_file_nonvoid.close()
        params_file_void.close()
        if(self.split_position_in_void):
            params_file_void_inner_void.close()
            params_file_void_outer_void.close()

        # better (TODO: explanation needed)
        if(have_gal_void_fits and have_gal_non_void_fits):
            plot_histogram("exp_fit_params_sfr_void.dat","exp_fit_params_sfr_nonvoid.dat","sfr","voids","non-voids",
                           maximum_allowed_fit_amplitude = maximum_allowed_fit_amplitude)
            if(self.split_position_in_void):
                plot_histogram("exp_fit_params_sfr_void_inner_void.dat","exp_fit_params_sfr_void_outer_void.dat","sfr","inner-void","outer-void",
                               maximum_allowed_fit_amplitude = maximum_allowed_fit_amplitude)



# --------------------------------------------------------

    def StellarMassFunction(self, G, w_lsbg, w_void_lsbg):

        print('Plotting the stellar mass function')

        plt.figure()  # New figure
        ax = plt.subplot(111)  # 1 plot on the figure

        binwidth = 0.1  # mass function histogram bin width

        # calculate all
        w = np.where(G.StellarMass > 0.0)[0]

        mass = np.log10(G.StellarMass[w] * 1.0e10 / self.Hubble_h)
        mass_lsbg = np.log10(G.StellarMass[w_lsbg] * 1.0e10 / self.Hubble_h)
        mass_void_lsbg = np.log10(G.StellarMass[w_void_lsbg] * 1.0e10 / self.Hubble_h)
        sSFR = (G.SfrDisk[w] + G.SfrBulge[w]) / (G.StellarMass[w] * 1.0e10 / self.Hubble_h)

        mi = np.floor(min(mass)) - 2
        ma = np.floor(max(mass)) + 2
        NB = np.int((ma - mi) / binwidth)

        (counts, binedges) = np.histogram(mass, range=(mi, ma), bins=NB)
        (counts_lsbg, binedges) = np.histogram(mass_lsbg, range=(mi, ma), bins=NB)
        (counts_void_lsbg, binedges) = np.histogram(mass_void_lsbg, range=(mi, ma), bins=NB)

        # Set the x-axis values to be the centre of the bins
        xaxeshisto = binedges[:-1] + 0.5 * binwidth

        # additionally calculate red
        w = np.where(sSFR < 10.0**sSFRcut)[0]
        massRED = mass[w]
        (countsRED, binedges) = np.histogram(massRED, range=(mi, ma), bins=NB)

        # additionally calculate blue
        w = np.where(sSFR > 10.0**sSFRcut)[0]
        massBLU = mass[w]
        (countsBLU, binedges) = np.histogram(massBLU, range=(mi, ma), bins=NB)

        # Baldry+ 2008 modified data used for the MCMC fitting
        Baldry = np.array([
            [7.05, 1.3531e-01, 6.0741e-02],
            [7.15, 1.3474e-01, 6.0109e-02],
            [7.25, 2.0971e-01, 7.7965e-02],
            [7.35, 1.7161e-01, 3.1841e-02],
            [7.45, 2.1648e-01, 5.7832e-02],
            [7.55, 2.1645e-01, 3.9988e-02],
            [7.65, 2.0837e-01, 4.8713e-02],
            [7.75, 2.0402e-01, 7.0061e-02],
            [7.85, 1.5536e-01, 3.9182e-02],
            [7.95, 1.5232e-01, 2.6824e-02],
            [8.05, 1.5067e-01, 4.8824e-02],
            [8.15, 1.3032e-01, 2.1892e-02],
            [8.25, 1.2545e-01, 3.5526e-02],
            [8.35, 9.8472e-02, 2.7181e-02],
            [8.45, 8.7194e-02, 2.8345e-02],
            [8.55, 7.0758e-02, 2.0808e-02],
            [8.65, 5.8190e-02, 1.3359e-02],
            [8.75, 5.6057e-02, 1.3512e-02],
            [8.85, 5.1380e-02, 1.2815e-02],
            [8.95, 4.4206e-02, 9.6866e-03],
            [9.05, 4.1149e-02, 1.0169e-02],
            [9.15, 3.4959e-02, 6.7898e-03],
            [9.25, 3.3111e-02, 8.3704e-03],
            [9.35, 3.0138e-02, 4.7741e-03],
            [9.45, 2.6692e-02, 5.5029e-03],
            [9.55, 2.4656e-02, 4.4359e-03],
            [9.65, 2.2885e-02, 3.7915e-03],
            [9.75, 2.1849e-02, 3.9812e-03],
            [9.85, 2.0383e-02, 3.2930e-03],
            [9.95, 1.9929e-02, 2.9370e-03],
            [10.05, 1.8865e-02, 2.4624e-03],
            [10.15, 1.8136e-02, 2.5208e-03],
            [10.25, 1.7657e-02, 2.4217e-03],
            [10.35, 1.6616e-02, 2.2784e-03],
            [10.45, 1.6114e-02, 2.1783e-03],
            [10.55, 1.4366e-02, 1.8819e-03],
            [10.65, 1.2588e-02, 1.8249e-03],
            [10.75, 1.1372e-02, 1.4436e-03],
            [10.85, 9.1213e-03, 1.5816e-03],
            [10.95, 6.1125e-03, 9.6735e-04],
            [11.05, 4.3923e-03, 9.6254e-04],
            [11.15, 2.5463e-03, 5.0038e-04],
            [11.25, 1.4298e-03, 4.2816e-04],
            [11.35, 6.4867e-04, 1.6439e-04],
            [11.45, 2.8294e-04, 9.9799e-05],
            [11.55, 1.0617e-04, 4.9085e-05],
            [11.65, 3.2702e-05, 2.4546e-05],
            [11.75, 1.2571e-05, 1.2571e-05],
            [11.85, 8.4589e-06, 8.4589e-06],
            [11.95, 7.4764e-06, 7.4764e-06],
            ], dtype=np.float32)

        # Finally plot the data
        # plt.errorbar(
        #     Baldry[:, 0],
        #     Baldry[:, 1],
        #     yerr=Baldry[:, 2],
        #     color='g',
        #     linestyle=':',
        #     lw = 1.5,
        #     label='Baldry et al. 2008',
        #     )

        Baldry_xval = np.log10(10 ** Baldry[:, 0]  /self.Hubble_h/self.Hubble_h)
        if(whichimf == 1):  Baldry_xval = Baldry_xval - 0.26  # convert back to Chabrier IMF
        Baldry_yvalU = (Baldry[:, 1]+Baldry[:, 2]) * self.Hubble_h*self.Hubble_h*self.Hubble_h
        Baldry_yvalL = (Baldry[:, 1]-Baldry[:, 2]) * self.Hubble_h*self.Hubble_h*self.Hubble_h

        plt.fill_between(Baldry_xval, Baldry_yvalU, Baldry_yvalL,
            facecolor='purple', alpha=0.25, label='Baldry et al. 2008 (z=0.1)')

        # This next line is just to get the shaded region to appear correctly in the legend
        #plt.plot(xaxeshisto, counts / self.volume * self.Hubble_h*self.Hubble_h*self.Hubble_h / binwidth, label='Baldry et al. 2008', color='purple', alpha=0.3)

        # # Cole et al. 2001 SMF (h=1.0 converted to h=0.73)
        # M = np.arange(7.0, 13.0, 0.01)
        # Mstar = np.log10(7.07*1.0e10 /self.Hubble_h/self.Hubble_h)
        # alpha = -1.18
        # phistar = 0.009 *self.Hubble_h*self.Hubble_h*self.Hubble_h
        # xval = 10.0 ** (M-Mstar)
        # yval = np.log(10.) * phistar * xval ** (alpha+1) * np.exp(-xval)
        # plt.plot(M, yval, 'g--', lw=1.5, label='Cole et al. 2001')  # Plot the SMF

        # Overplot the model histograms
        plt.plot(xaxeshisto, counts    / self.volume * self.Hubble_h*self.Hubble_h*self.Hubble_h / binwidth, 'k-', label='Model - All')
        plt.plot(xaxeshisto, counts_lsbg    / self.volume * self.Hubble_h*self.Hubble_h*self.Hubble_h / binwidth, 'r', label='LSBGs - All')
        plt.plot(xaxeshisto, counts_void_lsbg    / self.volume * self.Hubble_h*self.Hubble_h*self.Hubble_h / binwidth, 'b', label='LSBGs - Voids')

        plt.yscale('log', nonposy='clip')
        plt.axis([6.0, 12.5, 1.0e-6, 1.0e-1])

        # Set the x-axis minor ticks
        ax.xaxis.set_minor_locator(plt.MultipleLocator(0.1))

        plt.ylabel(r'$\phi\ (\mathrm{Mpc}^{-3}\ \mathrm{dex}^{-1})$')  # Set the y...
        plt.xlabel(r'$\log_{10} M_{\mathrm{stars}}\ (M_{\odot})$')  # and the x-axis labels

        plt.text(12.2, 0.03, whichsimulation, size = 'large')

        leg = plt.legend(loc='lower left', numpoints=1,
                         labelspacing=0.1)
        leg.draw_frame(False)  # Don't want a box frame
        for t in leg.get_texts():  # Reduce the size of the text
            t.set_fontsize('medium')

        outputFile = OutputDir + '1.StellarMassFunction' + OutputFormat
        plt.savefig(outputFile)  # Save the figure
        print('Saved file to', outputFile)
        plt.close()

        # Add this plot to our output list
        OutputList.append(outputFile)


# ---------------------------------------------------------

    def BaryonicMassFunction(self, G, w_lsbg, w_void_lsbg):

        print('Plotting the baryonic mass function')

        plt.figure()  # New figure
        ax = plt.subplot(111)  # 1 plot on the figure

        binwidth = 0.1  # mass function histogram bin width

        # calculate BMF
        w = np.where(G.StellarMass + G.ColdGas > 0.0)[0]
        mass = np.log10((G.StellarMass[w] + G.ColdGas[w]) * 1.0e10 / self.Hubble_h)
        mass_lsbg = np.log10((G.StellarMass[w_lsbg] + G.ColdGas[w_lsbg]) * 1.0e10 / self.Hubble_h)
        mass_void_lsbg = np.log10((G.StellarMass[w_void_lsbg] + G.ColdGas[w_void_lsbg]) * 1.0e10 / self.Hubble_h)

        mi = np.floor(min(mass)) - 2
        ma = np.floor(max(mass)) + 2
        NB = np.int((ma - mi) / binwidth)

        (counts, binedges) = np.histogram(mass, range=(mi, ma), bins=NB)
        (counts_lsbg, binedges) = np.histogram(mass_lsbg, range=(mi, ma), bins=NB)
        (counts_void_lsbg, binedges) = np.histogram(mass_void_lsbg, range=(mi, ma), bins=NB)

        # Set the x-axis values to be the centre of the bins
        xaxeshisto = binedges[:-1] + 0.5 * binwidth

        # Bell et al. 2003 BMF (h=1.0 converted to h=0.73)
        M = np.arange(7.0, 13.0, 0.01)
        Mstar = np.log10(5.3*1.0e10 /self.Hubble_h/self.Hubble_h)
        alpha = -1.21
        phistar = 0.0108 *self.Hubble_h*self.Hubble_h*self.Hubble_h
        xval = 10.0 ** (M-Mstar)
        yval = np.log(10.) * phistar * xval ** (alpha+1) * np.exp(-xval)

        if(whichimf == 0):
            # converted diet Salpeter IMF to Salpeter IMF
            plt.plot(np.log10(10.0**M /0.7), yval, 'b-', lw=2.0, label='Bell et al. 2003')  # Plot the SMF
        elif(whichimf == 1):
            # converted diet Salpeter IMF to Salpeter IMF, then to Chabrier IMF
            plt.plot(np.log10(10.0**M /0.7 /1.8), yval, 'g--', lw=1.5, label='Bell et al. 2003')  # Plot the SMF

        # Overplot the model histograms
        plt.plot(xaxeshisto, counts / self.volume * self.Hubble_h*self.Hubble_h*self.Hubble_h / binwidth, 'k-', label='Model -All')
        plt.plot(xaxeshisto, counts_lsbg / self.volume * self.Hubble_h*self.Hubble_h*self.Hubble_h / binwidth, 'r', label='LSBGs')
        plt.plot(xaxeshisto, counts_void_lsbg    / self.volume * self.Hubble_h*self.Hubble_h*self.Hubble_h / binwidth, 'b', label='LSBGs - Voids')

        plt.yscale('log', nonposy='clip')
        plt.axis([8.0, 12.5, 1.0e-6, 1.0e-1])

        # Set the x-axis minor ticks
        ax.xaxis.set_minor_locator(plt.MultipleLocator(0.1))

        plt.ylabel(r'$\phi\ (\mathrm{Mpc}^{-3}\ \mathrm{dex}^{-1})$')  # Set the y...
        plt.xlabel(r'$\log_{10}\ M_{\mathrm{bary}}\ (M_{\odot})$')  # and the x-axis labels

        leg = plt.legend(loc='lower left', numpoints=1,
                         labelspacing=0.1)
        leg.draw_frame(False)  # Don't want a box frame
        for t in leg.get_texts():  # Reduce the size of the text
            t.set_fontsize('medium')

        outputFile = OutputDir + '2.BaryonicMassFunction' + OutputFormat
        plt.savefig(outputFile)  # Save the figure
        print('Saved file to', outputFile)
        plt.close()

        # Add this plot to our output list
        OutputList.append(outputFile)


# ---------------------------------------------------------

    def GasMassFunction(self, G):

        print('Plotting the cold gas mass function')

        plt.figure()  # New figure
        ax = plt.subplot(111)  # 1 plot on the figure

        binwidth = 0.1  # mass function histogram bin width

        # calculate all
        w = np.where(G.ColdGas > 0.0)[0]
        mass = np.log10(G.ColdGas[w] * 1.0e10 / self.Hubble_h)
        sSFR = (G.SfrDisk[w] + G.SfrBulge[w]) / (G.StellarMass[w] * 1.0e10 / self.Hubble_h)
        mi = np.floor(min(mass)) - 2
        ma = np.floor(max(mass)) + 2
        NB = np.int((ma - mi) / binwidth)

        (counts, binedges) = np.histogram(mass, range=(mi, ma), bins=NB)

        # Set the x-axis values to be the centre of the bins
        xaxeshisto = binedges[:-1] + 0.5 * binwidth

        # additionally calculate red
        w = np.where(sSFR < 10.0**sSFRcut)[0]
        massRED = mass[w]
        (countsRED, binedges) = np.histogram(massRED, range=(mi, ma), bins=NB)

        # additionally calculate blue
        w = np.where(sSFR > 10.0**sSFRcut)[0]
        massBLU = mass[w]
        (countsBLU, binedges) = np.histogram(massBLU, range=(mi, ma), bins=NB)

        # Baldry+ 2008 modified data used for the MCMC fitting
        Zwaan = np.array([[6.933,   -0.333],
            [7.057,   -0.490],
            [7.209,   -0.698],
            [7.365,   -0.667],
            [7.528,   -0.823],
            [7.647,   -0.958],
            [7.809,   -0.917],
            [7.971,   -0.948],
            [8.112,   -0.927],
            [8.263,   -0.917],
            [8.404,   -1.062],
            [8.566,   -1.177],
            [8.707,   -1.177],
            [8.853,   -1.312],
            [9.010,   -1.344],
            [9.161,   -1.448],
            [9.302,   -1.604],
            [9.448,   -1.792],
            [9.599,   -2.021],
            [9.740,   -2.406],
            [9.897,   -2.615],
            [10.053,  -3.031],
            [10.178,  -3.677],
            [10.335,  -4.448],
            [10.492,  -5.083]        ], dtype=np.float32)

        ObrRaw = np.array([
            [7.300,   -1.104],
            [7.576,   -1.302],
            [7.847,   -1.250],
            [8.133,   -1.240],
            [8.409,   -1.344],
            [8.691,   -1.479],
            [8.956,   -1.792],
            [9.231,   -2.271],
            [9.507,   -3.198],
            [9.788,   -5.062 ]        ], dtype=np.float32)

        ObrCold = np.array([
            [8.009,   -1.042],
            [8.215,   -1.156],
            [8.409,   -0.990],
            [8.604,   -1.156],
            [8.799,   -1.208],
            [9.020,   -1.333],
            [9.194,   -1.385],
            [9.404,   -1.552],
            [9.599,   -1.677],
            [9.788,   -1.812],
            [9.999,   -2.312],
            [10.172,  -2.656],
            [10.362,  -3.500],
            [10.551,  -3.635],
            [10.740,  -5.010]        ], dtype=np.float32)

        ObrCold_xval = np.log10(10**(ObrCold[:, 0])  /self.Hubble_h/self.Hubble_h)
        ObrCold_yval = (10**(ObrCold[:, 1]) * self.Hubble_h*self.Hubble_h*self.Hubble_h)
        Zwaan_xval = np.log10(10**(Zwaan[:, 0]) /self.Hubble_h/self.Hubble_h)
        Zwaan_yval = (10**(Zwaan[:, 1]) * self.Hubble_h*self.Hubble_h*self.Hubble_h)
        ObrRaw_xval = np.log10(10**(ObrRaw[:, 0])  /self.Hubble_h/self.Hubble_h)
        ObrRaw_yval = (10**(ObrRaw[:, 1]) * self.Hubble_h*self.Hubble_h*self.Hubble_h)

        plt.plot(ObrCold_xval, ObrCold_yval, color='black', lw = 7, alpha=0.25, label='Obr. \& Raw. 2009 (Cold Gas)')
        plt.plot(Zwaan_xval, Zwaan_yval, color='cyan', lw = 7, alpha=0.25, label='Zwaan et al. 2005 (HI)')
        plt.plot(ObrRaw_xval, ObrRaw_yval, color='magenta', lw = 7, alpha=0.25, label='Obr. \& Raw. 2009 (H2)')


        # Overplot the model histograms
        plt.plot(xaxeshisto, counts    / self.volume * self.Hubble_h*self.Hubble_h*self.Hubble_h / binwidth, 'k-', label='Model - Cold Gas')

        plt.yscale('log', nonposy='clip')
        plt.axis([8.0, 11.5, 1.0e-6, 1.0e-1])

        # Set the x-axis minor ticks
        ax.xaxis.set_minor_locator(plt.MultipleLocator(0.1))

        plt.ylabel(r'$\phi\ (\mathrm{Mpc}^{-3}\ \mathrm{dex}^{-1})$')  # Set the y...
        plt.xlabel(r'$\log_{10} M_{\mathrm{X}}\ (M_{\odot})$')  # and the x-axis labels

        leg = plt.legend(loc='lower left', numpoints=1,
                         labelspacing=0.1)
        leg.draw_frame(False)  # Don't want a box frame
        for t in leg.get_texts():  # Reduce the size of the text
            t.set_fontsize('medium')

        outputFile = OutputDir + '3.GasMassFunction' + OutputFormat
        plt.savefig(outputFile)  # Save the figure
        print('Saved file to', outputFile)
        plt.close()

        # Add this plot to our output list
        OutputList.append(outputFile)


# ---------------------------------------------------------

    def BaryonicTullyFisher(self, G):

        print('Plotting the baryonic TF relationship')

        seed(2222)

        plt.figure()  # New figure
        ax = plt.subplot(111)  # 1 plot on the figure

        # w = np.where((G.Type == 0) & (G.StellarMass + G.ColdGas > 0.0) & (G.Vmax > 0.0))[0]
        w = np.where((G.Type == 0) & (G.StellarMass + G.ColdGas > 0.0) &
          (G.BulgeMass / G.StellarMass > 0.1) & (G.BulgeMass / G.StellarMass < 0.5))[0]
        if(len(w) > dilute): w = sample(w, dilute)

        mass = np.log10((G.StellarMass[w] + G.ColdGas[w]) * 1.0e10 / self.Hubble_h)
        vel = np.log10(G.Vmax[w])

        plt.scatter(vel, mass, marker='o', s=1, c='k', alpha=0.5, label='Model Sb/c galaxies')

        # overplot Stark, McGaugh & Swatters 2009 (assumes h=0.75? ... what IMF?)
        w = np.arange(0.5, 10.0, 0.5)
        TF = 3.94*w + 1.79
        plt.plot(w, TF, 'b-', lw=2.0, label='Stark, McGaugh \& Swatters 2009')

        plt.ylabel(r'$\log_{10}\ M_{\mathrm{bar}}\ (M_{\odot})$')  # Set the y...
        plt.xlabel(r'$\log_{10}V_{max}\ (km/s)$')  # and the x-axis labels

        # Set the x and y axis minor ticks
        ax.xaxis.set_minor_locator(plt.MultipleLocator(0.05))
        ax.yaxis.set_minor_locator(plt.MultipleLocator(0.25))

        plt.axis([1.4, 2.6, 8.0, 12.0])

        leg = plt.legend(loc='lower right')
        leg.draw_frame(False)  # Don't want a box frame
        for t in leg.get_texts():  # Reduce the size of the text
            t.set_fontsize('medium')

        outputFile = OutputDir + '4.BaryonicTullyFisher' + OutputFormat
        plt.savefig(outputFile)  # Save the figure
        print('Saved file to', outputFile)
        plt.close()

        # Add this plot to our output list
        OutputList.append(outputFile)


# ---------------------------------------------------------

    def SpecificStarFormationRate(self, G, w_lsbg):

        print('Plotting the specific SFR')

        seed(2222)

        plt.figure()  # New figure
        ax = plt.subplot(111)  # 1 plot on the figure

        w = np.where(G.StellarMass > 0.01)[0]
        if(len(w) > dilute): w = sample(w, dilute)

        #mass = np.log10(G.StellarMass[w] * 1.0e10 / self.Hubble_h)
        sSFR = np.log10( (G.SfrDisk[w] + G.SfrBulge[w]) / (G.StellarMass[w] * 1.0e10 / self.Hubble_h) )
        #plt.scatter(mass, sSFR, marker='o', s=1, c='k', alpha=0.5, label='Model galaxies')

        mass_lsbg = np.log10(G.StellarMass[w_lsbg] * 1.0e10 / self.Hubble_h)
        sSFR_lsbg = np.log10( (G.SfrDisk[w_lsbg] + G.SfrBulge[w_lsbg]) / (G.StellarMass[w_lsbg] * 1.0e10 / self.Hubble_h) )
        plt.scatter(mass_lsbg, sSFR_lsbg, marker='o', s=1, c='r', alpha=1., label='LSBGs')

        # overplot dividing line between SF and passive
        w = np.arange(7.0, 13.0, 1.0)
        plt.plot(w, w/w*sSFRcut, 'b:', lw=2.0)

        plt.ylabel(r'$\log_{10}\ s\mathrm{SFR}\ (\mathrm{yr^{-1}})$')  # Set the y...
        plt.xlabel(r'$\log_{10} M_{\mathrm{stars}}\ (M_{\odot})$')  # and the x-axis labels

        # Set the x and y axis minor ticks
        ax.xaxis.set_minor_locator(plt.MultipleLocator(0.05))
        ax.yaxis.set_minor_locator(plt.MultipleLocator(0.25))

        plt.axis([8.0, 12.0, -30.0, -8.0])

        leg = plt.legend(loc='lower right')
        leg.draw_frame(False)  # Don't want a box frame
        for t in leg.get_texts():  # Reduce the size of the text
            t.set_fontsize('medium')

        outputFile = OutputDir + '5.SpecificStarFormationRate' + OutputFormat
        plt.savefig(outputFile)  # Save the figure
        print('Saved file to', outputFile)
        plt.close()

        # Add this plot to our output list
        OutputList.append(outputFile)


# ---------------------------------------------------------

    def GasFraction(self, G):

        print('Plotting the gas fractions')

        seed(2222)

        plt.figure()  # New figure
        ax = plt.subplot(111)  # 1 plot on the figure

        w = np.where((G.Type == 0) & (G.StellarMass + G.ColdGas > 0.0) &
          (G.BulgeMass / G.StellarMass > 0.1) & (G.BulgeMass / G.StellarMass < 0.5))[0]
        if(len(w) > dilute): w = sample(w, dilute)

        mass = np.log10(G.StellarMass[w] * 1.0e10 / self.Hubble_h)
        fraction = G.ColdGas[w] / (G.StellarMass[w] + G.ColdGas[w])

        plt.scatter(mass, fraction, marker='o', s=1, c='k', alpha=0.5, label='Model Sb/c galaxies')

        plt.ylabel(r'$\mathrm{Cold\ Mass\ /\ (Cold+Stellar\ Mass)}$')  # Set the y...
        plt.xlabel(r'$\log_{10} M_{\mathrm{stars}}\ (M_{\odot})$')  # and the x-axis labels

        # Set the x and y axis minor ticks
        ax.xaxis.set_minor_locator(plt.MultipleLocator(0.05))
        ax.yaxis.set_minor_locator(plt.MultipleLocator(0.25))

        plt.axis([8.0, 12.0, 0.0, 1.0])

        leg = plt.legend(loc='upper right')
        leg.draw_frame(False)  # Don't want a box frame
        for t in leg.get_texts():  # Reduce the size of the text
            t.set_fontsize('medium')

        outputFile = OutputDir + '6.GasFraction' + OutputFormat
        plt.savefig(outputFile)  # Save the figure
        print('Saved file to', outputFile)
        plt.close()

        # Add this plot to our output list
        OutputList.append(outputFile)


# ---------------------------------------------------------

    def Metallicity(self, G):

        print('Plotting the metallicities')

        seed(2222)

        plt.figure()  # New figure
        ax = plt.subplot(111)  # 1 plot on the figure

        w = np.where((G.Type == 0) & (G.ColdGas / (G.StellarMass + G.ColdGas) > 0.1) & (G.StellarMass > 0.01))[0]
        if(len(w) > dilute): w = sample(w, dilute)

        mass = np.log10(G.StellarMass[w] * 1.0e10 / self.Hubble_h)
        Z = np.log10((G.MetalsColdGas[w] / G.ColdGas[w]) / 0.02) + 9.0

        plt.scatter(mass, Z, marker='o', s=1, c='k', alpha=0.5, label='Model galaxies')

        # overplot Tremonti et al. 2003 (h=0.7)
        w = np.arange(7.0, 13.0, 0.1)
        Zobs = -1.492 + 1.847*w - 0.08026*w*w
        if(whichimf == 0):
            # Conversion from Kroupa IMF to Slapeter IMF
            plt.plot(np.log10((10**w *1.5)), Zobs, 'b-', lw=2.0, label='Tremonti et al. 2003')
        elif(whichimf == 1):
            # Conversion from Kroupa IMF to Slapeter IMF to Chabrier IMF
            plt.plot(np.log10((10**w *1.5 /1.8)), Zobs, 'b-', lw=2.0, label='Tremonti et al. 2003')

        plt.ylabel(r'$12\ +\ \log_{10}[\mathrm{O/H}]$')  # Set the y...
        plt.xlabel(r'$\log_{10} M_{\mathrm{stars}}\ (M_{\odot})$')  # and the x-axis labels

        # Set the x and y axis minor ticks
        ax.xaxis.set_minor_locator(plt.MultipleLocator(0.05))
        ax.yaxis.set_minor_locator(plt.MultipleLocator(0.25))

        plt.axis([8.0, 12.0, 8.0, 9.5])

        leg = plt.legend(loc='lower right')
        leg.draw_frame(False)  # Don't want a box frame
        for t in leg.get_texts():  # Reduce the size of the text
            t.set_fontsize('medium')

        outputFile = OutputDir + '7.Metallicity' + OutputFormat
        plt.savefig(outputFile)  # Save the figure
        print('Saved file to', outputFile)
        plt.close()

        # Add this plot to our output list
        OutputList.append(outputFile)


# ---------------------------------------------------------

    def BlackHoleBulgeRelationship(self, G):

        print('Plotting the black hole-bulge relationship')

        seed(2222)

        plt.figure()  # New figure
        ax = plt.subplot(111)  # 1 plot on the figure

        w = np.where((G.BulgeMass > 0.01) & (G.BlackHoleMass > 0.00001))[0]
        if(len(w) > dilute): w = sample(w, dilute)

        bh = np.log10(G.BlackHoleMass[w] * 1.0e10 / self.Hubble_h)
        bulge = np.log10(G.BulgeMass[w] * 1.0e10 / self.Hubble_h)

        plt.scatter(bulge, bh, marker='o', s=1, c='k', alpha=0.5, label='Model galaxies')

        # overplot Haring & Rix 2004
        w = 10. ** np.arange(20)
        BHdata = 10. ** (8.2 + 1.12 * np.log10(w / 1.0e11))
        plt.plot(np.log10(w), np.log10(BHdata), 'b-', label="Haring \& Rix 2004")

        plt.ylabel(r'$\log\ M_{\mathrm{BH}}\ (M_{\odot})$')  # Set the y...
        plt.xlabel(r'$\log\ M_{\mathrm{bulge}}\ (M_{\odot})$')  # and the x-axis labels

        # Set the x and y axis minor ticks
        ax.xaxis.set_minor_locator(plt.MultipleLocator(0.05))
        ax.yaxis.set_minor_locator(plt.MultipleLocator(0.25))

        plt.axis([8.0, 12.0, 6.0, 10.0])

        leg = plt.legend(loc='upper left')
        leg.draw_frame(False)  # Don't want a box frame
        for t in leg.get_texts():  # Reduce the size of the text
            t.set_fontsize('medium')

        outputFile = OutputDir + '8.BlackHoleBulgeRelationship' + OutputFormat
        plt.savefig(outputFile)  # Save the figure
        print('Saved file to', outputFile)
        plt.close()

        # Add this plot to our output list
        OutputList.append(outputFile)


# ---------------------------------------------------------

    def QuiescentFraction(self, G):

        print('Plotting the quiescent fraction vs stellar mass')

        seed(2222)

        plt.figure()  # New figure
        ax = plt.subplot(111)  # 1 plot on the figure

        groupscale = 12.5

        w = np.where(G.StellarMass > 0.0)[0]
        StellarMass = np.log10(G.StellarMass[w] * 1.0e10 / self.Hubble_h)
        CentralMvir = np.log10(G.CentralMvir[w] * 1.0e10 / self.Hubble_h)
        Type = G.Type[w]
        sSFR = (G.SfrDisk[w] + G.SfrBulge[w]) / (G.StellarMass[w] * 1.0e10 / self.Hubble_h)

        MinRange = 9.5
        MaxRange = 12.0
        Interval = 0.1
        Nbins = int((MaxRange-MinRange)/Interval)
        Range = np.arange(MinRange, MaxRange, Interval)

        Mass = []
        Fraction = []
        CentralFraction = []
        SatelliteFraction = []
        SatelliteFractionLo = []
        SatelliteFractionHi = []

        for i in range(Nbins-1):

            w = np.where((StellarMass >= Range[i]) & (StellarMass < Range[i+1]))[0]
            if len(w) > 0:
                wQ = np.where((StellarMass >= Range[i]) & (StellarMass < Range[i+1]) & (sSFR < 10.0**sSFRcut))[0]
                Fraction.append(1.0*len(wQ) / len(w))
            else:
                Fraction.append(0.0)

            w = np.where((Type == 0) & (StellarMass >= Range[i]) & (StellarMass < Range[i+1]))[0]
            if len(w) > 0:
                wQ = np.where((Type == 0) & (StellarMass >= Range[i]) & (StellarMass < Range[i+1]) & (sSFR < 10.0**sSFRcut))[0]
                CentralFraction.append(1.0*len(wQ) / len(w))
            else:
                CentralFraction.append(0.0)

            w = np.where((Type == 1) & (StellarMass >= Range[i]) & (StellarMass < Range[i+1]))[0]
            if len(w) > 0:
                wQ = np.where((Type == 1) & (StellarMass >= Range[i]) & (StellarMass < Range[i+1]) & (sSFR < 10.0**sSFRcut))[0]
                SatelliteFraction.append(1.0*len(wQ) / len(w))
                wQ = np.where((Type == 1) & (StellarMass >= Range[i]) & (StellarMass < Range[i+1]) & (sSFR < 10.0**sSFRcut) & (CentralMvir < groupscale))[0]
                SatelliteFractionLo.append(1.0*len(wQ) / len(w))
                wQ = np.where((Type == 1) & (StellarMass >= Range[i]) & (StellarMass < Range[i+1]) & (sSFR < 10.0**sSFRcut) & (CentralMvir > groupscale))[0]
                SatelliteFractionHi.append(1.0*len(wQ) / len(w))
            else:
                SatelliteFraction.append(0.0)
                SatelliteFractionLo.append(0.0)
                SatelliteFractionHi.append(0.0)

            Mass.append((Range[i] + Range[i+1]) / 2.0)
            # print '  ', Mass[i], Fraction[i], CentralFraction[i], SatelliteFraction[i]

        Mass = np.array(Mass)
        Fraction = np.array(Fraction)
        CentralFraction = np.array(CentralFraction)
        SatelliteFraction = np.array(SatelliteFraction)
        SatelliteFractionLo = np.array(SatelliteFractionLo)
        SatelliteFractionHi = np.array(SatelliteFractionHi)

        w = np.where(Fraction > 0)[0]
        plt.plot(Mass[w], Fraction[w], label='All')

        w = np.where(CentralFraction > 0)[0]
        plt.plot(Mass[w], CentralFraction[w], color='Blue', label='Centrals')

        w = np.where(SatelliteFraction > 0)[0]
        plt.plot(Mass[w], SatelliteFraction[w], color='Red', label='Satellites')

        w = np.where(SatelliteFractionLo > 0)[0]
        plt.plot(Mass[w], SatelliteFractionLo[w], 'r--', label='Satellites-Lo')

        w = np.where(SatelliteFractionHi > 0)[0]
        plt.plot(Mass[w], SatelliteFractionHi[w], 'r-.', label='Satellites-Hi')

        plt.xlabel(r'$\log_{10} M_{\mathrm{stellar}}\ (M_{\odot})$')  # Set the x-axis label
        plt.ylabel(r'$\mathrm{Quescient\ Fraction}$')  # Set the y-axis label

        # Set the x and y axis minor ticks
        ax.xaxis.set_minor_locator(plt.MultipleLocator(0.05))
        ax.yaxis.set_minor_locator(plt.MultipleLocator(0.25))

        plt.axis([9.5, 12.0, 0.0, 1.05])

        leg = plt.legend(loc='lower right')
        leg.draw_frame(False)  # Don't want a box frame
        for t in leg.get_texts():  # Reduce the size of the text
            t.set_fontsize('medium')

        outputFile = OutputDir + '9.QuiescentFraction' + OutputFormat
        plt.savefig(outputFile)  # Save the figure
        print('Saved file to', outputFile)
        plt.close()

        # Add this plot to our output list
        OutputList.append(outputFile)


# --------------------------------------------------------

    def BulgeMassFraction(self, G):

        print('Plotting the mass fraction of galaxies')

        seed(2222)

        fBulge = G.BulgeMass / G.StellarMass
        fDisk = 1.0 - (G.BulgeMass) / G.StellarMass
        mass = np.log10(G.StellarMass * 1.0e10 / self.Hubble_h)
        #sSFR = np.log10((G.SfrDisk + G.SfrBulge) / (G.StellarMass * 1.0e10 / self.Hubble_h))

        binwidth = 0.2
        shift = binwidth/2.0
        mass_range = np.arange(8.5-shift, 12.0+shift, binwidth)
        bins = len(mass_range)

        fBulge_ave = np.zeros(bins)
        fBulge_var = np.zeros(bins)
        fDisk_ave = np.zeros(bins)
        fDisk_var = np.zeros(bins)

        for i in range(bins-1):
            w = np.where( (mass >= mass_range[i]) & (mass < mass_range[i+1]))[0]
            # w = np.where( (mass >= mass_range[i]) & (mass < mass_range[i+1]) & (sSFR < sSFRcut))[0]
            if(len(w) > 0):
                fBulge_ave[i] = np.mean(fBulge[w])
                fBulge_var[i] = np.var(fBulge[w])
                fDisk_ave[i] = np.mean(fDisk[w])
                fDisk_var[i] = np.var(fDisk[w])

        w = np.where(fBulge_ave > 0.0)[0]
        plt.plot(mass_range[w]+shift, fBulge_ave[w], 'r-', label='bulge')
        plt.fill_between(mass_range[w]+shift,
            fBulge_ave[w]+fBulge_var[w],
            fBulge_ave[w]-fBulge_var[w],
            facecolor='red', alpha=0.25)

        w = np.where(fDisk_ave > 0.0)[0]
        plt.plot(mass_range[w]+shift, fDisk_ave[w], 'k-', label='disk stars')
        plt.fill_between(mass_range[w]+shift,
            fDisk_ave[w]+fDisk_var[w],
            fDisk_ave[w]-fDisk_var[w],
            facecolor='black', alpha=0.25)

        plt.axis([mass_range[0], mass_range[bins-1], 0.0, 1.05])

        plt.ylabel(r'$\mathrm{Stellar\ Mass\ Fraction}$')  # Set the y...
        plt.xlabel(r'$\log_{10} M_{\mathrm{stars}}\ (M_{\odot})$')  # and the x-axis labels

        leg = plt.legend(loc='upper right', numpoints=1, labelspacing=0.1)
        leg.draw_frame(False)  # Don't want a box frame
        for t in leg.get_texts():  # Reduce the size of the text
                t.set_fontsize('medium')

        outputFile = OutputDir + '10.BulgeMassFraction' + OutputFormat
        plt.savefig(outputFile)  # Save the figure
        print('Saved file to', outputFile)
        plt.close()

        # Add this plot to our output list
        OutputList.append(outputFile)


# ---------------------------------------------------------

    def BaryonFraction(self, G):

        print('Plotting the average baryon fraction vs halo mass')

        seed(2222)

        plt.figure()  # New figure
        ax = plt.subplot(111)  # 1 plot on the figure

        HaloMass = np.log10(G.Mvir * 1.0e10 / self.Hubble_h)
        Baryons = G.StellarMass + G.ColdGas + G.HotGas + G.EjectedMass + G.IntraClusterStars + G.BlackHoleMass

        MinHalo = 11.0
        MaxHalo = 16.0
        Interval = 0.1
        Nbins = int((MaxHalo-MinHalo)/Interval)
        HaloRange = np.arange(MinHalo, MaxHalo, Interval)

        MeanCentralHaloMass = []
        MeanBaryonFraction = []
        MeanBaryonFractionU = []
        MeanBaryonFractionL = []

        MeanStars = []
        MeanCold = []
        MeanHot = []
        MeanEjected = []
        MeanICS = []
        MeanBH = []

        for i in range(Nbins-1):

            w1 = np.where((G.Type == 0) & (HaloMass >= HaloRange[i]) & (HaloMass < HaloRange[i+1]))[0]
            HalosFound = len(w1)

            if HalosFound > 2:

                BaryonFraction = []
                CentralHaloMass = []

                Stars = []
                Cold = []
                Hot = []
                Ejected = []
                ICS = []
                BH = []

                for j in range(HalosFound):

                    w2 = np.where(G.CentralGalaxyIndex == G.CentralGalaxyIndex[w1[j]])[0]
                    CentralAndSatellitesFound = len(w2)

                    if CentralAndSatellitesFound > 0:
                        BaryonFraction.append(sum(Baryons[w2]) / G.Mvir[w1[j]])
                        CentralHaloMass.append(np.log10(G.Mvir[w1[j]] * 1.0e10 / self.Hubble_h))

                        Stars.append(sum(G.StellarMass[w2]) / G.Mvir[w1[j]])
                        Cold.append(sum(G.ColdGas[w2]) / G.Mvir[w1[j]])
                        Hot.append(sum(G.HotGas[w2]) / G.Mvir[w1[j]])
                        Ejected.append(sum(G.EjectedMass[w2]) / G.Mvir[w1[j]])
                        ICS.append(sum(G.IntraClusterStars[w2]) / G.Mvir[w1[j]])
                        BH.append(sum(G.BlackHoleMass[w2]) / G.Mvir[w1[j]])

                MeanCentralHaloMass.append(np.mean(CentralHaloMass))
                MeanBaryonFraction.append(np.mean(BaryonFraction))
                MeanBaryonFractionU.append(np.mean(BaryonFraction) + np.var(BaryonFraction))
                MeanBaryonFractionL.append(np.mean(BaryonFraction) - np.var(BaryonFraction))

                MeanStars.append(np.mean(Stars))
                MeanCold.append(np.mean(Cold))
                MeanHot.append(np.mean(Hot))
                MeanEjected.append(np.mean(Ejected))
                MeanICS.append(np.mean(ICS))
                MeanBH.append(np.mean(BH))

                print('  ', i, HaloRange[i], HalosFound, np.mean(BaryonFraction))

        plt.plot(MeanCentralHaloMass, MeanBaryonFraction, 'k-', label='TOTAL')#, color='purple', alpha=0.3)
        plt.fill_between(MeanCentralHaloMass, MeanBaryonFractionU, MeanBaryonFractionL,
            facecolor='purple', alpha=0.25, label='TOTAL')

        plt.plot(MeanCentralHaloMass, MeanStars, 'k--', label='Stars')
        plt.plot(MeanCentralHaloMass, MeanCold, label='Cold', color='blue')
        plt.plot(MeanCentralHaloMass, MeanHot, label='Hot', color='red')
        plt.plot(MeanCentralHaloMass, MeanEjected, label='Ejected', color='green')
        plt.plot(MeanCentralHaloMass, MeanICS, label='ICS', color='yellow')
        # plt.plot(MeanCentralHaloMass, MeanBH, 'k:', label='BH')

        plt.xlabel(r'$\mathrm{Central}\ \log_{10} M_{\mathrm{vir}}\ (M_{\odot})$')  # Set the x-axis label
        plt.ylabel(r'$\mathrm{Baryon\ Fraction}$')  # Set the y-axis label

        # Set the x and y axis minor ticks
        ax.xaxis.set_minor_locator(plt.MultipleLocator(0.05))
        ax.yaxis.set_minor_locator(plt.MultipleLocator(0.25))

        plt.axis([10.8, 15.0, 0.0, 0.23])

        leg = plt.legend(bbox_to_anchor=[0.99, 0.6])
        leg.draw_frame(False)  # Don't want a box frame
        for t in leg.get_texts():  # Reduce the size of the text
            t.set_fontsize('medium')

        outputFile = OutputDir + '11.BaryonFraction' + OutputFormat
        plt.savefig(outputFile)  # Save the figure
        print('Saved file to', outputFile)
        plt.close()

        # Add this plot to our output list
        OutputList.append(outputFile)


# --------------------------------------------------------

    def SpinDistribution(self, G, w_lsbg, w_void_lsbg):

        print('Plotting the spin distribution of all galaxies')

        # set up figure
        plt.figure()
        ax = plt.subplot(111)

        SpinParameter = np.sqrt(G.Spin[:,0]*G.Spin[:,0] + G.Spin[:,1]*G.Spin[:,1] + G.Spin[:,2]*G.Spin[:,2]) / (np.sqrt(2) * G.Vvir * G.Rvir)
        SpinParameter_lsbg = np.sqrt(G.Spin[w_lsbg,0]*G.Spin[w_lsbg,0] + G.Spin[w_lsbg,1]*G.Spin[w_lsbg,1] + G.Spin[w_lsbg,2]*G.Spin[w_lsbg,2]) / (np.sqrt(2) * G.Vvir[w_lsbg] * G.Rvir[w_lsbg])
        SpinParameter_void_lsbg = np.sqrt(G.Spin[w_void_lsbg,0]*G.Spin[w_void_lsbg,0] + G.Spin[w_void_lsbg,1]*G.Spin[w_void_lsbg,1] + G.Spin[w_void_lsbg,2]*G.Spin[w_void_lsbg,2]) / (np.sqrt(2) * G.Vvir[w_void_lsbg] * G.Rvir[w_void_lsbg])

        mi = -0.02
        ma = 0.5
        binwidth = 0.01
        NB = np.int((ma - mi) / binwidth)

        (counts, binedges) = np.histogram(SpinParameter, range=(mi, ma), bins=NB)
        (counts_lsbg, binedges) = np.histogram(SpinParameter_lsbg, range=(mi, ma), bins=NB)
        (counts_void_lsbg, binedges) = np.histogram(SpinParameter_void_lsbg, range=(mi, ma), bins=NB)
        xaxeshisto = binedges[:-1] + 0.5 * binwidth
        plt.plot(xaxeshisto, counts, 'k-', label='Model - All')
        plt.plot(xaxeshisto, counts_lsbg, 'r', label='LSBG')
        plt.plot(xaxeshisto, counts_void_lsbg, 'b', label='LSBG - Voids')

        plt.axis([mi, ma, 0.0, max(counts)*1.15])

        plt.ylabel(r'$\mathrm{Number}$')  # Set the y...
        plt.xlabel(r'$\mathrm{Spin\ Parameter}$')  # and the x-axis labels

        leg = plt.legend(loc='upper right', numpoints=1, labelspacing=0.1)
        leg.draw_frame(False)  # Don't want a box frame
        for t in leg.get_texts():  # Reduce the size of the text
                t.set_fontsize('medium')

        outputFile = OutputDir + '12.SpinDistribution' + OutputFormat
        plt.savefig(outputFile)  # Save the figure
        print('Saved file to', outputFile)
        plt.close()

        # Add this plot to our output list
        OutputList.append(outputFile)


# --------------------------------------------------------

    def VelocityDistribution(self, G):

        print('Plotting the velocity distribution of all galaxies')

        seed(2222)

        mi = -40.0
        ma = 40.0
        binwidth = 0.5
        NB = (ma - mi) / binwidth

        # set up figure
        plt.figure()
        ax = plt.subplot(111)

        pos_x = G.Pos[:,0] / self.Hubble_h
        pos_y = G.Pos[:,1] / self.Hubble_h
        pos_z = G.Pos[:,2] / self.Hubble_h

        vel_x = G.Vel[:,0]
        vel_y = G.Vel[:,1]
        vel_z = G.Vel[:,2]

        dist_los = np.sqrt(pos_x*pos_x + pos_y*pos_y + pos_z*pos_z)
        vel_los = (pos_x/dist_los)*vel_x + (pos_y/dist_los)*vel_y + (pos_z/dist_los)*vel_z
        #dist_red = dist_los + vel_los/(self.Hubble_h*100.0)

        tot_gals = len(pos_x)


        (counts, binedges) = np.histogram(vel_los/(self.Hubble_h*100.0), range=(mi, ma), bins=NB)
        xaxeshisto = binedges[:-1] + 0.5 * binwidth
        plt.plot(xaxeshisto, counts / binwidth / tot_gals, 'k-', label='los-velocity')

        (counts, binedges) = np.histogram(vel_x/(self.Hubble_h*100.0), range=(mi, ma), bins=NB)
        xaxeshisto = binedges[:-1] + 0.5 * binwidth
        plt.plot(xaxeshisto, counts / binwidth / tot_gals, 'r-', label='x-velocity')

        (counts, binedges) = np.histogram(vel_y/(self.Hubble_h*100.0), range=(mi, ma), bins=NB)
        xaxeshisto = binedges[:-1] + 0.5 * binwidth
        plt.plot(xaxeshisto, counts / binwidth / tot_gals, 'g-', label='y-velocity')

        (counts, binedges) = np.histogram(vel_z/(self.Hubble_h*100.0), range=(mi, ma), bins=NB)
        xaxeshisto = binedges[:-1] + 0.5 * binwidth
        plt.plot(xaxeshisto, counts / binwidth / tot_gals, 'b-', label='z-velocity')


        plt.yscale('log', nonposy='clip')
        plt.axis([mi, ma, 1e-5, 0.5])
        # plt.axis([mi, ma, 0, 0.13])

        plt.ylabel(r'$\mathrm{Box\ Normalised\ Count}$')  # Set the y...
        plt.xlabel(r'$\mathrm{Velocity / H}_{0}$')  # and the x-axis labels

        leg = plt.legend(loc='upper left', numpoints=1, labelspacing=0.1)
        leg.draw_frame(False)  # Don't want a box frame
        for t in leg.get_texts():  # Reduce the size of the text
                t.set_fontsize('medium')

        outputFile = OutputDir + '13.VelocityDistribution' + OutputFormat
        plt.savefig(outputFile)  # Save the figure
        print('Saved file to', outputFile)
        plt.close()

        # Add this plot to our output list
        OutputList.append(outputFile)


# --------------------------------------------------------

    def MassReservoirScatter(self, G):

        print('Plotting the mass in stellar, cold, hot, ejected, ICS reservoirs')

        seed(2222)

        plt.figure()  # New figure
        ax = plt.subplot(111)  # 1 plot on the figure

        w = np.where((G.Type == 0) & (G.Mvir > 1.0) & (G.StellarMass > 0.0))[0]
        if(len(w) > dilute): w = sample(w, dilute)

        mvir = np.log10(G.Mvir[w] * 1.0e10)
        plt.scatter(mvir, np.log10(G.StellarMass[w] * 1.0e10), marker='o', s=0.3, c='k', alpha=0.5, label='Stars')
        plt.scatter(mvir, np.log10(G.ColdGas[w] * 1.0e10), marker='o', s=0.3, color='blue', alpha=0.5, label='Cold gas')
        plt.scatter(mvir, np.log10(G.HotGas[w] * 1.0e10), marker='o', s=0.3, color='red', alpha=0.5, label='Hot gas')
        plt.scatter(mvir, np.log10(G.EjectedMass[w] * 1.0e10), marker='o', s=0.3, color='green', alpha=0.5, label='Ejected gas')
        plt.scatter(mvir, np.log10(G.IntraClusterStars[w] * 1.0e10), marker='o', s=10, color='yellow', alpha=0.5, label='Intracluster stars')

        plt.ylabel(r'$\mathrm{stellar,\ cold,\ hot,\ ejected,\ ICS\ mass}$')  # Set the y...
        plt.xlabel(r'$\log\ M_{\mathrm{vir}}\ (h^{-1}\ M_{\odot})$')  # and the x-axis labels

        plt.axis([10.0, 14.0, 7.5, 12.5])

        leg = plt.legend(loc='upper left')
        leg.draw_frame(False)  # Don't want a box frame
        for t in leg.get_texts():  # Reduce the size of the text
            t.set_fontsize('medium')

        plt.text(13.5, 8.0, r'$\mathrm{All}')

        outputFile = OutputDir + '14.MassReservoirScatter' + OutputFormat
        plt.savefig(outputFile)  # Save the figure
        print('Saved file to', outputFile)
        plt.close()

        # Add this plot to our output list
        OutputList.append(outputFile)


# --------------------------------------------------------

    def SpatialDistribution(self, G):

        print('Plotting the spatial distribution of all galaxies')

        seed(2222)

        plt.figure()  # New figure

        w = np.where((G.Mvir > 0.0) & (G.StellarMass > 0.1))[0]
        if(len(w) > dilute): w = sample(w, dilute)

        xx = G.Pos[w,0]
        yy = G.Pos[w,1]
        zz = G.Pos[w,2]

        buff = self.BoxSize*0.1

        ax = plt.subplot(221)  # 1 plot on the figure
        plt.scatter(xx, yy, marker='o', s=0.3, c='k', alpha=0.5)
        plt.axis([0.0-buff, self.BoxSize+buff, 0.0-buff, self.BoxSize+buff])

        plt.ylabel(r'$\mathrm{x}$')  # Set the y...
        plt.xlabel(r'$\mathrm{y}$')  # and the x-axis labels

        ax = plt.subplot(222)  # 1 plot on the figure
        plt.scatter(xx, zz, marker='o', s=0.3, c='k', alpha=0.5)
        plt.axis([0.0-buff, self.BoxSize+buff, 0.0-buff, self.BoxSize+buff])

        plt.ylabel(r'$\mathrm{x}$')  # Set the y...
        plt.xlabel(r'$\mathrm{z}$')  # and the x-axis labels

        ax = plt.subplot(223)  # 1 plot on the figure
        plt.scatter(yy, zz, marker='o', s=0.3, c='k', alpha=0.5)
        plt.axis([0.0-buff, self.BoxSize+buff, 0.0-buff, self.BoxSize+buff])
        plt.ylabel(r'$\mathrm{y}$')  # Set the y...
        plt.xlabel(r'$\mathrm{z}$')  # and the x-axis labels

        outputFile = OutputDir + '15.SpatialDistribution' + OutputFormat
        plt.savefig(outputFile)  # Save the figure
        print('Saved file to', outputFile)
        plt.close()

        # Add this plot to our output list
        OutputList.append(outputFile)


    def plot_void_gals(self, G, w_void, w_void_lsbg):

        fig = plt.figure()
        fig.add_subplot # fig.add_axes() is deprecated since matplotlib 3.3
        ax = fig.gca(projection='3d')
        #ax.set_aspect("equal")
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("z")

        w = np.where(G.StellarMass > 0.0)[0]


        read_gadget.get_particles.argtypes = [ctypes.c_char_p]
        read_gadget.read_gadget.argtypes = [ctypes.c_char_p]
        if(print_xyz_particle_ids):
            read_gadget.read_ids.argtypes = [ctypes.c_char_p]
        num_tracers=read_gadget.get_particles(tracer_file.encode('utf-8'))

        tracers = np.empty((num_tracers, 3))
        returnValue=np.empty((num_tracers, 4))
        if(print_xyz_particle_ids):
            particle_ids=np.empty((num_tracers,1))
        #potential_centre=True
        #if(potential_centre):
        #    pot=np.empty(num_tracers)

        read_gadget.read_gadget.restype = ndpointer(dtype=ctypes.c_float, shape=(num_tracers,4))

        returnValue=read_gadget.read_gadget(tracer_file.encode('utf-8'))
        potential_centre = False
        tracers[:,0]=returnValue[:,0]
        tracers[:,1]=returnValue[:,1]
        tracers[:,2]=returnValue[:,2]
        #if(potential_centre):
        #    pot=returnValue[:,3]

        if(print_xyz_particle_ids):
            read_gadget.read_ids.restype = ndpointer(dtype=ctypes.c_long, shape=(num_tracers,1))
            particle_ids = read_gadget.read_ids(tracer_file.encode('utf-8'))

        X, Y, Z = [], [], []
        GalX,GalY,GalZ,GalRadius = [],[],[],[]
        LSBGX,LSBGY,LSBGZ,LSBGRadius = [],[],[],[]

        # generate random indices to dilute the plot
        if(len(tracers[:,0]) < 100000):
            i_ran=range(0,len(tracers[:,0]),1)
        if(len(tracers[:,0]) < 400000 and len(tracers[:,0]) > 100000):
            i_ran=random.sample(range(len(tracers[:,0])),250000)
        if(len(tracers[:,0]) > 400000):
            i_ran=random.sample(range(len(tracers[:,0])),400000)

        for i in i_ran:
            X.append(tracers[i,0])
            Y.append(tracers[i,1])
            Z.append(tracers[i,2])

        print("Read DM particles")

        for gal in w:
            u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
            SphereX=G.Pos[gal,0]+(G.Rvir[gal])*np.cos(u)*np.sin(v)
            SphereY=G.Pos[gal,1]+(G.Rvir[gal])*np.sin(u)*np.sin(v)
            SphereZ=G.Pos[gal,2]+(G.Rvir[gal])*np.cos(v)
            ax.plot_wireframe(SphereX, SphereY, SphereZ)

        ax.scatter(X, Y, Z, s=1, c='red', alpha =0.05, label="DM particle")
        plt.savefig("gals.png")
        ax.view_init(30,30)
        plt.savefig("gals_90.png")

        plt.close()

        fig = plt.figure()
        fig.add_subplot()
        ax = fig.gca(projection='3d')
        #ax.set_aspect("equal")
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("z")

        for voidgal in w_void:
            GalX.append(G.Pos[voidgal,0])
            GalY.append(G.Pos[voidgal,1])
            GalZ.append(G.Pos[voidgal,2])
            GalRadius.append(G.Rvir[voidgal])
            a, b = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
            VoidSphereX=G.Pos[voidgal,0]+(G.Rvir[voidgal])*np.cos(a)*np.sin(b)
            VoidSphereY=G.Pos[voidgal,1]+(G.Rvir[voidgal])*np.sin(a)*np.sin(b)
            VoidSphereZ=G.Pos[voidgal,2]+(G.Rvir[voidgal])*np.cos(b)
            ax.plot_wireframe(VoidSphereX, VoidSphereY, VoidSphereZ)

        ax.scatter(X, Y, Z, s=1, c='red', alpha =0.05, label="DM particle")
        plt.savefig("void_gal.png")
        ax.view_init(30,30)
        plt.savefig("void_gal_90.png")

        plt.close()

        fig = plt.figure()
        fig.add_subplot()
        ax = fig.gca(projection='3d')
        #ax.set_aspect("equal")
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("z")

        for lsbg in w_void_lsbg:
            LSBGX.append(G.Pos[lsbg,0])
            LSBGY.append(G.Pos[lsbg,1])
            LSBGZ.append(G.Pos[lsbg,2])
            LSBGRadius.append(G.Rvir[lsbg])
            LSBGSphereX=G.Pos[lsbg,0]+(G.Rvir[lsbg])*np.cos(a)*np.sin(b)
            LSBGSphereY=G.Pos[lsbg,1]+(G.Rvir[lsbg])*np.sin(a)*np.sin(b)
            LSBGSphereZ=G.Pos[lsbg,2]+(G.Rvir[lsbg])*np.cos(b)
            ax.plot_wireframe(LSBGSphereX, LSBGSphereY, LSBGSphereZ)

        ax.scatter(X, Y, Z, s=1, c='red', alpha =0.05, label="DM particle")
        plt.savefig("void_lsbg.png")
        ax.view_init(30,30)
        plt.savefig("void_lsbg_90.png")

        plt.close()

        if(print_xyz_particle_ids):
            xyz_particle_id_file = "xyz_particle_id.txt"
            with open(xyz_particle_id_file,'w+') as xyz_id_file:
                for l in range(0,len(tracers[:,0])):
                    xyz_id_file.write("%f %f %f %d\n" %
                                      (tracers[l,0], tracers[l,1], tracers[l,2],
                                       particle_ids[l]))
            xyz_id_file.close()


# --------------------------------------------------------

    def calc_rad(self, G, w, radius_factor = 1.678):
        """
        Calculate the mean and std for the size of galaxies.

        Parameter
        ------------
        G: galaxy data
        w: list
           The list of indices of the galaxies should be analysed.
        radius_factor: float (optional keyword)
           Factor to multiple by the disc scale length:
           - use 1.0 for the disc scale length;
           - use 1.678 for the disc half-mass radius.

        returns
        ------------
        mean_rad: float
           The mean radius over all galaxies in w.
        std_rad: float
           The standard deviation of the mean for these galaxies.
        median_rad: float
           The median radius of all galaxies in w.
        sig_median_rad:float
           standard deviation of the median.
        stderr_median_rad: float
           The standard error for the median radius.
        max_rad: flaot
           The maximal radius of all galaxies in w.
        N: int
        The number of galaxies in w.
        """
        Gaussian_interpretation = 1.4826
        N=len(set(w))
        if(N>0):
            radii = radius_factor * G.DiskRadius[np.array(w)] * 1000.0
            mean_rad = np.mean(radii)
            # standard deviation
            std_rad = np.std(radii)
            median_rad = np.median(radii)
            sig_median_rad = Gaussian_interpretation * np.median(np.abs(radii-median_rad))
            # standard error of the median
            stderr_median_rad = np.sqrt( np.sum((radii-median_rad)**2)/(float(max(1,N-1))) ) / np.sqrt(float(max(1,N)))
            max_rad = np.amax(radii)



            return mean_rad, std_rad, median_rad, sig_median_rad, stderr_median_rad, max_rad, N
        else:
            return 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0


    def calc_spin(self, G, w):
        """
        Calculate the mean and std for the spin parameter of a galaxy.

        Parameter
        ------------
        G: galaxy data
        w: list
        The list of indices of the galaxies should be analysed.

        returns
        ------------
        mean_spin: float
        The mean spin parameter over all galaxies in w.
        std_rad_spin: float
        The standard deviation of the mean for these galaxies.
        median_spin: float
        The median spin parameter of all galaxies in w.
        sig_median_spin:float
        standard deviation of the median.
        stderr_median_spin: float
        The standard error for the median spin parameter.
        max_spin: flaot
        The maximal spin parameter of all galaxies in w.
        N: int
        The number of galaxies in w.
        """

        Gaussian_interpretation = 1.4826
        N=len(set(w))
        if(N>0):
            SpinParameter = (np.sqrt(G.Spin[w,0]*G.Spin[w,0] + G.Spin[w,1]*G.Spin[w,1] + G.Spin[w,2]*G.Spin[w,2])
                                    / (np.sqrt(2) * G.Vvir[w] * G.Rvir[w]))
            mean_spin = np.mean(SpinParameter)
            # standard deviation
            std_spin = np.std(SpinParameter)
            median_spin = np.median(SpinParameter)
            sig_median_spin = Gaussian_interpretation * np.median(np.abs(SpinParameter-median_spin))
            # standard error of the median
            stderr_median_spin = np.sqrt( np.sum((SpinParameter-median_spin)**2)/(float(max(1,N-1))) ) / np.sqrt(float(max(1,N)))
            max_spin = np.amax(SpinParameter)
            return mean_spin, std_spin, median_spin, sig_median_spin, stderr_median_spin, max_spin, N
        else:
            return 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0


    def calc_rvir(self, G, w):
        """
        Calculate the mean and std for the virial radius of the host halo of a galaxy.

        Parameter
        ------------
        G: galaxy data
        w: list
        The list of indices of the galaxies should be analysed.

        returns
        ------------
        mean_rvir: float
        The mean virial radius over all galaxies in w.
        std_rad_rvir: float
        The standard deviation of the mean for these galaxies.
        median_rvir: float
        The median virial radius of all galaxies in w.
        sig_median_rvir:float
        standard deviation of the median.
        stderr_median_rvir: float
        The standard error for the median virial radius.
        max_rvir: flaot
        The maximal virial radius of all galaxies in w.
        N: int
        The number of galaxies in w.
        """

        Gaussian_interpretation = 1.4826
        N=len(set(w))
        if(N>0):
            Rvir = G.Rvir[w] * 1000.0 # in kpc/h
            mean_rvir = np.mean(Rvir)
            # standard deviation
            std_rvir = np.std(Rvir)
            median_rvir = np.median(Rvir)
            sig_median_rvir = Gaussian_interpretation * np.median(np.abs(Rvir-median_rvir))
            # standard error of the median
            stderr_median_rvir = np.sqrt( np.sum((Rvir-median_rvir)**2)/(float(max(1,N-1))) ) / np.sqrt(float(max(1,N)))
            max_rvir = np.amax(Rvir)
            return mean_rvir, std_rvir, median_rvir, sig_median_rvir, stderr_median_rvir, max_rvir, N
        else:
            return 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0


    def create_subsample_of_indices(self, G, w):
        """
        We are only interested to compare galaxies in a given mass interval.
        The comparision of galaxies is only meaningful when we compare galaxies
        of comparable mass. This function takes a subsample of galaxies and returns
        all galaxies that lay in a previously defined mass interval.

        Parameter
        ------------
        G: galaxy data
        w: list
        The indices of galaxies in G
        (the global variable mass_gap is used as well)

        returns
        ------------
        w_sub: list
        The indices of galaxies that lay in a chosen mass interval
        """

        print("create_subsample_of_indices")
        print("w=",w)
        if len(w) > 0:
            w=np.array(w)
            w_indices = (np.where((G.Mvir[w]*1.0e10 > np.float(self.mass_gap[0])) & (G.Mvir[w]*1.0e10 < np.float(self.mass_gap[1]))))[0]
            if(len(w_indices) > 0):
                w_sub = w[w_indices]
            else:
                w_sub= []
        else:
            w_sub = []
        print("w_sub",w_sub)
        return w_sub

    def plot_halo_mass_dist(self, halo_masses_voids, halo_masses_nonvoids, halo_masses_all, N_part, massDM, box_length):

        halo_masses_voids = halo_masses_voids
        halo_masses_nonvoids = halo_masses_nonvoids
        max_void_halo_mass = max(halo_masses_voids)
        min_void_halo_mass = min(halo_masses_voids)
        max_nonvoid_halo_mass = max(halo_masses_nonvoids)
        min_nonvoid_halo_mass = min(halo_masses_nonvoids)


        counts, counts_void, counts_nonvoid, mass = [], [], [], []
        
        num_halos = len(halo_masses_all)
        num_halos_voids = len(halo_masses_voids)
        num_halos_nonvoids = len(halo_masses_nonvoids)

        density=N_part*N_part*N_part*massDM/pow(box_length,3)
        for i in np.arange(10,15,0.2):
            count = 0
            for l in range(0,num_halos):
                if halo_masses_all[l] > pow(10,(i-0.1)) and halo_masses_all[l] < pow(10,(i+0.1)):
                    count += 1
            count_void = 0
            for l in range(0,num_halos_voids):
                if halo_masses_voids[l] > pow(10,(i-0.1)) and halo_masses_voids[l] < pow(10,(i+0.1)):
                    count_void += 1
            count_nonvoid = 0
            for l in range(0,num_halos_nonvoids):
                if halo_masses_nonvoids[l] > pow(10,(i-0.1)) and halo_masses_nonvoids[l] < pow(10,(i+0.1)):
                    count_nonvoid += 1

            counts.append(pow(10,i)*pow(10,i)/density*count/np.abs((pow(10,(i+0.1))-pow(10,(i-0.1))))/pow(box_length,3))
            counts_void.append(pow(10,i)*pow(10,i)/density*count_void/np.abs((pow(10,(i+0.1))-pow(10,(i-0.1))))/pow(box_length,3))
            counts_nonvoid.append(pow(10,i)*pow(10,i)/density*count_nonvoid/np.abs((pow(10,(i+0.1))-pow(10,(i-0.1))))/pow(box_length,3))
            mass.append(pow(10,i))

        counts_clean = np.delete(np.array(counts), np.where(np.array(counts) < 1.e-7))
        counts_void_clean = np.delete(np.array(counts_void), np.where(np.array(counts_void) < 1.e-7))
        counts_nonvoid_clean = np.delete(np.array(counts_nonvoid), np.where(np.array(counts_nonvoid) < 1.e-7))

        mass_clean = np.delete(np.array(mass), np.where(np.array(counts) < 1.e-7))
        mass_void_clean = np.delete(np.array(mass), np.where(np.array(counts_void) < 1.e-7))
        mass_nonvoid_clean = np.delete(np.array(mass), np.where(np.array(counts_nonvoid) < 1.e-7))

        plt.plot(mass_clean, counts_clean,'k-', label='all haloes')
        plt.plot(mass_nonvoid_clean, counts_nonvoid_clean,'r--', label='non-void haloes')
        plt.plot(mass_void_clean, counts_void_clean,'b-.', label='void haloes')
        plt.legend(loc='best')
        plt.xscale("log")
        plt.yscale("log")
        plt.ylim(1.e-6,0.5)
        plt.ylabel(r'$M^2$/$\rho$ d$n/$d$M$')
        plt.xlabel(r'$M$ (M$_{\odot}$/$h$)')
        outputFile = 'HaloMassFunction.eps'
        plt.tight_layout()
        plt.savefig(outputFile, metadata=self.metadata)
        print('Saved file to', outputFile)
        plt.close()


    def print_statistics(self, G, w_void, w_lsbg, w_void_lsbg, w_wall, w_filament,
                         w_inner_void, w_outer_void, w_void_full,
                         radius_factor = 1.678):
        """
        This routine prints the results about the galaxy sizes into the terminal
        and writes them to a file.

        Parameter
        ------------
        G: galaxy data
        w_void: list
           Indices of galaxies in a void.
        w_lsbg: list
           Indices of galaxies are LSBGs.
        w_void_lsbg: list
           Indices of LSBGs in voids.
        ...
        radius_factor: float (optional keyword)
           Factor to multiple by the disc scale length:
           - use 1.0 for the disc scale length;
           - use 1.678 for the disc half-mass radius.


        returns
        ------------
        galaxy_size.dat
        A file that contains the information about the median size and the error of size
        of different galaxy populations.
        """

        res = Results()
        w_udg =[]
        w_void_udg =[]
        w_non_void =[]
        w_non_void_full = []
        w_total = np.where(G.StellarMass > 0.0)[0]
        w = np.where((G.StellarMass > 0.0) & (G.Mvir*1.0e10 > np.float(self.mass_gap[0])) & (G.Mvir*1.0e10 < np.float(self.mass_gap[1])))[0]
        for l in w_lsbg:
            if(1.678*G.DiskRadius[l]*1000 > 1.5):
                w_udg.append(l)
        for l in w_void_lsbg:
            if(1.678*G.DiskRadius[l]*1000 > 1.5):
                w_void_udg.append(l)
        for l in w:
            if l not in w_void:
                w_non_void.append(l)
        for l in w_total:
            if l not in w_void_full:
                w_non_void_full.append(l)

        mean_non_void_rad, std_err_non_void, med_non_void_rad, sig_med_non_void, stderr_med_non_void, rad_max_non_void, N_non_void = res.calc_rad(G, w_non_void, radius_factor = radius_factor)
        mean_void_rad, std_err_void, med_void_rad, sig_med_void, stderr_med_void, rad_max_void, N_void = res.calc_rad(G, w_void, radius_factor = radius_factor)
        mean_non_void_full_rad, std_err_non_void_full, med_non_void_full_rad, sig_med_non_void_full, stderr_med_non_void_full, rad_max_non_void_full, N_non_void_full = res.calc_rad(G, w_non_void_full, radius_factor = radius_factor)
        mean_void_full_rad, std_err_void_full, med_void_full_rad, sig_med_void_full, stderr_med_void_full, rad_max_void_full, N_void_full = res.calc_rad(G, w_void_full, radius_factor = radius_factor)
        if(self.split_position_in_void):
            mean_inner_void_rad, std_err_inner_void, med_inner_void_rad, sig_med_inner_void, stderr_med_inner_void, rad_max_inner_void, N_inner_void = res.calc_rad(G, w_inner_void, radius_factor = radius_factor)
            mean_outer_void_rad, std_err_outer_void, med_outer_void_rad, sig_med_outer_void, stderr_med_outer_void, rad_max_outer_void, N_outer_void = res.calc_rad(G, w_outer_void, radius_factor = radius_factor)
        if(split_walls_filaments):
            mean_wall_rad, std_err_wall, med_wall_rad, sig_med_wall, stderr_med_wall, rad_max_wall, N_wall = res.calc_rad(G, w_wall, radius_factor = radius_factor)
            mean_filament_rad, std_err_filament, med_filament_rad, sig_med_filament, stderr_med_filament, rad_max_filament, N_filament = res.calc_rad(G, w_filament, radius_factor = radius_factor)

        # Analyse spins
        mean_non_void_spin, std_err_non_void_spin, med_non_void_spin, sig_med_non_void_spin, stderr_med_non_void_spin, spin_max_non_void, N_non_void_spin = res.calc_spin(G, w_non_void)
        mean_void_spin, std_err_void_spin, med_void_spin, sig_med_void_spin, stderr_med_void_spin, spin_max_void, N_void_spin = res.calc_spin(G, w_void)
        mean_non_void_full_spin, std_err_non_void_full_spin, med_non_void_full_spin, sig_med_non_void_full_spin, stderr_med_non_void_full_spin, spin_max_non_void_full, N_non_void_full_spin = res.calc_spin(G, w_non_void_full)
        mean_void_full_spin, std_err_void_full_spin, med_void_full_spin, sig_med_void_full_spin, stderr_med_void_full_spin, spin_max_void_full, N_void_full_spin = res.calc_spin(G, w_void_full)
        if(self.split_position_in_void):
            mean_inner_void_spin, std_err_inner_void_spin, med_inner_void_spin, sig_med_inner_void_spin, stderr_med_inner_void_spin, spin_max_inner_void, N_inner_void_spin = res.calc_spin(G, w_inner_void)
            mean_outer_void_spin, std_err_outer_void_spin, med_outer_void_spin, sig_med_outer_void_spin, stderr_med_outer_void_spin, spin_max_outer_void, N_outer_void_spin = res.calc_spin(G, w_outer_void)
        if(split_walls_filaments):
            mean_wall_spin, std_err_wall_spin, med_wall_spin, sig_med_wall_spin, stderr_med_wall_spin, spin_max_wall, N_wall_spin = res.calc_spin(G, w_wall)
            mean_filament_spin, std_err_filament_spin, med_filament_spin, sig_med_filament_spin, stderr_med_filament_spin, spin_max_filament, N_filament_spin = res.calc_spin(G, w_filament)

        # Analyse virial radius
        mean_non_void_rvir, std_err_non_void_rvir, med_non_void_rvir, sig_med_non_void_rvir, stderr_med_non_void_rvir, rvir_max_non_void, N_non_void_rvir = res.calc_rvir(G, w_non_void)
        mean_void_rvir, std_err_void_rvir, med_void_rvir, sig_med_void_rvir, stderr_med_void_rvir, rvir_max_void, N_void_rvir = res.calc_rvir(G, w_void)
        mean_non_void_full_rvir, std_err_non_void_full_rvir, med_non_void_full_rvir, sig_med_non_void_full_rvir, stderr_med_non_void_full_rvir, rvir_max_non_void_full, N_non_void_full_rvir = res.calc_rvir(G, w_non_void_full)
        mean_void_full_rvir, std_err_void_full_rvir, med_void_full_rvir, sig_med_void_full_rvir, stderr_med_void_full_rvir, rvir_max_void_full, N_void_full_rvir = res.calc_rvir(G, w_void_full)
        if(self.split_position_in_void):
            mean_inner_void_rvir, std_err_inner_void_rvir, med_inner_void_rvir, sig_med_inner_void_rvir, stderr_med_inner_void_rvir, rvir_max_inner_void, N_inner_void_rvir = res.calc_rvir(G, w_inner_void)
            mean_outer_void_rvir, std_err_outer_void_rvir, med_outer_void_rvir, sig_med_outer_void_rvir, stderr_med_outer_void_rvir, rvir_max_outer_void, N_outer_void_rvir = res.calc_rvir(G, w_outer_void)
        if(split_walls_filaments):
            mean_wall_rvir, std_err_wall_rvir, med_wall_rvir, sig_med_wall_rvir, stderr_med_wall_rvir, rvir_max_wall, N_wall_rvir = res.calc_rvir(G, w_wall)
            mean_filament_rvir, std_err_filament_rvir, med_filament_rvir, sig_med_filament_rvir, stderr_med_filament_rvir, rvir_max_filament, N_filament_rvir = res.calc_rvir(G, w_filament)


        print("##############################################################")
        print("#              Statistics of LSBGs                           #")
        print("#                                                            #")
        print("# how many galaxies: %d                                      #" % len(set(w)))
        print("# how many galaxies are in voids: %d                         #" % len(set(w_void)))
        print("# how many galaxies are LSBGs: %d                            #" % len(set(w_lsbg)))
        print("# how many LSBGs are in voids: %d                            #" % len(set(w_void_lsbg)))
        print("# how many LSBGs have r > 1.5kpc/h: %d                         #" % len(set(w_udg)))
        print("# how many void-LSBGs have r > 1.5kpc/h: %d                    #" % len(set(w_void_udg)))
        print("# average radius of a non_void galaxy: %f +- %f (in kpc/h)     #" % (med_non_void_rad, stderr_med_non_void))
        print("# average radius of a void galaxy: %f +- %f (in kpc/h)         #" % (med_void_rad, stderr_med_void))
        print("#                                                            #")
        print("##############################################################")

        print("w=",w)
        with open("galaxy_size.dat","w+") as F:
            F.write("N_gals_total=%i\n" % len(set(w_total)))
            F.write("N_gals_in_interval=%i\n" % len(set(w)))
            F.write("N_void-gals_total=%i\n" % N_void_full)
            F.write("N_non-void-gals_total=%i\n" % N_non_void_full)
            F.write("N_void-gals=%i\n" % N_void)
            F.write("N_non-void-gals=%i\n" % N_non_void)
            F.write("r_avg-nonvoids=%f\n" % med_non_void_rad)
            F.write("error-r_avg-nonvoids=%f\n" % stderr_med_non_void)
            F.write("r_avg-voids=%f\n" % med_void_rad)
            F.write("error-r_avg-voids=%f\n" % stderr_med_void)
            F.write("r_avg-nonvoids_total=%f\n" % med_non_void_full_rad)
            F.write("error-r_avg-nonvoids_total=%f\n" % stderr_med_non_void_full)
            F.write("r_avg-voids_total=%f\n" % med_void_full_rad)
            F.write("error-r_avg-voids_total=%f\n" % stderr_med_void_full)
            if(self.split_position_in_void):
                F.write("r_avg-inner-voids=%f\n" % med_inner_void_rad)
                F.write("error-r_avg-inner-voids=%f\n" % stderr_med_inner_void)
                F.write("N_inner-void-gals=%i\n" % N_inner_void)
                F.write("r_avg-outer-voids=%f\n" % med_outer_void_rad)
                F.write("error-r_avg-outer-voids=%f\n" % stderr_med_outer_void)
                F.write("N_outer-void-gals=%i\n" % N_outer_void)
            if(split_walls_filaments):
                F.write("r_avg-walls=%f\n" % med_wall_rad)
                F.write("error-r_avg-walls=%f\n" % stderr_med_wall)
                F.write("N_wall-gals=%i\n" % N_wall)
                F.write("r_avg-filaments=%f\n" % med_filament_rad)
                F.write("error-r_avg-filaments=%f\n" % stderr_med_filament)
                F.write("N_filament-gals=%i\n" % N_filament)
            F.close()

        with open("galaxy_spin.dat","w+") as F:
            F.write("N_gals_total=%i\n" % len(set(w_total)))
            F.write("N_gals_in_interval=%i\n" % len(set(w)))
            F.write("N_void-gals_total=%i\n" % N_void_full_spin)
            F.write("N_non-void-gals_total=%i\n" % N_non_void_full_spin)
            F.write("N_void-gals=%i\n" % N_void_spin)
            F.write("N_non-void-gals=%i\n" % N_non_void_spin)
            F.write("spin_avg-nonvoids=%f\n" % med_non_void_spin)
            F.write("error-spin_avg-nonvoids=%f\n" % stderr_med_non_void_spin)
            F.write("spin_avg-voids=%f\n" % med_void_spin)
            F.write("error-spin_avg-voids=%f\n" % stderr_med_void_spin)
            F.write("spin_avg-nonvoids_total=%f\n" % med_non_void_full_spin)
            F.write("error-spin_avg-nonvoids_total=%f\n" % stderr_med_non_void_full_spin)
            F.write("spin_avg-voids_total=%f\n" % med_void_full_spin)
            F.write("error-spin_avg-voids_total=%f\n" % stderr_med_void_full_spin)
            if(self.split_position_in_void):
                F.write("spin_avg-inner-voids=%f\n" % med_inner_void_spin)
                F.write("error-spin_avg-inner-voids=%f\n" % stderr_med_inner_void_spin)
                F.write("N_inner-void-gals=%i\n" % N_inner_void_spin)
                F.write("spin_avg-outer-voids=%f\n" % med_outer_void_spin)
                F.write("error-spin_avg-outer-voids=%f\n" % stderr_med_outer_void_spin)
                F.write("N_outer-void-gals=%i\n" % N_outer_void_spin)
            if(split_walls_filaments):
                F.write("spin_avg-walls=%f\n" % med_wall_spin)
                F.write("error-spin_avg-walls=%f\n" % stderr_med_wall_spin)
                F.write("N_wall-gals=%i\n" % N_wall_spin)
                F.write("spin_avg-filaments=%f\n" % med_filament_spin)
                F.write("error-spin_avg-filaments=%f\n" % stderr_med_filament_spin)
                F.write("N_filament-gals=%i\n" % N_filament_spin)
            F.close()

        with open("galaxy_rvir.dat","w+") as F:
            F.write("N_gals_total=%i\n" % len(set(w_total)))
            F.write("N_gals_in_interval=%i\n" % len(set(w)))
            F.write("N_void-gals_total=%i\n" % N_void_full_rvir)
            F.write("N_non-void-gals_total=%i\n" % N_non_void_full_rvir)
            F.write("N_void-gals=%i\n" % N_void_rvir)
            F.write("N_non-void-gals=%i\n" % N_non_void_rvir)
            F.write("rvir_avg-nonvoids=%f\n" % med_non_void_rvir)
            F.write("error-rvir_avg-nonvoids=%f\n" % stderr_med_non_void_rvir)
            F.write("rvir_avg-voids=%f\n" % med_void_rvir)
            F.write("error-rvir_avg-voids=%f\n" % stderr_med_void_rvir)
            F.write("rvir_avg-nonvoids_total=%f\n" % med_non_void_full_rvir)
            F.write("error-rvir_avg-nonvoids_total=%f\n" % stderr_med_non_void_full_rvir)
            F.write("rvir_avg-voids_total=%f\n" % med_void_full_rvir)
            F.write("error-rvir_avg-voids_total=%f\n" % stderr_med_void_full_rvir)
            if(self.split_position_in_void):
                F.write("rvir_avg-inner-voids=%f\n" % med_inner_void_rvir)
                F.write("error-rvir_avg-inner-voids=%f\n" % stderr_med_inner_void_rvir)
                F.write("N_inner-void-gals=%i\n" % N_inner_void_rvir)
                F.write("rvir_avg-outer-voids=%f\n" % med_outer_void_rvir)
                F.write("error-rvir_avg-outer-voids=%f\n" % stderr_med_outer_void_rvir)
                F.write("N_outer-void-gals=%i\n" % N_outer_void_rvir)
            if(split_walls_filaments):
                F.write("rvir_avg-walls=%f\n" % med_wall_rvir)
                F.write("error-rvir_avg-walls=%f\n" % stderr_med_wall_rvir)
                F.write("N_wall-gals=%i\n" % N_wall_rvir)
                F.write("rvir_avg-filaments=%f\n" % med_filament_rvir)
                F.write("error-rvir_avg-filaments=%f\n" % stderr_med_filament_rvir)
                F.write("N_filament-gals=%i\n" % N_filament_rvir)
            F.close()


# =================================================================


#  'Main' section of code.  This if statement executes if the code is run from the
#   shell command line, i.e. with 'python allresults.py'

if __name__ == '__main__':

    if os.environ.get('SPLIT_POSITION_IN_VOID'):
        split=os.environ.get('SPLIT_POSITION_IN_VOID')
        if(split=="YES"):
            split_position_in_void = True
        else:
            split_position_in_void = False

    from optparse import OptionParser
    import os
    import cProfile

    parser = OptionParser()
    parser.add_option(
        '-d',
        '--dir_name',
        dest='DirName',
        default='./',
        help='input directory name (default: ./millennium/)',
        metavar='DIR',
        )
    parser.add_option(
        '-f',
        '--file_base',
        dest='FileName',
        default='model_z-0.000',
        help='filename base (default: model_z0.000)',
        metavar='FILE',
        )
    parser.add_option(
        '-n',
        '--file_range',
        type='int',
        nargs=2,
        dest='FileRange',
        default=(0, 7),
        help='first and last filenumbers (default: 0 7)',
        metavar='FIRST LAST',
        )
    parser.add_option(
        '-t',
        '--tracer_file_base',
        dest='TracerFile',
        default='./out_gadget',
        help='path and filename of tracer file',
        metavar='FILE',
        )
    parser.add_option(
        '-b',
        '--halo-dir',
        dest='HaloDir',
        default='../haloes/',
        help='path to the output of the halo finder',
        metavar='DIR',
        )
    parser.add_option(
        '-v',
        '--void-dir',
        dest='VoidDir',
        default='../voids/outputpotcentres',
        help='path of the void file',
        metavar='FILE',
        )
    parser.add_option(
        '-o',
        '--void-file',
        dest='VoidFile',
        default='zobov-Voids_potC_cat.txt',
        help='filename of the void file',
        metavar='FILE',
        )

    parser.add_option(
        '-p',
        '--print_png',
        dest='PrintPNGFiles',
        default=False,
        help='print png files',
        )

    (opt, args) = parser.parse_args()

    if opt.DirName[-1] != '/':
        opt.DirName += '/'

    OutputDir = 'LSBG_plots'
    if not os.path.exists(OutputDir):
        os.makedirs(OutputDir)

    res = Results()

    tracer_file = opt.TracerFile
    void_dir = opt.VoidDir
    haloes_dir = opt.HaloDir
    void_file = opt.VoidDir + '/' + opt.VoidFile # assumes POSIX

    print("void_dir=",void_dir)
    print("haloes_dir=",haloes_dir)
    print("void_file=",void_file)

    if opt.PrintPNGFiles:
        create_png_files = True
    else:
        create_png_files = False

    print("create_png_files = ",create_png_files)
    print("type(create_png_files) = ",type(create_png_files))

    profile_object = cProfile.Profile()
    profile_object.enable()

    FirstFile = opt.FileRange[0]
    LastFile = opt.FileRange[1]
    last_snap_number = sys.argv[1]
    print("last_snap_number=",last_snap_number)

    sage_dir = opt.DirName
    fin_base = opt.DirName + opt.FileName
    G = res.read_gals(fin_base, FirstFile, LastFile)
    w_lsbg = res.identify_lsbgs(G)
    if(split_walls_filaments and split_position_in_void):
        w_void, w_wall, w_filament, w_inner_void, w_outer_void = res.void_gals(G,void_file, void_dir, haloes_dir, last_snap_number)
    else:
        if(split_walls_filaments):
            w_void, w_wall, w_filament = res.void_gals(G,void_file, void_dir, haloes_dir, last_snap_number)
        elif(split_position_in_void):
            w_void, w_inner_void, w_outer_void = res.void_gals(G,void_file, void_dir, haloes_dir, last_snap_number)
        else:
            w_void = res.void_gals(G,void_file, void_dir, haloes_dir, last_snap_number)

    if(len(w_void) < 1):
        raise RuntimeError('No galaxies were found in voids. Cannot continue further.')

    w_void_lsbg = res.void_lsbgs(w_lsbg, w_void)

    # make subsamples
    w_void_sub = res.create_subsample_of_indices(G, w_void)
    if(split_position_in_void):
        w_inner_void_sub = res.create_subsample_of_indices(G, w_inner_void)
        w_outer_void_sub = res.create_subsample_of_indices(G, w_outer_void)
    else:
        w_inner_void_sub = []
        w_outer_void_sub = []
    if(split_walls_filaments):
        w_wall_sub = res.create_subsample_of_indices(G, w_wall)
        w_filament_sub = res.create_subsample_of_indices(G, w_filament)
    else:
        w_wall_sub = []
        w_filament_sub = []


    res.gals_infall(G)
    res.gals_sfr(G)
    res.plot_void_gals(G, w_void, w_void_lsbg)
    res.print_statistics(G, w_void_sub, w_lsbg, w_void_lsbg, w_wall_sub, w_filament_sub,
                         w_inner_void_sub, w_outer_void_sub, w_void,
                         radius_factor = 1.0)

    if (create_png_files or (OutputFormat != '.png')):
        res.StellarMassFunction(G,w_lsbg, w_void_lsbg)
        res.BaryonicMassFunction(G,w_lsbg, w_void_lsbg)
        #res.GasMassFunction(G)
        #res.BaryonicTullyFisher(G)
        res.SpecificStarFormationRate(G, w_lsbg)
        #res.GasFraction(G)
        #res.Metallicity(G)
        #res.BlackHoleBulgeRelationship(G)
        #res.QuiescentFraction(G)
        #res.BulgeMassFraction(G)
        #res.BaryonFraction(G)
        res.SpinDistribution(G,w_lsbg, w_void_lsbg)
        #res.VelocityDistribution(G)
        #res.MassReservoirScatter(G)
        #res.SpatialDistribution(G)



    profile_object.enable()
    profile_object.print_stats()
