#!/usr/bin/env python

# plot_void_galaxies - plot particles, galaxies and voids
# (C) 2020 Boud Roukema GPL-3+
#
# Plots for sanity checking of the XYZ placements of particles and
# the galaxies and voids that are identified using them.
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import matplotlib.markers as markers
from mpl_toolkits.mplot3d import axes3d

debug = False

## Required input files
void_particle_filename = 'plots/xyz_particle_id.txt'
gal_in_void_id_filename = 'plots/gal_in_void_ID.dat'
void_particle_ids_filename = 'voids/Gadget_Ids_within_rad.txt'
halo_particle_ids_filename = 'plots/particle_ids_of_haloes_of_gals_in_voids.txt'

def voids_gals_read_data():
    # Read x,y,z positions and rockstar particle identities
    xyz = np.loadtxt(void_particle_filename, usecols=[0,1,2], dtype=np.float)
    particle_ids = np.loadtxt(void_particle_filename, usecols=3, dtype=np.int64)

    # Number of particles
    N_part = (xyz.shape)[0]
    print('N_part=', N_part)

    # Read associated galaxy and void identity numbers:
    gal_ids = np.loadtxt(gal_in_void_id_filename, usecols=0, dtype=np.int64)
    void_ids = np.loadtxt(gal_in_void_id_filename, usecols=3, dtype=np.int64)
    print('void_ids = ',void_ids)

    # Read in the particle identities of each halo associated with a galaxy:
    halo_particle_lists = []
    with open(halo_particle_ids_filename,'r') as halo_file:
        for line in halo_file:
            halo_particle_lists.append(np.fromstring(line,
                                                     dtype=np.int64, sep=' '))
    #print('halo_particle_lists = ',halo_particle_lists)

    void_particle_lists = []
    voids_with_particles_list = []

    # Read void particle identities
    line_count = 0
    have_new_void_id = False

    with open(void_particle_ids_filename, 'r') as void_particle_ids:
        for line in void_particle_ids:
            line_count += 1 # first line is 1
            # If this is a line with a void number:
            if(0 == line_count%2):
                this_void = np.fromstring(line, dtype=np.int64, count=1, sep=' ')
                #print('this_void= ',this_void)
                if(np.in1d(this_void,void_ids)):
                    have_new_void_id = True
                    #print('found a void entry')
                    # If we are ready to read particle identities for a new void
                    # associated with at least one galaxy, then do so:
            if(line_count > 1
               and 1 == line_count%2 and have_new_void_id):
                #print('Will append void particle data to list.')
                # add an array to the list of arrays
                voids_with_particles_list.append(this_void)
                void_particle_lists.append (np.fromstring(line, dtype=np.int64,
                                                          sep=' '))
                have_new_void_id = False

    void_particle_ids.close()

    print('voids_with_particles_list=',voids_with_particles_list)
    if(debug):
        print('void_particle_lists=',void_particle_lists)

    return (N_part, xyz, particle_ids,
            gal_ids, void_ids, halo_particle_lists,
            void_particle_lists, voids_with_particles_list)


def voids_gals_plot_data(N_part, xyz, particle_ids,
                         gal_ids, void_ids, halo_particle_lists,
                         void_particle_lists, voids_with_particles_list):
    # prepare sort key
    xyz_rows_particle_ids = np.zeros((N_part,2),dtype=np.int64)
    ## The first column of xyz_rows_particle_ids will contain row identity
    ## numbers in the python/numpy row convention;
    xyz_rows_particle_ids[:,0] = (np.arange(N_part))
    ## the second column contains the particle identity numbers.
    xyz_rows_particle_ids[:,1] = particle_ids
    print('xyz_rows_particle_ids=\n',xyz_rows_particle_ids)

    #print('xxx',xyz_rows_particle_ids[:,1].argsort())

    xyz_rows_particle_ids_sorted = (
        xyz_rows_particle_ids[xyz_rows_particle_ids[:,1].argsort(),:] )

    print('xyz_rows_particle_ids_sorted=\n',xyz_rows_particle_ids_sorted)

    ### 2D plots ###

    # Start a plot
    fig = plt.figure()
    i_sub=0
    xylabels=['x','y','z']

    #i1=0; i2=1 # choose axes
    for plane in [[0,1], [2,1], [0,2]]:
        i1=plane[0]
        i2=plane[1]
        i_sub += 1
        plt.subplot(2,2,i_sub)


        # Add the full particle distribution
        plt.scatter(xyz[:,i1], xyz[:,i2], marker='.', c='k', s=0.2)
        plt.xlabel(xylabels[i1])
        plt.ylabel(xylabels[i2])

        N_voids = len(voids_with_particles_list)
        N_gals_in_voids = (gal_ids.shape)[0]
        print('N_voids = ',N_voids,' N_gals_in_voids=',N_gals_in_voids)
        colour_list = cm.rainbow(np.linspace(0, 1, N_gals_in_voids))
        #print('Available markers = ',markers.MarkerStyle.markers)
        #print('Available fillstyles = ',markers.MarkerStyle.fillstyles)

        i_gal = -1
        #for void_particle_list in void_particle_lists:
        for gal_id_tuple in zip(gal_ids, void_ids, halo_particle_lists):
            i_gal += 1
            #print('plotting: voids_with_particles_list[i_void]=',voids_with_particles_list[i_void])
            ## Find which rows of xyz should contain positions
            ## for this particular galaxy.
            #print('gal_id_tuple[2]=',gal_id_tuple[2])
            i_rows_gal = (
                ## The -1 converts from particle IDs to python row numbers.
                xyz_rows_particle_ids_sorted[gal_id_tuple[2]-1,0] )
            #print('i_rows_gal=',i_rows_gal)
            #print('xyz=',xyz[i_rows_gal,i1])

            # Find the void_particle_list for this galaxy.
            void_id = gal_id_tuple[1]
            #print('gal_id_tuple[1]=',gal_id_tuple[1])
            index = ((np.where(np.array(voids_with_particles_list) == void_id))[0])[0]
            #print('index = ',index)
            void_particle_list = (np.array(void_particle_lists))[index]
            #print('void_particle_list = ',void_particle_list)

            ## Find which rows of xyz should contain positions
            ## for this particular void.
            i_rows_void = (
                ## The -1 converts from particle IDs to python row numbers.
                xyz_rows_particle_ids_sorted[void_particle_list-1,0] )

            #mmm = markers.MarkerStyle(marker='s',fillstyle='none')
            plt.scatter(xyz[i_rows_void,i1],
                        xyz[i_rows_void,i2],
                        marker='o',
                        edgecolors=colour_list[i_gal],
                        facecolors='none',
                        s=20)
            plt.scatter(xyz[i_rows_gal,i1],
                        xyz[i_rows_gal,i2],
                        marker='X',
                        c=colour_list[i_gal],
                        #facecolors='none',
                        s=20)
            ## Arbitrarily make one galaxy particle in black and make it bigger;
            ## this is not necessarily the "centre" or most-bound particle.
            plt.scatter(xyz[i_rows_gal[0],i1],
                        xyz[i_rows_gal[0],i2],
                        marker='X',
                        c='k',
                        #facecolors='none',
                        s=100)
    plt.show()

    ## 3D plot (slow!) ##

    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111,projection='3d')
    ax2.scatter3D(xyz[:,0], xyz[:,1], xyz[:,2], marker='.', c='k', s=0.2)
    #plt.xlabel(xylabels[0])
    #plt.ylabel(xylabels[1])
    #plt.zlabel(xylabels[2])

    i_gal = -1
    #for void_particle_list in void_particle_lists:
    for gal_id_tuple in zip(gal_ids, void_ids, halo_particle_lists):
        i_gal += 1
        ## Find which rows of xyz should contain positions
        ## for this particular galaxy.
        i_rows_gal = (
            ## The -1 converts from particle IDs to python row numbers.
            xyz_rows_particle_ids_sorted[gal_id_tuple[2]-1,0] )

        # Find the void_particle_list for this galaxy.
        void_id = gal_id_tuple[1]
        index = ((np.where(np.array(voids_with_particles_list) == void_id))[0])[0]
        void_particle_list = (np.array(void_particle_lists))[index]

        ## Find which rows of xyz should contain positions
        ## for this particular void.
        i_rows_void = (
            ## The -1 converts from particle IDs to python row numbers.
            xyz_rows_particle_ids_sorted[void_particle_list-1,0] )

        ax2.scatter3D(xyz[i_rows_void,0],
                    xyz[i_rows_void,1],
                    xyz[i_rows_void,2],
                    marker='o',
                    edgecolors=colour_list[i_gal],
                    facecolors='none',
                    s=20)
        ax2.scatter3D(xyz[i_rows_gal,0],
                      xyz[i_rows_gal,1],
                      xyz[i_rows_gal,2],
                      marker='X',
                      c=colour_list[i_gal],
                      #facecolors='none',
                      s=20)
        ## Arbitrarily make one galaxy particle in black and make it bigger;
        ## this is not necessarily the "centre" or most-bound particle.
        ax2.scatter3D(xyz[i_rows_gal[0],0],
                      xyz[i_rows_gal[0],1],
                      xyz[i_rows_gal[0],2],
                      marker='X',
                      c='k',
                      #facecolors='none',
                      s=100)

    plt.show()


if __name__ == '__main__':

    print('Interactive use:')
    print('Enable `print_xyz_particle_ids = True` in allresults_LSBGs.py')
    print('and run that script, e.g. with `./project make analyse-plot`.')
    print('Run this script in the directory `.build/` ')
    print('with a command such as  ./software/installed/bin/plot_void_galaxies.py .')
    print('The `maneage` environment is preferentially non-interactive and might not')
    print('provide python interactive plotting libraries.')

    (N_part, xyz, particle_ids,
     gal_ids, void_ids, halo_particle_lists,
     void_particle_lists, voids_with_particles_list) = (
         voids_gals_read_data() )

    voids_gals_plot_data(N_part, xyz, particle_ids,
                         gal_ids, void_ids, halo_particle_lists,
                         void_particle_lists, voids_with_particles_list)
