#!/usr/bin/env python

# theil_sen_2param - Theil-Sen two-parameter linear regression
# (C) 2018-2020 Boud Roukema GPL-3+
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.


## TODO: the nanmedians should be checked and preferably removed.
## A likely source for nans is '/ (dx1dx0_01 - dx1dx0_02)' in
## special cases.


import sys
import warnings

import numpy as np
from numpy.random import SeedSequence, default_rng
import matplotlib
import matplotlib.pyplot as plt

debug = False

def theil_sen_2param(x_in0, x_in1, y_in,
                     N_triples_max = 500,
                     tol_abs = 1e-7):
    """
    Theil-Sen linear regression - robust linear fit
    e.g. https://en.wikipedia.org/w/index.php?title=Theil%E2%80%93Sen_estimator&oldid=865140889
    input parameters:
    *
    *                  x_in0 (independent, numpy array)
    *                  x_in1 (independent, numpy array)
    *                  y_in (dependent, numpy array)
    """

    ## TODO: error feedback for cases with two few points
    N_x1 = x_in0.size;
    N_x2 = x_in1.size;
    N_y = y_in.size;
    if (N_x1 != N_y or N_x1 != N_x2):
        raise RuntimeError("theil_sen: Error: N_x1, N_x2, N_y must all be equal:",
                           N_x1,N_x2,N_y)

    if(N_triples_max < 2):
        raise RuntimeError('theil_sen_2param: Warning: N_triples_max = '+
                           str(N_triples_max)+
                           ' is most likely too low')
    elif(N_triples_max < 10):
        warnings.warn('theil_sen_2param: Warning: N_triples_max = '+
                      str(N_triples_max)+
                      ' is most likely too low.')
    ## gaussian scatter
    seeds = SeedSequence(6967)
    seed = seeds.spawn(2)
    rng = default_rng(seed[0])

    ## For reasons of speed, test only a random subset of slopes.
    #N_triples_max = 500
    if(debug):
        N_triples_max = 4
    # Generate some indices up to the size N_x1.
    index0_try1 = rng.integers(N_x1, size=N_triples_max)
    index1_try1 = rng.integers(N_x1, size=N_triples_max)
    index2_try1 = rng.integers(N_x1, size=N_triples_max)
    if(debug):
        print("index._try1 = ",index0_try1, index1_try1, index2_try1)

    # Remove index triples that are identical
    ii = (np.array(np.where(
        (index0_try1 != index1_try1) & (index0_try1 != index2_try1)
        & (index1_try1 != index2_try1))))[0]
    if(debug):
        print("ii = ",ii)
    index0_try2 = index0_try1[ii]
    index1_try2 = index1_try1[ii]
    index2_try2 = index2_try1[ii]
    if(debug):
        print("index._try2 = ",index0_try2, index1_try2, index2_try2)

    # Remove index triples where x0 values are identical
    jj = (np.array(np.where(
        (np.abs(x_in0[index0_try2] - x_in0[index1_try2]) > tol_abs) &
        (np.abs(x_in0[index0_try2] - x_in0[index2_try2]) > tol_abs) &
        (np.abs(x_in0[index1_try2] - x_in0[index2_try2]) > tol_abs) )))[0]
    index0_try3 = index0_try2[jj]
    index1_try3 = index1_try2[jj]
    index2_try3 = index2_try2[jj]
    if(debug):
        print("index._try3 = ",index0_try3, index1_try3, index2_try3)

    # Remove index triples where x1 values are identical
    kk = (np.array(np.where(
        (np.abs(x_in1[index0_try2] - x_in1[index1_try2]) > tol_abs) &
        (np.abs(x_in1[index0_try2] - x_in1[index2_try2]) > tol_abs) &
        (np.abs(x_in1[index1_try2] - x_in1[index2_try2]) > tol_abs) )))[0]
    index0 = index0_try2[kk]
    index1 = index1_try2[kk]
    index2 = index2_try2[kk]
    if(debug):
        print("index. = ",index0, index1, index2)


    slopes0 = np.array([])
    slopes1 = np.array([])
    for indices in zip(index0,index1,index2):
        # Solve the system of linear equations for y = const + dydx0 * x_in0 + dydx1 * x_in1
        dydx0_01 = (y_in[index1] - y_in[index0])/(x_in0[index1] - x_in0[index0])
        dydx0_02 = (y_in[index2] - y_in[index0])/(x_in0[index2] - x_in0[index0])
        dx1dx0_01 = (x_in1[index1] - x_in1[index0])/(x_in0[index1] - x_in0[index0])
        dx1dx0_02 = (x_in1[index2] - x_in1[index0])/(x_in0[index2] - x_in0[index0])

        dydx1 = (dydx0_01 - dydx0_02) / (dx1dx0_01 - dx1dx0_02)
        dydx0 = dydx0_01 - dydx1 * dx1dx0_01
        slopes0 = np.append(slopes0,dydx0)
        slopes1 = np.append(slopes1,dydx1)
        if(debug):
            print("dydx0, dydx1 = ",dydx0, dydx1)

    dydx0_med = np.median(slopes0)
    dydx1_med = np.median(slopes1)
    if(debug):
        print("dydx0_med, dydx1_med = ",dydx0_med, dydx1_med)

    pzeropoint = np.median(y_in - (dydx0_med * x_in0 + dydx1_med * x_in1))
    if(debug):
        print("pzeropoint = ",pzeropoint)

    return (dydx0_med, dydx1_med, pzeropoint)


def theil_sen_2param_robust_stderr(x_in0, x_in1, y_in,
                     N_triples_max = 500):
    """
    bootstrap estimate of uncertainty in Theil-Sen zeropoint
    """

    ## TODO: error feedback for cases with two few points

    ## sigma is 1.4826 * median absolute deviation for Gaussian
    sig_from_MAD = 1.4826;

    N_boot = 100;
    ps0 = np.zeros(N_boot)
    ps1 = np.zeros(N_boot)
    pz = np.zeros(N_boot)
    N_x = x_in0.size

    seeds = SeedSequence(4733)
    seed = seeds.spawn(2)
    rng = default_rng(seed[0])
    #rng = default_rng()

    for i in np.arange(N_boot):
        ## bootstrap indices - indices allowing resampling
        j = rng.integers(N_x, size=N_x)
        (ps0[i], ps1[i], pz[i]) = (
            theil_sen_2param(x_in0[j], x_in1[j], y_in[j],
                             N_triples_max) )

    ps0_median = np.nanmedian(ps0)
    ps1_median = np.nanmedian(ps1)
    pz_median = np.nanmedian(pz)
    ## robust estimate of variation
    sig_pslope0 = np.nanmedian(np.abs(ps0-ps0_median)) * sig_from_MAD;
    sig_pslope1 = np.nanmedian(np.abs(ps1-ps1_median)) * sig_from_MAD;
    sig_pzeropoint = np.nanmedian(np.abs(pz-pz_median)) * sig_from_MAD;

    return sig_pslope0, sig_pslope1, sig_pzeropoint


def test_theil_sen():
    ## interactive test parameters
    show_plot = True

    N_x_gauss = 50
    if(debug):
        N_x_gauss = 5
    noise_amp = 0.1 # 1.0
    N_x_outlier = 5
    if(debug):
        N_x_outlier = 2
    outl_noise_amp = 0.1 # 2.0

    N_x = N_x_gauss + N_x_outlier

    true_slope0 = np.pi
    true_slope1 = -np.pi
    true_zeropoint = np.e
    outlier_slope0 = -2.0
    outlier_slope1 = true_slope1 # Initial test = not too difficult
    outlier_zeropoint = 3.0

    ## gaussian scatter
    seeds = SeedSequence(4547)
    seed = seeds.spawn(2)
    rng = default_rng(seed[0])
    #rng = default_rng()

    # Choose some x values randomly in the range from 0 to 1
    x0 = rng.uniform(size=N_x)
    x1 = rng.uniform(size=N_x)

    ## Create a linear relation with a little Gaussian noise
    ii_gauss = np.arange(N_x_gauss)
    y = np.zeros(N_x)
    y[ii_gauss] = (true_slope0 * x0[ii_gauss] +
                   true_slope1 * x1[ii_gauss] +
                   true_zeropoint +
                   rng.normal(size=N_x_gauss)*noise_amp)

    ## Add some outliers - with a different slope, zeropoint, noise, and add
    ## a square component:
    ii_outl = np.arange(N_x_gauss,N_x)
    y[ii_outl] = (outlier_slope0*x0[ii_outl] +
                  outlier_slope1*x1[ii_outl] +
                  outlier_zeropoint -
                  (x0[ii_outl]**2) +
                  rng.normal(size=N_x_outlier) * outl_noise_amp)

    if(show_plot):
        matplotlib.use('Agg')
        plt.figure()
        plt.scatter(x0[ii_gauss],y[ii_gauss],marker='o',c='g',label='good')
        plt.scatter(x0[ii_outl],y[ii_outl],marker='x',c='r',label='bad')
        plt.savefig('theil_sen_test_x0.eps')
        plt.close()

        plt.clf()
        plt.figure()
        plt.scatter(x1[ii_gauss],y[ii_gauss],marker='o',c='g',label='good')
        plt.scatter(x1[ii_outl],y[ii_outl],marker='x',c='r',label='bad')
        plt.savefig('theil_sen_test_x1.eps')
        plt.close()
        print("Have created plots theil_sen*.eps")


    print("\n\ntrue: %g %g %g\n" % (true_slope0, true_slope1, true_zeropoint))
    #pp = np.polyfit(x0, y, 1)
    #print("polyfit: %g %g\n" % (pp[0], pp[1]))
    if(debug):
        print("Ready to call theil_sen_2param; the input parameters will be:")
        print("x0 = ",x0)
        print("x1 = ",x1)
        print("y = ",y)
    pslope0, pslope1, pzeropoint = ( theil_sen_2param(x0, x1, y,
                                                      N_triples_max = 500) )
    print("theil-sen: %g %g %g\n" % (pslope0, pslope1, pzeropoint))

    sig_pslope0, sig_pslope1, sig_pzeropoint = (
        theil_sen_2param_robust_stderr(x0,x1,y, N_triples_max = 500) )
    print("sigma theil-sen: %g %g %g\n" % (sig_pslope0, sig_pslope1, sig_pzeropoint))

    #plot([0 1], pslope.*[0 1] .+ pzeropoint,'-k;Theil--Sen;','linewidth',5)
    #plot([0 1], pp(1).*[0 1] .+ pp(2),'-b;polyfit;','linewidth',5)

    pass_value = 0
    if( np.abs(pslope0-true_slope0)/sig_pslope0 > 3.0 ): # 3 sigma error
        pass_value +=1
    if( np.abs(pslope1-true_slope1)/sig_pslope1 > 3.0 ): # 3 sigma error
        pass_value +=2
    if( np.abs(pzeropoint-true_zeropoint)/sig_pzeropoint > 3.0 ): # 3 sigma error
        pass_value +=4

    return pass_value


if __name__ == '__main__':

    # pass_val value of test: 0 = OK, 1 = fail

    pass_val = test_theil_sen() # interactive

    print("pass_val value = ",pass_val)
    sys.exit(pass_val)

#end __main__
