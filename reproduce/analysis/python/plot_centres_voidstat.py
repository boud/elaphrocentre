#!/usr/bin/env python

# plot_centres_voidstat - This script mostly analyses the voids
# that were found in our simulation. It calculates the elaphro-
# acceleration. Moreover, it includes a check for the order of
# magnitude for the potentials and it analyses the positions
# and distances of the various centres that can be defined for
# a void. Several plots can be generated to understand the potential
# and the positions of the centres in a void.
# (C) 2019-2020 Marius Peper, Boud Roukema GPL-3+
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import matplotlib
matplotlib.use('Agg')
import sys
import numpy as np
from scipy.interpolate import interpn
from scipy.interpolate import LinearNDInterpolator
from scipy.interpolate import interp1d
import scipy.integrate as integrate
#from ctypes import *
import ctypes
from itertools import product, combinations
import random
from random import sample, seed, shuffle
from os.path import getsize as getFileSize

import os

macOS_Darwin = False
if os.environ.get('LD_LIBRARY_PATH'):
    print("os.environ.get('LD_LIBRARY_PATH') was set as an evironment variable and passed to python.")
    print("No macOS/Darwin hack is needed: os.environ.get('LD_LIBRARY_PATH') = ",
          os.environ.get('LD_LIBRARY_PATH'))
else:
    print("os.environ.get('LD_LIBRARY_PATH') was either *not* set as an evironment variable or *not* passed to python.")
    print("os.environ.get('LD_LIBRARY_PATH_PYTHON') = ",
          os.environ.get('LD_LIBRARY_PATH_PYTHON'))
    os.environ['LD_LIBRARY_PATH'] = os.environ.get('LD_LIBRARY_PATH_PYTHON')
    io_gadget_with_path = os.environ.get('IO_GADGET_WITH_PATH')
    macOS_Darwin = True
    print("After macOS/Darwin hack: os.environ.get('LD_LIBRARY_PATH') = ",
          os.environ.get('LD_LIBRARY_PATH'),
          " io_gadget_with_path = ",io_gadget_with_path)

if(macOS_Darwin):
    read_gadget = ctypes.CDLL(io_gadget_with_path)
else:
    read_gadget = ctypes.CDLL("io_gadget.so")
#read_gadget = ctypes.CDLL("io_gadget.so")

from numpy.ctypeslib import ndpointer
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from extra_calculations import calculations
from plot_voidgals_properties import voidgals_acc_plot
from infall_stat import median_stats

## Benchmarking
#import time

debug_phase = False
print_potentials = False

enable_density_centre = False
enable_geom_centre = False
enable_pot_centre = True
enable_circum_centre = False

Julian_Gyr_in_seconds = (np.float(1.0e9) * 365.25 * 24.0 * 3600.0)

class Results:
# Taken from sage's 'allresults.py'

    def read_gals(self, model_name, first_file, last_file):

        # The input galaxy structure:
        Galdesc_full = [
            ('SnapNum'                      , np.int32),
            ('Type'                         , np.int32),
            ('GalaxyIndex'                  , np.int64),
            ('CentralGalaxyIndex'           , np.int64),
            ('SAGEHaloIndex'                , np.int32),
            ('SAGETreeIndex'                , np.int32),
            ('SimulationHaloIndex'          , np.int64),
            ('mergeType'                    , np.int32),
            ('mergeIntoID'                  , np.int32),
            ('mergeIntoSnapNum'             , np.int32),
            ('dT'                           , np.float32),
            ('Pos'                          , (np.float32, 3)),
            ('Vel'                          , (np.float32, 3)),
            ('Spin'                         , (np.float32, 3)),
            ('Len'                          , np.int32),
            ('Mvir'                         , np.float32),
            ('CentralMvir'                  , np.float32),
            ('Rvir'                         , np.float32),
            ('Vvir'                         , np.float32),
            ('Vmax'                         , np.float32),
            ('VelDisp'                      , np.float32),
            ('ColdGas'                      , np.float32),
            ('StellarMass'                  , np.float32),
            ('BulgeMass'                    , np.float32),
            ('HotGas'                       , np.float32),
            ('EjectedMass'                  , np.float32),
            ('BlackHoleMass'                , np.float32),
            ('IntraClusterStars'            , np.float32),
            ('MetalsColdGas'                , np.float32),
            ('MetalsStellarMass'            , np.float32),
            ('MetalsBulgeMass'              , np.float32),
            ('MetalsHotGas'                 , np.float32),
            ('MetalsEjectedMass'            , np.float32),
            ('MetalsIntraClusterStars'      , np.float32),
            ('SfrDisk'                      , np.float32),
            ('SfrBulge'                     , np.float32),
            ('SfrDiskZ'                     , np.float32),
            ('SfrBulgeZ'                    , np.float32),
            ('DiskRadius'                   , np.float32),
            ('Cooling'                      , np.float32),
            ('Heating'                      , np.float32),
            ('QuasarModeBHaccretionMass'    , np.float32),
            ('TimeOfLastMajorMerger'         , np.float32),
            ('TimeOfLastMinorMerger'         , np.float32),
            ('OutflowRate'                  , np.float32),
            ('infallMvir'                   , np.float32),
            ('infallVvir'                   , np.float32),
            ('infallVmax'                   , np.float32)
        ]
        names = [Galdesc_full[i][0] for i in range(len(Galdesc_full))]
        formats = [Galdesc_full[i][1] for i in range(len(Galdesc_full))]
        Galdesc = np.dtype({'names':names, 'formats':formats}, align=True)


        # Initialize variables.
        TotNTrees = 0
        TotNGals = 0
        FileIndexRanges = []

        print("Determining array storage requirements.")

        # Read each file and determine the total number of galaxies to be read in
        goodfiles = 0
        for fnr in range(first_file,last_file+1):
            fname = model_name+'_'+str(fnr)  # Complete filename

            if not os.path.isfile(fname):
              # print "File\t%s  \tdoes not exist!  Skipping..." % (fname)
                continue

            if getFileSize(fname) == 0:
                print("File\t%s  \tis empty!  Skipping..." % (fname))
                continue

            fin = open(fname, 'rb')  # Open the file
            Ntrees = np.fromfile(fin,np.dtype(np.int32),1)  # Read number of trees in file
            NtotGals = np.fromfile(fin,np.dtype(np.int32),1)[0]  # Read number of gals in file.
            TotNTrees = TotNTrees + Ntrees  # Update total sim trees number
            TotNGals = TotNGals + NtotGals  # Update total sim gals number
            goodfiles = goodfiles + 1  # Update number of files read for volume calculation
            fin.close()

        print()
        print("Input files contain:\t%d trees ;\t%d galaxies ." % (TotNTrees, TotNGals))
        print()

        # Initialize the storage array
        G = np.empty(TotNGals, dtype=Galdesc)

        offset = 0  # Offset index for storage array

        # Open each file in turn and read in the preamble variables and structure.
        print("Reading in files.")
        for fnr in range(first_file,last_file+1):
            fname = model_name+'_'+str(fnr)  # Complete filename

            if not os.path.isfile(fname):
                continue

            if getFileSize(fname) == 0:
                continue

            fin = open(fname, 'rb')  # Open the file
            Ntrees = np.fromfile(fin, np.dtype(np.int32), 1)  # Read number of trees in file
            NtotGals = np.fromfile(fin, np.dtype(np.int32), 1)[0]  # Read number of gals in file.
            #GalsPerTree = np.fromfile(fin, np.dtype((np.int32, Ntrees)),1) # Read the number of gals in each tree
            print(":   Reading N=", NtotGals, "   \tgalaxies from file: ", fname)
            GG = np.fromfile(fin, Galdesc, NtotGals)  # Read in the galaxy structures

            FileIndexRanges.append((offset,offset+NtotGals))

            # Slice the file array into the global array
            # N.B. the copy() part is required otherwise we simply point to
            # the GG data which changes from file to file
            # NOTE THE WAY PYTHON WORKS WITH THESE INDICES!
            G[offset:offset+NtotGals]=GG[0:NtotGals].copy()

            del(GG)
            offset = offset + NtotGals  # Update the offset position for the global array

            fin.close()  # Close the file


        print()
        print("Total galaxies considered:", TotNGals)

        # Convert the Galaxy array into a recarray
        G = G.view(np.recarray)

        w = np.where(G.StellarMass > 1.0)[0]
        print("Galaxies more massive than 10^10Msun/h:", len(w))
        print()

        # Calculate the volume given the first_file and last_file
        #self.volume = self.BoxSize**3.0 * goodfiles / self.MaxTreeFiles

        return G


    def read_tracers(self, tracer_file):
        """
        The routine reads in the positions of the DM particles (the tracers) from the simulation. To read in the data
        the routine will call a function that is written in C. This routine only works on simulated
        data that is provided in the gadget format.

        Parameter
        ------------
        tracer_file : string
        String that contains the path and filename of the simulation file we want to use.

        returns
        ------------
        tracers : np.array(3, dtype=float)
        The x,y,z coordinates of all DM particles in our simulation.
        """

        read_gadget.get_particles.argtypes = [ctypes.c_char_p]
        read_gadget.read_gadget.argtypes = [ctypes.c_char_p]
        num_tracers=read_gadget.get_particles(tracer_file.encode('utf-8'))
        tracers = np.empty((num_tracers, 3))
        returnValue=np.empty((num_tracers, 4))

        read_gadget.read_gadget.restype = ndpointer(dtype=ctypes.c_float, shape=(num_tracers,4))

        returnValue=read_gadget.read_gadget(tracer_file.encode('utf-8'))

        tracers[:,0]=returnValue[:,0]
        tracers[:,1]=returnValue[:,1]
        tracers[:,2]=returnValue[:,2]

        return tracers

    def read_ids(self, tracer_file):
        """
        The routine reads in the IDs of the DM particles from the simulation. To read in the data
        the routine will call a function that is written in C. This routine only works on simulated
        data that is provided in the gadget format.

        Parameter
        ------------
        tracer_file : string
        String that contains the path and filename of the simulation file we want to use.

        returns
        ------------
        gadget_ids : np.array(dtype=int)
        The simulation IDs of all DM particles in our simulation.
        """

        read_gadget.get_particles.argtypes = [ctypes.c_char_p]
        read_gadget.read_gadget.argtypes = [ctypes.c_char_p]
        num_tracers=read_gadget.get_particles(tracer_file.encode('utf-8'))

        gadget_ids = np.empty(num_tracers)
        read_gadget.read_ids.restype = ndpointer(dtype=ctypes.c_long, shape=(num_tracers))
        returnValue=read_gadget.read_gadget(tracer_file.encode('utf-8'))
        returnValue=read_gadget.read_ids(tracer_file.encode('utf-8'))
        gadget_ids[:]=returnValue[:]

        return gadget_ids

    def read_pots(self, tracer_file):
        """
        The routine reads in the potentials of the DM particles from the simulation. To read in the data
        the routine will call a function that is written in C. This routine only works on simulated
        data that is provided in the gadget format.

        Parameter
        ------------
        tracer_file : string
        String that contains the path and filename of the simulation file we want to use.

        returns
        ------------
        pot : np.array(dtype=float)
        The gravitational potential at the positions of the DM particles.
        """

        read_gadget.get_particles.argtypes = [ctypes.c_char_p]
        read_gadget.read_gadget.argtypes = [ctypes.c_char_p]
        num_tracers=read_gadget.get_particles(tracer_file.encode('utf-8'))
        tracers = np.empty((num_tracers, 3))
        returnValue=np.empty((num_tracers, 4))
        potential_centre=True
        if(potential_centre):
            pot=np.empty(num_tracers)

        read_gadget.read_gadget.restype = ndpointer(dtype=ctypes.c_float, shape=(num_tracers,4))

        returnValue=read_gadget.read_gadget(tracer_file.encode('utf-8'))

        tracers[:,0]=returnValue[:,0]
        tracers[:,1]=returnValue[:,1]
        tracers[:,2]=returnValue[:,2]
        if(potential_centre):
            pot=returnValue[:,3]

        return pot


    def read_particle_ids_of_gals_in_voids(self):
        """
        This function opens a file produced in allresults_LSBG.py and reads in the information
        which particles are in haloes and in voids.

        Parameter
        ------------

        returns
        ------------
        halo_particle_lists: list
        A list of particles that are in voids and in haloes.
        """

        # TODO: make this more flexible
        halo_particle_ids_filename = 'particle_ids_of_haloes_of_gals_in_voids.txt'
        halo_particle_lists = []
        with open(halo_particle_ids_filename,'r') as halo_file:
            for line in halo_file:
                halo_particle_lists.append(np.fromstring(line,
                                                         dtype=np.int64, sep=' '))
        return halo_particle_lists

    def read_gal_in_void_ids(self):
        """
        This function opens a file produced in allresults_LSBG.py and reads in the information
        which galaxies are in voids.

        Parameter
        ------------

        returns
        ------------
        gal_ids: np.array(dtype=int)
        An array of galaxy IDs that are in voids.
        void_ids_of_gals: np.array(dtype=int)
        A corresponding array that defines in which void the galaxy is located.
        """

        # TODO: make this more flexible
        gal_in_void_id_filename = 'gal_in_void_ID.dat'
        gal_ids = np.loadtxt(gal_in_void_id_filename, usecols=0, dtype=np.int64)
        void_ids_of_gals = np.loadtxt(gal_in_void_id_filename, usecols=4, dtype=np.int64)

        return gal_ids, void_ids_of_gals


    def read_void_data(self, void_file):
        """
        This function reads in an output file from Revolver and saves the position
        of the density centre (default ZOBOV output).

        Parameter
        ------------
        void_file: string
        The path and filename of the input file.

        returns
        ------------
        density_centre: np.array(3, dtype=float)
        The positions of the particles which lay in the Vornoi cell with the lowest density
        for their void.
        """

        voiddata = np.loadtxt(void_file, skiprows=2)
        num_voids=len(voiddata)
        density_centre = np.empty((num_voids, 3))

        for i in range(0,num_voids):
            density_centre[i,0]=tracers[np.int(voiddata[i,1]),0]
            density_centre[i,1]=tracers[np.int(voiddata[i,1]),1]
            density_centre[i,2]=tracers[np.int(voiddata[i,1]),2]
        return density_centre

    def read_geom_data(self, geomcentre_file):
        """
        This function reads in an output file from Revolver and saves the position
        of the geometric centre (called the "volume-weighted barycentre" or
        "macrocentre" by Sutter+2015 and Nadathur+Hotchkiss2015).

        Parameter
        ------------
        geomcentre_file: string
        The path and filename of the input file.

        returns
        ------------
        geom_centre: np.array(3, dtype=float)
        The positions of the barycentre (geometric centre) of the voids.
        """

        geomcentredata =  np.loadtxt(geomcentre_file, skiprows=2)
        num_voids=len(geomcentredata)

        geom_centre = np.empty((num_voids, 3))

        geom_centre[:,0]=geomcentredata[:,1]
        geom_centre[:,1]=geomcentredata[:,2]
        geom_centre[:,2]=geomcentredata[:,3]

        return geom_centre

    def read_void_radius(self, geomcentre_file):
        """
        This function reads in an output file from Revolver and saves the effective
        radii for the voids. We could read it from any of Revolver's output files, the
        radius is the same in all of them.

        Parameter
        ------------
        geomcentre_file: string
          The path and filename of the input file.

        returns
        ------------
        eff_void_radius: np.array(dtype=float)
           An array with the effective radii of the voids.
        """

        geomcentredata =  np.loadtxt(geomcentre_file, skiprows=2)
        num_voids=len(geomcentredata)
        eff_void_radius = np.empty((num_voids))
        eff_void_radius = geomcentredata[:,4]

        return eff_void_radius

    def read_circ_centre(self, void_detail_file):
        """
        This function reads in an output file from Revolver and saves the position
        of the circumcentre (position of lowest density).

        Parameter
        ------------
        void_detail_file: string
           The path and filename of the input file.

        returns
        ------------
        circ_centre: np.array(3, dtype=float)
           The positions of the circumcentres of the voids.
        """

        voiddata = np.loadtxt(void_detail_file, skiprows=2)
        circ_centre = np.empty((num_voids, 3))

        circ_centre[:,0]=voiddata[:,1]
        circ_centre[:,1]=voiddata[:,2]
        circ_centre[:,2]=voiddata[:,3]

        return circ_centre

    def read_pot_centre(self, potcentre_file, num_voids):
        """
        This function reads in an output file (non default) from Revolver and saves the position
        of the potential centre (elaphrocentre).

        Parameter
        ------------
        potcentre_file: string
           The path and filename of the input file.
        num_voids: int
           The number of voids in the input file.

        returns
        ------------
        pot_centre: np.array(3, dtype=float)
           The positions of the potential centres of the voids.
        """

        voiddata = np.loadtxt(void_detail_file, skiprows=2)
        pot_centre = np.empty((num_voids, 3))

        pot_centre[:,0]=voiddata[:,1]
        pot_centre[:,1]=voiddata[:,2]
        pot_centre[:,2]=voiddata[:,3]

        return pot_centre

    def read_pot_centre_old(self, potcentre_file, num_voids):

        pot_centre = np.empty((num_voids, 3))
        k=0
        for line in open(potcentre_file,'r'):
            values=[float(s) for s in line.split( )]
            if(k%2 == 0):
                if(values[1] + 1 > 0.99):
                    pot_centre[k/2,0]=values[1]
                    pot_centre[k/2,1]=values[2]
                    pot_centre[k/2,2]=values[3]
                else:
                    pot_centre[k/2,0]=None
                    pot_centre[k/2,1]=None
                    pot_centre[k/2,2]=None
            k=k+1
        return pot_centre

    def read_void_particles(self, id_file):
        """
        This function reads in an output file (non default) from Revolver and saves the IDs of
        the DM particles that build up a void together with the void ID.

        Parameter
        ------------
        id_file: string
        The path and filename of the input file.

        returns
        ------------
        void_id_from_revolver: list(type=int)
        The IDs of the voids (to easily connect the particles to the void).
        member_ids: list(type=int)
        The IDs of the particles inside a void.
        """

        member_ids = []
        void_id_from_revolver = []
        k=0
        for line in open(id_file,'r'):
            if not line.startswith('#'):
                if(k%2 == 0):
                    # add a scalar void identity to the void_id list
                    this_voidID=np.fromstring(line, dtype=np.int, sep=' ')
                    void_id_from_revolver.append(this_voidID[0])
                else:
                    # associate a list of DM particle identities with the void
                    dm_particles_of_this_void=np.fromstring(line, dtype=np.int64, sep=' ')
                    member_ids.append(dm_particles_of_this_void)
                k += 1

        return void_id_from_revolver, member_ids

    def statistics(self, member_ids, tracers, pot_centre, circ_centre, geom_centre, eff_void_radius, num_voids):
        """
        This function evaluates how close the different centres in a void are to each other and prints
        it to the terminal. It sets one of the centres in the middle of the void and calculates
        the T^3 distances of the other centres to the chosen centre.

        Parameter
        ------------
        member_ids: list(type=int)
           The indices in the gadget file of the DM particles in the void.
           tracers: np.array(3,dtype=float)
           The positions of the DM particles.
        pot_centre: np.array(3,dtype=float)
           The positions of the potential centres.
        circ_centre: np.array(3,dtype=float)
           The positions of the circumcentres.
        geom_centre: np.array(3,dtype=float)
           The positions of the geomcentres.
        eff_void_radius: np.array(dtype=float)
           The effective radii of the voids.
        num_voids: int
           The number of voids in our simulation.

        returns
        ------------
        """

        calc=calculations()

        print('Void ID & Bary-Pot/R_{eff} & Den-Pot/R_{eff} & Bary-Den/R_{eff} & Circ-Pot/R_{eff} & Circ-Bary/R_{eff} & & Circ-Den/R_{eff} R_{eff} & N_{part} \\')
        for i in range(0,num_voids):
            members_x=tracers[member_ids[i],0]
            #members_y=tracers[member_ids[i],1]
            #members_z=tracers[member_ids[i],2]

            moved_pots=calc.move_coordinates(pot_centre[i,0], pot_centre[i,1], pot_centre[i,2], density_centre[i], box_length)
            moved_circ=calc.move_coordinates(circ_centre[i,0], circ_centre[i,1], circ_centre[i,2], density_centre[i], box_length)
            moved_barys=calc.move_coordinates(geom_centre[i,0], geom_centre[i,1], geom_centre[i,2], density_centre[i], box_length)
            #moved_members=calc.move_coordinates(members_x, members_y, members_z, density_centre, i)

            # now statistics
            #print 'print output statistics in LaTeX format for easy copying
            if not np.isnan(pot_centre[i,0]): # check the proper if condition because this is always true
                print(i, '&', np.sqrt(((moved_barys[0]-moved_pots[0])*(moved_barys[0]-moved_pots[0])) + ((moved_barys[1]-moved_pots[1])*(moved_barys[1]-moved_pots[1])) + ((moved_barys[2]-moved_pots[2])*(moved_barys[2]-moved_pots[2])))/eff_void_radius[i], '&', np.sqrt(((0-moved_pots[0])*(0-moved_pots[0])) + ((0-moved_pots[1])*(0-moved_pots[1])) + ((0-moved_pots[2])*(0-moved_pots[2])))/eff_void_radius[i], '&',  np.sqrt(((moved_barys[0]-0)*(moved_barys[0]-0)) + ((moved_barys[1]-0)*(moved_barys[1]-0)) + ((moved_barys[2]-0)*(moved_barys[2]-0)))/eff_void_radius[i], '&', np.sqrt(((moved_circ[0]-moved_pots[0])*(moved_circ[0]-moved_pots[0])) + ((moved_circ[1]-moved_pots[1])*(moved_circ[1]-moved_pots[1])) + ((moved_circ[2]-moved_pots[2])*(moved_circ[2]-moved_pots[2])))/eff_void_radius[i], '&', np.sqrt(((moved_circ[0]-moved_barys[0])*(moved_circ[0]-moved_barys[0])) + ((moved_circ[1]-moved_barys[1])*(moved_circ[1]-moved_barys[1])) + ((moved_circ[2]-moved_barys[2])*(moved_circ[2]-moved_barys[2])))/eff_void_radius[i], '&', np.sqrt(((moved_circ[0]-0)*(moved_circ[0]-0)) + ((moved_circ[1]-0)*(moved_circ[1]-0)) + ((moved_circ[2]-0)*(moved_circ[2]-0)))/eff_void_radius[i], '&', eff_void_radius[i], '&', len(members_x), '\\')
        return 0

    def do_plots(self, member_ids, tracers, gadget_ids, pot_centre, circ_centre, geom_centre, eff_void_radius, num_voids, halo_particle_lists, gal_ids, void_ids_of_gals, void_id_from_revolver, OutputDir):
        """
        This function plots the particles that build up the void and the centres. There
        will be a new plot for every void. Additionally it plots the DM particles in a
        halo/galaxy in a different color.

        Parameter
        ------------
        member_ids: list(type=int)
           The indices in the gadget file of the DM particles in the void.
        tracers: np.array(3,dtype=float)
           The positions of the DM particles.
        pot_centre: np.array(3,dtype=float)
           The positions of the potential centres.
        circ_centre: np.array(3,dtype=float)
           The positions of the circumcentres.
        geom_centre: np.array(3,dtype=float)
           The positions of the barycentres.
        eff_void_radius: np.array(dtype=float)
           The effective radii of the voids.
        num_voids: int
           The number of voids in our simulation.
        halo_particle_lists: list
           A list with all the IDs of particles that are in a halo.
        gal_ids: np.array(dtype=int)
           A list of galaxy IDs that are inside a void.
        void_ids_of_gals: np.array(dtype=int)
           A corresponding array that defines in which void the galaxy is located.
        void_id_from_revolver: list (type=int)
           The IDs of the voids (to easily connect the particles to the void; member_ids).
        OutputDir: string
           Optional specification of where the plots should be safed.

        returns
        ------------
        """

        calc=calculations()

        print('Void ID && Bary-Pot/R_{eff} && Den-Pot/R_{eff} && Bary-Den/R_{eff} && R_{eff} && N_{part} \\')
        for i in range(0,num_voids):
            where_in_full_data = []
            dm_in_gals_in_this_void = []
            members_x=tracers[member_ids[i],0]
            members_y=tracers[member_ids[i],1]
            members_z=tracers[member_ids[i],2]
            member_gadget_ids=gadget_ids[member_ids[i]]
            this_void_id = void_id_from_revolver[i]
            print("this_void_id=",this_void_id)
            this_void_id_in_gals=np.where(void_ids_of_gals == this_void_id)[0]
            print("this_void_id_in_gals=",this_void_id_in_gals)
            if(this_void_id_in_gals.size > 0):
                for gal_id in this_void_id_in_gals:
                    for gal in halo_particle_lists[gal_id]:
                        dm_in_gals_in_this_void.append(gal)
                print("dm_in_gals_in_this_void=",dm_in_gals_in_this_void)
                for part_in_void in dm_in_gals_in_this_void:
                    this_id = np.where(member_gadget_ids == part_in_void)[0]
                    print("this_id=",this_id)
                    if(this_id.size > 0):
                        where_in_full_data.append(this_id[0])
                print("where_in_full_data=",where_in_full_data)
            else:
                print("this galaxy is in no void")

            moved_pots=calc.move_coordinates(pot_centre[i,0], pot_centre[i,1], pot_centre[i,2], density_centre[i], box_length)
            moved_circ=calc.move_coordinates(circ_centre[i,0], circ_centre[i,1], circ_centre[i,2], density_centre[i], box_length)
            moved_barys=calc.move_coordinates(geom_centre[i,0], geom_centre[i,1], geom_centre[i,2], density_centre[i], box_length)
            moved_members=calc.move_coordinates(members_x, members_y, members_z, density_centre[i], box_length)

            fig = plt.figure()
            ax = fig.gca(projection='3d')
            #ax.set_aspect("equal")
            #ax.set_xlim(-2*eff_void_radius[i],2*eff_void_radius[i])
            #ax.set_ylim(-2*eff_void_radius[i],2*eff_void_radius[i])
            #ax.set_zlim(-2*eff_void_radius[i],2*eff_void_radius[i])

            ax.set_xlabel("x in Mpc/h")
            ax.set_ylabel("y in Mpc/h")
            ax.set_zlabel("z in Mpc/h")
            ax.scatter(moved_pots[0],moved_pots[1],moved_pots[2], marker='x',c='darkgreen',label='Potential Centre')
            ax.scatter(moved_circ[0],moved_circ[1],moved_circ[2], marker='x',c='cyan',label='Circumcentre')
            ax.scatter(moved_barys[0],moved_barys[1],moved_barys[2], marker='x',c='red',label='Vol-Averaged Barycentre')
            if(this_void_id_in_gals.size > 0):
                members_not_in_gal_x = np.delete(moved_members[:,0],where_in_full_data)
                members_not_in_gal_y = np.delete(moved_members[:,1],where_in_full_data)
                members_not_in_gal_z = np.delete(moved_members[:,2],where_in_full_data)
                ax.scatter(moved_members[where_in_full_data,0],moved_members[where_in_full_data,1],
                           moved_members[where_in_full_data,2], alpha=0.3, marker='v', c='red',label='DM-Particles in a galaxy')
                ax.scatter(members_not_in_gal_x,members_not_in_gal_y,members_not_in_gal_z,
                           alpha=0.05, marker='.', c='blue',label='DM-Particles')
            else:
                ax.scatter(moved_members[:,0],moved_members[:,1],moved_members[:,2], alpha=0.05, marker='.', c='blue',label='DM-Particles')
            ax.scatter(0,0,0, marker='x', c='violet',label='Origin - Lowest Density Centre') # the density centre is chosen to sit at 0,0,0 in our coordinate frame
            ax.legend(loc='upper right', bbox_to_anchor=(0.4, 1.1))

            if print_png_files:
                if(this_void_id_in_gals.size > 0):
                    plt.savefig(OutputDir + '/voidcentres_%d_gals.png' % i)
                else:
                    plt.savefig(OutputDir + '/voidcentres_%d.png' % i)
            plt.close()

        return 0

    # TODO: add the circumcentre
    def find_galaxies(self, G, geom_centre, pot_centre, density_centre, num_voids):
        """
        This function searches how many galaxies are close to the various centres by
        calculating the T^3 distance between galaxy and centre.

        Parameter
        ------------
        G: galaxy data
        pot_centre: np.array(3,dtype=float)
          The positions of the potential centres.
        circ_centre: np.array(3,dtype=float)
          The positions of the circumcentres.
        geom_centre: np.array(3,dtype=float)
          The positions of the geomcentres.
        num_voids: int
          The nuber of voids in the simulation.

        returns
        ------------
        out: list
        This list holds the indices of galaxies that are closer than a specified
        threshold to the chosen centre.
        """

        # We are interesed in every galaxy closer to a centre than 100kpc (or any other random distance)
        # First move all coordinated again to not falisfy results due to boundary conditions
        calc=calculations()
        out=[]

        for i in range(0,num_voids):
            moved_gals=calc.move_coordinates(G.Pos[:,0], G.Pos[:,1], G.Pos[:,2], density_centre[i], box_length)
            #moved_pots=calc.move_coordinates(pot_centre[i,0], pot_centre[i,1], pot_centre[i,2], density_centre,i)
            #moved_barys=calc.move_coordinates(geom_centre[i,0], geom_centre[i,1], geom_centre[i,2], density_centre, i)

            # next step is time intensive...
            # check for every galaxy if it is close to a given centre
            for k in range(0,len(moved_gals[:,0])):
                #dist_to_pot=np.sqrt((moved_gals[k,0]-moved_pots[0])*(moved_gals[k,0]-moved_pots[0]) + (moved_gals[k,1]-moved_pots[1])*(moved_gals[k,1]-moved_pots[1]) + (moved_gals[k,2]-moved_pots[2])*(moved_gals[k,2]-moved_pots[2]))
                #dist_to_bary=np.sqrt((moved_gals[k,0]-moved_barys[0])*(moved_gals[k,0]-moved_barys[0]) + (moved_gals[k,1]-moved_barys[1])*(moved_gals[k,1]-moved_barys[1]) + (moved_gals[k,2]-moved_barys[2])*(moved_gals[k,2]-moved_barys[2]))
                dist_to_den=np.sqrt((moved_gals[k,0]-0)*(moved_gals[k,0]-0) + (moved_gals[k,1]-0)*(moved_gals[k,1]-0) + (moved_gals[k,2]-0)*(moved_gals[k,2]-0))

                if dist_to_den < 0.5:
                    print(i, k)
                    out.append(k)

        print(out)
        return out

    def void_pot_crosssection(self, tracers, member_ids, pots, OutputDir):
        """
        This function plots a map of the potential for different slices
        with fixed z coordinate.

        Parameter
        ------------
        tracers: np.array(3,dtype=float)
        The positions of the DM particles.
        member_ids: list(type=int)
        The indices in the gadget file of the DM particles in the void.
        pots: np.array(dtype=float)
        The potentials at the positions of the DM particles.
        OutputDir: string
        Optional specification of where the plots should be saved.

        returns
        ------------
        """

        calc=calculations()

        # TODO: make the chosen void an input parameter.
        i=9
        members_x=tracers[member_ids[i],0]
        members_y=tracers[member_ids[i],1]
        members_z=tracers[member_ids[i],2]
        #member_pots=pots[member_ids[i]]

        moved_members=calc.move_coordinates(members_x, members_y, members_z, density_centre[i], box_length)
        moved_tracers=calc.move_coordinates(tracers[:,0], tracers[:,1], tracers[:,2], density_centre[i], box_length)

        NX = len(moved_members[:,0])
        xmin = min(moved_members[:,0])
        xmax = max(moved_members[:,0])
        dx = (xmax -xmin )/(NX-1.)

        NY = len(moved_members[:,1])
        ymin = min(moved_members[:,1])
        ymax = max(moved_members[:,1])
        dy = (ymax -ymin )/(NY-1.)

        #NZ = len(moved_members[:,2])
        #zmin = min(moved_members[:,2])
        #zmax = max(moved_members[:,2])
        #dz = (zmax -zmin )/(NZ-1)

        # fix one axis:

        y = np.array([ ymin + float(l)*dy for l in range(NY)])
        x = np.array([ xmin + float(l)*dx for l in range(NX)])
        #z = np.array([ zmin + float(i)*dz for i in range(NZ)])

        x, y = np.meshgrid( x, y, indexing = 'ij', sparse = False)

        linInter= LinearNDInterpolator(moved_tracers, pots)

        #label=np.empty([2,2],dtype='string')
        fig, axs = plt.subplots(2, 2, sharex='all',sharey='all')

        z=-eff_void_radius[i]
        ax=axs[0,0]
        ax.set_title('z=%.2f * $R_{eff}$' % (z/eff_void_radius[i]))
        g = linInter(x,y,z)
        c=ax.pcolormesh(x, y, g)
        #plt.colorbar(c,ax=ax)

        z=-0.1*eff_void_radius[i]
        ax=axs[1,0]
        ax.set_title('z=%.2f * $R_{eff}$' % (z/eff_void_radius[i]))
        g = linInter(x,y,z)
        c=ax.pcolormesh(x, y, g)
        #plt.colorbar(c,ax=ax)

        z=0.1*eff_void_radius[i]
        ax=axs[0,1]
        ax.set_title('z=%.2f * $R_{eff}$' % (z/eff_void_radius[i]))
        g = linInter(x,y,z)
        c=ax.pcolormesh(x, y, g)
        #plt.colorbar(c,ax=ax)

        z=1*eff_void_radius[i]
        ax=axs[1,1]
        ax.set_title('z=%.2f * $R_{eff}$' % (z/eff_void_radius[i]))
        g = linInter(x,y,z)
        c=ax.pcolormesh(x, y, g)
        plt.colorbar(c,ax=axs[:,:],location='right')

        figname = 'PotCross%i.png' % i
        if print_png_files:
            plt.savefig(OutputDir + figname, format = 'png')
        #plt.show()

    def plot_void_population(self, eff_void_radius):
        """
        This function creates a histogram visualising the occurance of different void sizes
        in the simulation. Using DM as tracers leads to much smaller voids, this histogram
        should give a feeling for the void sizes.

        Parameter
        ------------
        eff_void_radius: np.array(dtype=float)
        An array of the effective void radii.

        returns
        ------------
        """

        max_void_rad = max(eff_void_radius)
        Nvoids=len(eff_void_radius)

        with open("Nvoids.dat","w+") as F:
            F.write("Nvoids = %i\n" % Nvoids)

        bins = np.linspace(0,max_void_rad,20)
        ax=plt.figure()
        plt.rcParams.update({'font.size': 18,
                         'font.family': 'serif'})
        plt.ylabel('$N$')
        plt.xlabel('$R_{\mathrm{eff}}$ (Mpc/$h$)')
        plt.hist(eff_void_radius, bins=bins, linewidth=1,
                 edgecolor ='k', facecolor='none',
                 label='all voids')
        ylims = plt.ylim()
        plt.ylim((0.0, ylims[1]))
        plt.tight_layout()
        #plt.legend(loc='upper left', numpoints=1, labelspacing=0.1)
        plt.savefig("void_population.eps",metadata=metadata)
        plt.close()

    def mass_def(self, centre, tracers, pots, eff_void_radius, void_id, mean_den):
        """This function calculates the mass deficit within a void using spherical shells.
        It is supposed to help us making a quick check if the potentials we use are reasonable.

        Parameter
        ------------
        centre: np.array(3,dtype=float)
           Specify which centre should be used as the centre of the void (for the spherical shells).
        tracers: np.array(3,dtype=float)
           The positions of the DM particles.
        pots: np.array(dtype=float)
           The potentials at the positions of the DM particles.
        eff_void_radius: np.array(dtype=float)
          An array of the effective void radii.
        void_id: int
           The index of the treated void.
        mean_den: float
           The mean density of the simulation

        returns
        ------------
        mass_def: np.array(dtype=float)
           The mass deficit at within the volume for the different radii.
        den_contrast: np.array(dtype=float)
           The density contrast at within the volume for the different radii.
        density: np.array(dtype=float)
           The density in the void at the different radii.
        radii: np.array(dtype=float)
           The differnt radii to set the volume for the spherical shells.
        index: np.array(dtype=int)
           An array that holds all indices of DM particles that are within
           the maximal considered radius.  radius.
        """

        calc=calculations()
        moved_tracers=calc.move_coordinates(tracers[:,0], tracers[:,1], tracers[:,2], centre[void_id], box_length)
        moved_centre=calc.move_coordinates(centre[void_id,0], centre[void_id,1], centre[void_id,2], centre[void_id], box_length)

        # Split the full radius into N_radii-1 intervals, each of length dr
        radius_max = 2.0*eff_void_radius[void_id]
        # N_radii = number of endpoints of intervals
        # N_radii-1 = number of intervals
        N_radii = 20
        dr = 2*eff_void_radius[void_id]/float(N_radii-1) # interval
        ## Array of discretised radii up to twice the effective void radius
        radii = np.linspace(0.0,radius_max,N_radii)
        # flat-space volume
        volume = 4.0/3.0 * np.pi * np.power(radii,3)
        # square of distances of particles from the centre of the given void
        distance_sq = (np.power((moved_centre[0] - moved_tracers[:,0]),2) +
                       np.power((moved_centre[1] - moved_tracers[:,1]),2) +
                       np.power((moved_centre[2] - moved_tracers[:,2]),2))
        ## Use 1e-80 as a conservative minimum square distance
        distance = np.sqrt(np.maximum(distance_sq,1e-80))

        mass_def = np.zeros(1)
        den_contrast = np.array([-1.0])

        # Count the number of particles in each distance shell
        diff_number_count = (np.histogram(distance,N_radii-1,(0.0,radius_max)))[0]
        number_count_cum = (np.cumsum(diff_number_count)).astype(float)
        number_count = np.concatenate(([0.0], number_count_cum))
        density_excluding_centre = number_count[1:N_radii]/volume[1:N_radii]
        d0 = np.array(density_excluding_centre[0])
        # extrapolate 1-th density to 0-th position.
        print("density_excluding_centre, type(density_excluding_centre), density_excluding_centre.shape, density_excluding_centre.size")
        print(density_excluding_centre, type(density_excluding_centre), density_excluding_centre.shape, density_excluding_centre.size)
        density = np.concatenate((density_excluding_centre[[0]],density_excluding_centre))
        den_contrast = (density/mean_den) - 1.0

        # Wikipedia: https://en.wikipedia.org/wiki/Mass_deficit
        # using https://academic.oup.com/mnras/article/454/4/3357/997874 eq.13
        mass_def = volume*(density - mean_den)

        # Create a list of particle indices (corresponding to the first
        # index in tracers[:,.]) for the particles inside radius_max
        index = (np.where(distance/dr <= radius_max))[0]

        return mass_def, den_contrast, density, radii, index


    def order_of_magnitude(self, mass_def, rad, mass_dm, plotfile):
        """
        This function calculates the potential, velocity and acceleration a particle at given
        distance should feel under the assumption of a perfectly spherical void. It is supposed
        to help us making a quick check if the potentials we use are reasonable.

        Parameter
        ------------
        mass_def: np.array(dtype=float)
           The mass deficit at within the volume for the different radii.
        rad: np.array(dtype=float)
           The differnt radii to set the volume for the spherical shells.
        mass_dm: float:
           The mass of one DM particle.
        plotfile: string
           The filename in which the plot will be saved.

        returns
        ------------
        Phi: np.array(dtype=float)
           The theoretical potential derived from the toy model.
        a: np.array(dtype=float)
           The theoretical acceleration derived from the toy model.
        v: np.array(dtype=float)
           The theoretical velocity derived from the toy model.
        """

        Phi = np.empty(len(mass_def))
        a = np.empty(len(mass_def))
        v = np.empty(len(mass_def))
        G = 4.5e4

        for l in range(0,len(mass_def)):
            if(l == 0):
                Phi[l] = 0
                a[l] = 0
                v[l] = 0
            else:
                Phi[l] = -G * (mass_def[l]*mass_dm/1.e10) / (rad[l]*1000)  * 0.97779*0.97779
                a[l] = Phi[l] / (rad[l]*1000)
                v[l] = 10 * a[l]

        plt.xlabel("r in Mpc")
        #plt.ylabel("v in km/s")
        plt.ylabel("Phi in (km/s)^2")
        #plt.xscale("log")
        plt.yscale("log")
        plt.plot(rad, Phi, label='model')
        plt.savefig(plotfile)
        plt.close()

        return Phi, a, v

    def Phi_fraction(self, pots, centre, tracers, Phi, rad, index, void_id, plotfile):
        """
        This function sets the calculated potential from the spherical toy model
        into relation to the potential we read in from the simulation. It is supposed
        to help us making a quick check if the potentials we use are reasonable.

        Parameter
        ------------
        pots: np.array(dtype=float)
           The potentials at the positions of the DM particles.
        centre: np.array(3,dtype=float)
           Specify which centre should be used as the centre of the void (for the spherical shells).
        tracers: np.array(3,dtype=float)
           The positions of the DM particles.
        Phi: np.array(dtype=float)
           The theoretical potential derived from the toy model.
        rad: np.array(dtype=float)
           The different radii to set the volume for the spherical shells.
        index: np.array(dtype=int)
           An array that holds all indices of DM particles that are within the maximal considered radius.
        void_id: int
           The index of the treated void.
        plotfile: string
           The filename in which the plot will be saved.

        returns
        ------------
        """

        calc=calculations()
        moved_tracers=calc.move_coordinates(tracers[:,0], tracers[:,1], tracers[:,2], centre[void_id], box_length)
        moved_centre=calc.move_coordinates(centre[void_id,0], centre[void_id,1], centre[void_id,2], centre[void_id], box_length)
        fraction = []
        radius_part = []

        # see https://docs.scipy.org/doc/scipy/reference/tutorial/interpolate.html
        Phi_interp = interp1d(rad,Phi,kind='cubic')

        for l in index:
            radius = np.sqrt(((moved_centre[0]-moved_tracers[l,0])*(moved_centre[0]-moved_tracers[l,0]))+((moved_centre[1]-moved_tracers[l,1])*(moved_centre[1]-moved_tracers[l,1]))+((moved_centre[2]-moved_tracers[l,2])*(moved_centre[2]-moved_tracers[l,2])))
            fraction.append(pots[l]/Phi_interp(radius))
            radius_part.append(radius)
        #fraction = Phi_interp(radius_part)

        plt.xlabel("r in Mpc")
        plt.ylabel("Phi(simulation)/Phi(model)")
        #plt.xscale("log")
        #plt.yscale("log")
        plt.scatter(radius_part, fraction)
        len_fraction = len(fraction)
        print('Phi_fraction:',void_id,'len(index)=',len(index),'len_fraction=',len_fraction)
        if(print_potentials):
            local_index = np.arange(0,len_fraction-1,1)
            for i in local_index:
                print('fractions: ',void_id,radius_part[i],fraction[i])

        plt.savefig(plotfile)
        plt.close()

        print('mean of phi_simulation/phi_model: %f' % np.mean(fraction))

    def calc_acc(self, void_id, centre, tracers, pots, member_ids, eff_void_radius):
        """
        This function calculates the elaphro-acceleration for a
        hypothetical galaxy placed exactly at the elaphrocentre. It
        calculates the acceleration at several positions around the galaxy
        and estimates the total "netto" acceleration between two points. To
        estimate the acceleration we use scipy's LinearNDInterpolator and
        numpy's gradient.

        Parameter
        ------------
        void_id: int
           The index of the treated void.
        centre: np.array(3,dtype=float)
           Specify which centre should be used as the centre of the void
           (for the spherical shells).
        tracers: np.array(3,dtype=float)
           The positions of the DM particles.
        pots: np.array(dtype=float)
           The potentials at the positions of the DM particles.
        member_ids: list(type=int)
           The indices in the gadget file of the DM particles in the void.
        eff_void_radius: np.array(dtype=float)
           An array of the effective void radii.

        returns
        ------------
        acc_x, acc_neg_x, acc_y, acc_neg_y, acc_z, acc_neg_z: each is np.array(3,dtype=float)
           The acceleration components cartesian coordinate directions;
        acc_rad, acc_tan: dtype=float
           Mean radial and tangential components
        """

        # TODO: make it more flexible for all kinds of centres, not only the density centre
        #elapho_gals_pos=G.Pos[elapho_gal_ids]
        #elapho_gals_radius=G.Rvir[elapho_gal_ids]

        calc=calculations()


        members_x=tracers[member_ids[void_id],0]
        members_y=tracers[member_ids[void_id],1]
        members_z=tracers[member_ids[void_id],2]
        member_pots=pots[member_ids[void_id]]

        moved_members=calc.move_coordinates(members_x, members_y, members_z, density_centre[void_id], box_length)
        #moved_tracers=calc.move_coordinates(tracers[:,0], tracers[:,1], tracers[:,2], density_centre, void_id)
        moved_centre=calc.move_coordinates(centre[void_id,0], centre[void_id,1], centre[void_id,2], density_centre[void_id], box_length)

        # radius of original region from which the Milky Way may have formed
        radius=elaphrocentre_halo_original_radius_Mpc_over_h
        print("radius = elaphrocentre_halo_original_radius_Mpc_over_h = ",radius," Mpc/h")


        #play a bit with np.gradient to understand how it is working
        #x=np.linspace(0,10,11)
        #y=np.linspace(0,10,11)
        #grid_x = grid_y = np.meshgrid( x, y, indexing = 'ij', sparse = False)
        #print grid_x

        #print f
        #force=np.gradient(f,edge_order=2)
        #print("time before", time.clock())
        linInter= LinearNDInterpolator(moved_members, member_pots)
        #print("time after", time.clock())

        acc_x=calc.gradient([moved_centre[0]+radius,moved_centre[1],moved_centre[2]], linInter, eff_void_radius[void_id])
        acc_neg_x=calc.gradient([moved_centre[0]-radius,moved_centre[1],moved_centre[2]], linInter, eff_void_radius[void_id])
        #print("time after force_x", time.clock())

        acc_y=calc.gradient([moved_centre[0],moved_centre[1]+radius,moved_centre[2]], linInter, eff_void_radius[void_id])
        acc_neg_y=calc.gradient([moved_centre[0],moved_centre[1]-radius,moved_centre[2]], linInter, eff_void_radius[void_id])
        #print("time after force_y", time.clock())

        acc_z=calc.gradient([moved_centre[0],moved_centre[1],moved_centre[2]+radius], linInter, eff_void_radius[void_id])
        acc_neg_z=calc.gradient([moved_centre[0],moved_centre[1],moved_centre[2]-radius], linInter, eff_void_radius[void_id])
        #print("time after force_z", time.clock())

        # Check if any point is outside the void, if yes remove the whole void from further analysis.
        # Manually set the gradient to NaNs, they will be excluded later on.
        if(any(np.isnan(acc) for acc in [acc_x[0],acc_y[0],acc_z[0],acc_neg_x[0],acc_neg_y[0],acc_neg_z[0],
                                         acc_x[1],acc_y[1],acc_z[1],acc_neg_x[1],acc_neg_y[1],acc_neg_z[1],
                                         acc_x[2],acc_y[2],acc_z[2],acc_neg_x[2],acc_neg_y[2],acc_neg_z[2]]) ):
            acc_x = np.array([float('NaN'), float('NaN'), float('NaN')])
            acc_neg_x = np.array([float('NaN'), float('NaN'), float('NaN')])
            acc_y = np.array([float('NaN'), float('NaN'), float('NaN')])
            acc_neg_y = np.array([float('NaN'), float('NaN'), float('NaN')])
            acc_z = np.array([float('NaN'), float('NaN'), float('NaN')])
            acc_neg_z = np.array([float('NaN'), float('NaN'), float('NaN')])

        print("############## accelerations ##############")
        print("a(x) a(-x) diff F(x)    a(y) a(-y) diff a(y)    a(z) a(-z) diff a(z)")
        print("%.4e %.4e %.4e %.4e %.4e %.4e %.4e %.4e %.4e" % (acc_x[0], acc_neg_x[0], (acc_x[0]-acc_neg_x[0]), acc_y[0], acc_neg_y[0], (acc_y[0]-acc_neg_y[0]), acc_z[0], acc_neg_z[0], (acc_z[0]-acc_neg_z[0])))
        print("%.4e %.4e %.4e %.4e %.4e %.4e %.4e %.4e %.4e" % (acc_x[1], acc_neg_x[1], (acc_x[1]-acc_neg_x[1]), acc_y[1], acc_neg_y[1], (acc_y[1]-acc_neg_y[1]), acc_z[1], acc_neg_z[1], (acc_z[1]-acc_neg_z[1])))
        print("%.4e %.4e %.4e %.4e %.4e %.4e %.4e %.4e %.4e" % (acc_x[2], acc_neg_x[2], (acc_x[2]-acc_neg_x[2]), acc_y[2], acc_neg_y[2], (acc_y[2]-acc_neg_y[2]), acc_z[2], acc_neg_z[2], (acc_z[2]-acc_neg_z[2])))

        # Calculate radial and tangential components; the radial component
        # may be negative, zero or positive; the tangential component is
        # non-negative.

        # radial component
        acc_rad = (acc_x[0] - acc_neg_x[0] +
                   acc_y[1] - acc_neg_y[1] +
                   acc_z[2] - acc_neg_z[2]) / 6.0
        # tangential component
        tol_acc_tan = 1e-100 # tolerance to avoid imaginary square root
        acc_tan = (np.sqrt(np.maximum(tol_acc_tan,
                                     (acc_x[1]**2 + acc_x[2]**2))) +
                   np.sqrt(np.maximum(tol_acc_tan,
                                     (acc_neg_x[1]**2 + acc_neg_x[2]**2))) +
                   np.sqrt(np.maximum(tol_acc_tan,
                                     (acc_y[0]**2 + acc_y[2]**2))) +
                   np.sqrt(np.maximum(tol_acc_tan,
                                     (acc_neg_y[0]**2 + acc_neg_y[2]**2))) +
                   np.sqrt(np.maximum(tol_acc_tan,
                                     (acc_z[0]**2 + acc_z[1]**2))) +
                   np.sqrt(np.maximum(tol_acc_tan,
                                     (acc_neg_z[0]**2 + acc_neg_z[1]**2)))
                   )/ 6.0

        return acc_x, acc_neg_x, acc_y, acc_neg_y, acc_z, acc_neg_z, acc_rad, acc_tan


    def corrrel(self,num_voids, pots, member_ids, eff_void_radius, void_file):
        """
        This function is supposed to be a quick check that our voids have a reasonable behaviour.
        We plot the potential at the centre (default is what we call the density centre, the
        ZOBOV output) against the size of the galaxy, it's effective radius. We can compare our
        results with https://arxiv.org/pdf/1610.08382.pdf.

        Parameter
        ------------
        num_voids: int
        The nuber of voids in the simulation.
        pots: np.array(dtype=float)
        The potentials at the positions of the DM particles.
        member_ids: list(type=int)
        The indices in the gadget file of the DM particles in the void.
        eff_void_radius: np.array(dtype=float)
        An array of the effective void radii.
        void_file: string
        Specifies void-data, which centre, should be used.

        returns
        ------------
        """
        # see https://arxiv.org/pdf/1610.08382.pdf

        #V=4*3.1415926*(eff_void_radius[void_id]*eff_void_radius[void_id]*eff_void_radius[void_id])/3
        #average_number_density=128*128*128/(40*40*40)
        #average_tracer_den=2526546176.00*2.e30
        voiddata = np.loadtxt(void_file, skiprows=2)

        eff_void_rad_min = min(eff_void_radius) *1.05
        eff_void_rad_max = max(eff_void_radius) *0.95
        delta_eff_void_rad = (eff_void_rad_max-eff_void_rad_min)/4.0 *0.98

        x = np.arange(eff_void_rad_min, eff_void_rad_max, delta_eff_void_rad)
        print("x=", x)
        y=[]
        corr=np.empty((num_voids,2))
        for i in range(0,num_voids):
            corr[i,0]=eff_void_radius[i]
            corr[i,1]=pots[np.int(voiddata[i,1])]

        print("num_voids=%d" %num_voids)
        for el in x:
            count=0
            pre_pot=0
            for i in range(0,num_voids):
                if corr[i,0] > (el-delta_eff_void_rad*0.5) and corr[i,0] < (el+delta_eff_void_rad*0.5):
                    count=count+1
                    pre_pot=pre_pot+corr[i,1]
            print("count=%d" %count)
            avg_pot=pre_pot/count
            y.append(avg_pot)
        plt.figure()
        plt.scatter(x,y)
        outputFile = 'corr.png'
        if print_png_files:
            plt.savefig(outputFile)
        plt.close()


    def plot_acc_rad(self, eff_rad_array, acc):

        N_acc = acc.size
        if(N_acc > 200):
            N_max_sample = np.int(N_acc*np.sqrt(N_acc))+1 # avoid N^2
        pslope, pzero = theil_sen_try_fast(eff_rad_array, acc, N_max_sample = N_max_sample)
        sig_pslope, sig_pzero = theil_sen_robust_stderr(eff_rad_array, acc_x_pot[:,0], N_max_sample = N_max_sample)
        # Plot
        plt.clf()
        plt.figure()
        plt.rcParams.update({'font.size': 18,
                             'font.family': 'serif'})

        plt.plot(eff_rad_array, acc,
                 'o', linewidth=1, markersize=1)
        #plt.plot(eff_rad_array, acc_x_pot[:,0], '-', linewidth=2)

        #plt.xlim(xlims)
        #plt.ylim(ylims_amp)
        plt.xlabel(r'effective void radius $r_{eff}$')
        plt.ylabel(r'radial acceleration (km/s/Gyr)')

        plt.tight_layout()
        plt.savefig('radial_acc.eps',metadata=metadata)
        plt.close()


#  'Main' section of code.  This if statement executes if the code is run from the
#   shell command line, i.e. with 'python allresults.py'

if __name__ == '__main__':

    from optparse import OptionParser
    import os
    import cProfile

    box_length=float(sys.argv[1])
    print(box_length)

    # could also be read from the gadget file
    mass_dm=float(sys.argv[2])
    print(mass_dm)

    parser = OptionParser()
    parser.add_option(
        '-d',
        '--revolver_dir_name',
        dest='RevDirName',
        default='./',
        help='revolver input directory name',
        metavar='RDIR',
        )
    parser.add_option(
        '-s',
        '--sage_dir_name',
        dest='SageDirName',
        default='./',
        help='sage input directory name',
        metavar='SDIR',
        )
    parser.add_option(
        '-f',
        '--file_base',
        dest='FileName',
        default='model_z-0.000',
        help='filename base (default: model_z0.000)',
        metavar='FILE',
        )
    parser.add_option(
        '-t',
        '--tracer_file_base',
        dest='TracerFile',
        default='./out_gadget',
        help='path and filename of tracer file',
        metavar='FILE',
        )
    parser.add_option(
        '-n',
        '--file_range',
        type='int',
        nargs=2,
        dest='FileRange',
        default=(0, 7),
        help='first and last filenumbers (default: 0 7)',
        metavar='FIRST LAST',
        )
    parser.add_option(
        '-o',
        '--output_dir_name',
        dest='OutputDirName',
        default='./voids_plots/',
        help='output directory name',
        metavar='ODIR',
        )
    parser.add_option(
        '-p',
        '--print_png',
        dest='PrintPNGFiles',
        default=False,
        help='print png files',
        )
    parser.add_option(
        '-m',
        '--milky-way-orig-radius',
        dest='elaphrocentre_halo_original_radius_Mpc_over_h',
        default='1.0',
        help='print png files',
        )

    (opt, args) = parser.parse_args()

    if opt.SageDirName[-1] != '/':
        opt.SageDirName += '/'

    #OutputDir = opt.DirName + 'plots/'
    OutputDir = opt.OutputDirName

    if not os.path.exists(OutputDir):
        os.makedirs(OutputDir)

    res = Results()

    # Set up paths
    tracer_file = opt.TracerFile
    geomcentre_file = opt.RevDirName + '/outputbarycentres/zobov-Voids_baryC_cat.txt'
    potcentre_file_old = opt.RevDirName + '/outputpotcentres/zobov-Voids_potC_cat.txt'
    potcentre_file = opt.RevDirName + '/outputpotcentres/zobov-Voids_cat.txt'
    void_file = opt.RevDirName + '/outputzobov-Voids_list.txt'
    print('voidfile =', void_file)
    void_detail_file = opt.RevDirName + '/outputzobov-Voids_cat.txt'
    particle_id_file = opt.RevDirName + '/Revolver_Ids.txt'

    if opt.PrintPNGFiles:
        print_png_files = True
    else:
        print_png_files = False

    print("print_png_files = ",print_png_files)
    print("type(print_png_files) = ",type(print_png_files))

    if os.environ.get('METADATA_CREATOR'):
        metadata_creator = os.environ.get('METADATA_CREATOR')
    else:
        metadata_creator = 'elaphrocentre'
    metadata = {"Creator": metadata_creator}



    FirstFile = opt.FileRange[0]
    LastFile = opt.FileRange[1]

    fin_base = opt.SageDirName + opt.FileName

    elaphrocentre_halo_original_radius_Mpc_over_h = (
        float(opt.elaphrocentre_halo_original_radius_Mpc_over_h ))

    print('Will read galaxies data...')
    G = res.read_gals(fin_base, FirstFile, LastFile)
    print('Will read DM particle data...')
    tracers=res.read_tracers(tracer_file)
    gadget_ids=res.read_ids(tracer_file)
    pots=res.read_pots(tracer_file)
    halo_particle_lists=res.read_particle_ids_of_gals_in_voids()
    gal_ids, void_ids_of_gals=res.read_gal_in_void_ids()
    #print interpn(tracers[:,0], tracers[:,1], tracers[:,2], pots, 'linear')
    print('Will read void data...')
    density_centre=res.read_void_data(void_file)
    num_voids=len(density_centre[:,0])
    geom_centre=res.read_geom_data(geomcentre_file)
    eff_void_radius=res.read_void_radius(geomcentre_file)
    circ_centre=res.read_circ_centre(void_detail_file)
    pot_centre=res.read_pot_centre(potcentre_file, num_voids)
    void_id_from_revolver, member_ids=res.read_void_particles(particle_id_file)
    mean_den = len(tracers)/(box_length * box_length * box_length)
    print('mean_dem', mean_den)
    print('find elaphro-galaxies')
    #elapho_gal_ids=res.find_galaxies(G, geom_centre, pot_centre, density_centre, num_voids)
    print('calculate force on the edges of a elaphro-galaxy')

        # Set up variables
    # how many voids do we want to analyse at the moment?
    x = os.environ.get('NVOID_ANALYSE')
    if (x):
        n = np.int(x)
        if( n > num_voids):
            Nvoid_analyse = num_voids
        else:
            Nvoid_analyse = n
    else:
        Nvoid_analyse=num_voids

    acc_x_dens = np.empty((Nvoid_analyse,3))
    acc_y_dens = np.empty((Nvoid_analyse,3))
    acc_z_dens = np.empty((Nvoid_analyse,3))
    acc_neg_x_dens = np.empty((Nvoid_analyse,3))
    acc_neg_y_dens = np.empty((Nvoid_analyse,3))
    acc_neg_z_dens = np.empty((Nvoid_analyse,3))

    acc_x_geom = np.empty((Nvoid_analyse,3))
    acc_y_geom = np.empty((Nvoid_analyse,3))
    acc_z_geom = np.empty((Nvoid_analyse,3))
    acc_neg_x_geom = np.empty((Nvoid_analyse,3))
    acc_neg_y_geom = np.empty((Nvoid_analyse,3))
    acc_neg_z_geom = np.empty((Nvoid_analyse,3))

    acc_x_pot = np.empty((Nvoid_analyse,3))
    acc_y_pot = np.empty((Nvoid_analyse,3))
    acc_z_pot = np.empty((Nvoid_analyse,3))
    acc_neg_x_pot = np.empty((Nvoid_analyse,3))
    acc_neg_y_pot = np.empty((Nvoid_analyse,3))
    acc_neg_z_pot = np.empty((Nvoid_analyse,3))

    acc_x_circ = np.empty((Nvoid_analyse,3))
    acc_y_circ = np.empty((Nvoid_analyse,3))
    acc_z_circ = np.empty((Nvoid_analyse,3))
    acc_neg_x_circ = np.empty((Nvoid_analyse,3))
    acc_neg_y_circ = np.empty((Nvoid_analyse,3))
    acc_neg_z_circ = np.empty((Nvoid_analyse,3))

    acc_rad_pot = np.empty(Nvoid_analyse)
    acc_tan_pot = np.empty(Nvoid_analyse)

    eff_rad_array = np.empty(Nvoid_analyse)

    profile_object = cProfile.Profile()
    profile_object.enable()

    counted_voids=0
    for i_void in range(0,Nvoid_analyse):
        if(eff_void_radius[i_void] > elaphrocentre_halo_original_radius_Mpc_over_h):
            counted_voids += 1
            print('')
            print('calculate force for void = %i' %i_void)

            if(enable_density_centre):
                print('Force acting around the density centre')
                acc_x_dens[i_void], acc_neg_x_dens[i_void], acc_y_dens[i_void], acc_neg_y_dens[i_void], acc_z_dens[i_void], acc_neg_z_dens[i_void] , acc_rad_dens[i_void], acc_tan_dens[i_void] = res.calc_acc(i_void, density_centre, tracers, pots, member_ids, eff_void_radius)
                mass_def, den_contrast, density, rad, index = res.mass_def(density_centre, tracers, pots, eff_void_radius, i_void, mean_den)

                if debug_phase:
                    mass_def_old, den_contrast_old, density_old, rad_old, index_old = res.mass_def_deprecated(density_centre, tracers, pots, eff_void_radius, i_void, mean_den)
                    print("Check mass_def: new:",mass_def,"\n", den_contrast, "\n", density, "\n",rad)
                    print("Check mass_def: old:",mass_def_old,"\n", den_contrast_old, "\n",density_old,"\n",rad_old)
                    n1=len(index)
                    print("Check mass_def: new:",n1,type(index),index.size)
                    print("Check mass_def: new:",index[[0,1,n1-2,n1-1]])
                    n2=len(index)
                    print("Check mass_def: old:",n2,index[[0,1,n2-2,n2-1]])

                print('')
                print('calculate mass deficit')
                mass_def_file = 'mass_def_density_void_%i.dat' % i_void
                mass_def_plot_file = 'mass_def_density_void_%i.png' % i_void
                Phi, a, v = res.order_of_magnitude(mass_def, rad, mass_dm, mass_def_plot_file)
                with open(mass_def_file,'w') as F:
                    F.write("# mass deficit [m_dm]  density contrast  density [m_dm/Mpc^3]  radius [Mpc]\n")
                    for l in range(0,len(mass_def)):
                        F.write("%.4f %.4f %.4f %.4f\n" % (mass_def[l], den_contrast[l], density[l], rad[l]))
                F.close()
                print('')

            if(enable_geom_centre):
                print('Force acting around the geomcentre (vol-weighted barycentre)')
                acc_x_geom[i_void], acc_neg_x_geom[i_void], acc_y_geom[i_void], acc_neg_y_geom[i_void], acc_z_geom[i_void], acc_neg_z_geom[i_void] , acc_rad_geom[i_void], acc_tan_geom[i_void] = res.calc_acc(i_void, geom_centre, tracers, pots, member_ids, eff_void_radius)
                mass_def, den_contrast, density, rad, index = res.mass_def(geom_centre, tracers, pots, eff_void_radius, i_void, mean_den)
                print('')
                print('calculate mass deficit')
                mass_def_file = 'mass_def_geom_void_%i.dat' % i_void
                mass_def_plot_file = 'mass_def_geom_void_%i.png' % i_void
                Phi, a, v = res.order_of_magnitude(mass_def, rad, mass_dm, mass_def_plot_file)
                with open(mass_def_file,'w') as F:
                    F.write("# mass deficit [m_dm]  density contrast  density [m_dm/Mpc^3]  radius [Mpc]\n")
                    for l in range(0,len(mass_def)):
                        F.write("%.4f %.4f %.4f %.4f\n" % (mass_def[l], den_contrast[l], density[l], rad[l]))
                F.close()
                print('')

            if(enable_pot_centre):
                print('Force acting around the potential centre')
                acc_x_pot[i_void], acc_neg_x_pot[i_void], acc_y_pot[i_void], acc_neg_y_pot[i_void], acc_z_pot[i_void], acc_neg_z_pot[i_void] , acc_rad_pot[i_void], acc_tan_pot[i_void] = res.calc_acc(i_void, pot_centre, tracers, pots, member_ids, eff_void_radius)

                eff_rad_array[i_void] = eff_void_radius[i_void]
                print('Will call res.mass_def...')

                mass_def, den_contrast, density, rad, index = res.mass_def(pot_centre, tracers, pots, eff_void_radius, i_void, mean_den)
                print('')
                print('calculate mass deficit')
                mass_def_file = 'mass_def_pot_void_%i.dat' % i_void
                mass_def_plot_file = 'mass_def_pot_void_%i.png' % i_void
                Phi, a, v = res.order_of_magnitude(mass_def, rad, mass_dm, mass_def_plot_file)
                with open(mass_def_file,'w') as F:
                    F.write("# mass deficit [m_dm]  density contrast  density [m_dm/Mpc^3]  radius [Mpc]\n")
                    for l in range(0,len(mass_def)):
                        F.write("%.4f %.4f %.4f %.4f\n" % (mass_def[l], den_contrast[l], density[l], rad[l]))
                F.close()
                print('')

            if(enable_circum_centre):
                print('Force acting around the circumcentre')
                acc_x_circ[i_void], acc_neg_x_circ[i_void], acc_y_circ[i_void], acc_neg_y_circ[i_void], acc_z_circ[i_void], acc_neg_z_circ[i_void] , acc_rad_circ[i_void], acc_tan_circ[i_void] = res.calc_acc(i_void, circ_centre, tracers, pots, member_ids, eff_void_radius)
                mass_def, den_contrast, density, rad, index = res.mass_def(circ_centre, tracers, pots, eff_void_radius, i_void, mean_den)
                print('')
                print('calculate mass deficit')
                mass_def_file = 'mass_def_circ_void_%i.dat' % i_void
                mass_def_plot_file = 'mass_def_circvoid_%i.png' % i_void
                Phi, a, v = res.order_of_magnitude(mass_def, rad, mass_dm, mass_def_plot_file)
                with open(mass_def_file,'w') as F:
                    F.write("# mass deficit [m_dm]  density contrast  density [m_dm/Mpc^3]  radius [Mpc]\n")
                    for l in range(0,len(mass_def)):
                        F.write("%.4f %.4f %.4f %.4f\n" % (mass_def[l], den_contrast[l], density[l], rad[l]))
                F.close()

            plotfile = 'potential_fraction_%i.png' % i_void
            if(print_potentials):
                print('eff_void_radius[i_void] = ',eff_void_radius[i_void])
            #res.Phi_fraction(pots, circ_centre, tracers, Phi, rad, index, i_void, plotfile)
            print('')

    # NANs are assumed to occur when the fixed test radius lies outside of
    # of the void, where interpolation from void particles doesn't work,
    # because it is extrapolation rather than interpolation. Extrapolation
    # would be noisy, so is better ignored.
    nan_indices = np.array(np.where(np.isnan(acc_x_pot[:,0])))[0]
    how_many_bad_voids = nan_indices.size
    how_many_good_voids = acc_x_pot[:,0].size - how_many_bad_voids
    acc_non_nan_x = np.delete(acc_x_pot,nan_indices,0)
    acc_non_nan_y = np.delete(acc_y_pot,nan_indices,0)
    acc_non_nan_z = np.delete(acc_z_pot,nan_indices,0)
    acc_non_nan_neg_x = np.delete(acc_neg_x_pot,nan_indices,0)
    acc_non_nan_neg_y = np.delete(acc_neg_y_pot,nan_indices,0)
    acc_non_nan_neg_z = np.delete(acc_neg_z_pot,nan_indices,0)

    acc_rad_non_nan = np.delete(acc_rad_pot,nan_indices)
    acc_tan_non_nan = np.delete(acc_tan_pot,nan_indices)
    eff_rad_array_non_nan = np.delete(eff_rad_array,nan_indices)

    voidgals_acc_plot(how_many_good_voids, eff_rad_array_non_nan,
                      acc_rad_non_nan*Julian_Gyr_in_seconds,
                      acc_tan_non_nan*Julian_Gyr_in_seconds)

    with open("elaphro-acc.dat","w+") as F:
        print('print the average value for the difference of the force at the edge of a sample galaxy and its standard deviation for the first %i voids' % Nvoid_analyse)
        print('From these voids %i are large enough' % counted_voids)
        F.write("Nvoid=%i\n" % Nvoid_analyse)

        if(enable_density_centre):
            print('density centre:')
            avg_x = np.nanmean(acc_x_dens,axis=0)*Julian_Gyr_in_seconds
            avg_y = np.nanmean(acc_y_dens,axis=0)*Julian_Gyr_in_seconds
            avg_z = np.nanmean(acc_z_dens,axis=0)*Julian_Gyr_in_seconds
            error_x = np.nanstd(acc_x_dens,axis=0)*Julian_Gyr_in_seconds
            error_y = np.nanstd(acc_y_dens,axis=0)*Julian_Gyr_in_seconds
            error_z = np.nanstd(acc_z_dens,axis=0)*Julian_Gyr_in_seconds
            F.write(" avg_x_denxx=%e\n avg_x_denyy=%e\n avg_x_denzz=%e\n avg_y_denxx=%e\n avg_y_denyy=%e\n avg_y_denzz=%e\n avg_z_denxx=%e\n avg_z_denyy=%e\n avg_z_denzz=%e\n error_x_denxx=%e\n error_x_denyy=%e\n error_x_denzz=%e\n error_y_denxx=%e\n error_y_denyy=%e\n error_y_denzz=%e\n error_z_denxx=%e\n error_z_denyy=%e\n error_z_denzz=%e\n "% (avg_x[0], avg_x[1], avg_x[2], avg_y[0], avg_y[1], avg_y[2], avg_z[0], avg_z[1], avg_z[2], error_x[0], error_x[1], error_x[2], error_y[0], error_y[1], error_y[2], error_z[0], error_z[1], error_z[2],))
            avg_neg_x = np.nanmean(acc_neg_x_dens,axis=0)*Julian_Gyr_in_seconds
            avg_neg_y = np.nanmean(acc_neg_y_dens,axis=0)*Julian_Gyr_in_seconds
            avg_neg_z = np.nanmean(acc_neg_z_dens,axis=0)*Julian_Gyr_in_seconds
            error_neg_x = np.nanstd(acc_neg_x_dens,axis=0)*Julian_Gyr_in_seconds
            error_neg_y = np.nanstd(acc_neg_y_dens,axis=0)*Julian_Gyr_in_seconds
            error_neg_z = np.nanstd(acc_neg_z_dens,axis=0)*Julian_Gyr_in_seconds
            F.write(" avg_neg_x_denxx=%e\n avg_neg_x_denyy=%e\n avg_neg_x_denzz=%e\n avg_neg_y_denxx=%e\n avg_neg_y_denyy=%e\n avg_neg_y_denzz=%e\n avg_neg_z_denxx=%e\n avg_neg_z_denyy=%e\n avg_neg_z_denzz=%e\n error_neg_x_denxx=%e\n error_neg_x_denyy=%e\n error_neg_x_denzz=%e\n error_neg_y_denxx=%e\n error_neg_y_denyy=%e\n error_neg_y_denzz=%e\n error_neg_z_denxx=%e\n error_neg_z_denyy=%e\n error_neg_z_denzz=%e\n "% (avg_neg_x[0], avg_neg_x[1], avg_neg_x[2], avg_neg_y[0], avg_neg_y[1], avg_neg_y[2], avg_neg_z[0], avg_neg_z[1], avg_neg_z[2], error_neg_x[0], error_neg_x[1], error_neg_x[2], error_neg_y[0], error_neg_y[1], error_neg_y[2], error_neg_z[0], error_neg_z[1], error_neg_z[2],))
            how_many_bad_voids = (np.where(np.isnan(acc_x_dens[:,0]))[0]).size
            how_many_good_voids = acc_x_dens[:,0].size - how_many_bad_voids
            F.write("CountedVoids_dens=%i\n" % how_many_good_voids)

        if(enable_geom_centre):
            print('geometric centre:')
            avg_x = np.nanmean(acc_x_geom,axis=0)*Julian_Gyr_in_seconds
            avg_y = np.nanmean(acc_y_geom,axis=0)*Julian_Gyr_in_seconds
            avg_z = np.nanmean(acc_z_geom,axis=0)*Julian_Gyr_in_seconds
            error_x = np.nanstd(acc_x_geom,axis=0)*Julian_Gyr_in_seconds
            error_y = np.nanstd(acc_y_geom,axis=0)*Julian_Gyr_in_seconds
            error_z = np.nanstd(acc_z_geom,axis=0)*Julian_Gyr_in_seconds
            F.write(" avg_x_geomxx=%e\n avg_x_geomyy=%e\n avg_x_geomzz=%e\n avg_y_geomxx=%e\n avg_y_geomyy=%e\n avg_y_geomzz=%e\n avg_z_geomxx=%e\n avg_z_geomyy=%e\n avg_z_geomzz=%e\n error_x_geomxx=%e\n error_x_geomyy=%e\n error_x_geomzz=%e\n error_y_geomxx=%e\n error_y_geomyy=%e\n error_y_geomzz=%e\n error_z_geomxx=%e\n error_z_geomyy=%e\n error_z_geomzz=%e\n "% (avg_x[0], avg_x[1], avg_x[2], avg_y[0], avg_y[1], avg_y[2], avg_z[0], avg_z[1], avg_z[2], error_x[0], error_x[1], error_x[2], error_y[0], error_y[1], error_y[2], error_z[0], error_z[1], error_z[2],))
            avg_neg_x = np.nanmean(acc_neg_x_geom,axis=0)*Julian_Gyr_in_seconds
            avg_neg_y = np.nanmean(acc_neg_y_geom,axis=0)*Julian_Gyr_in_seconds
            avg_neg_z = np.nanmean(acc_neg_z_geom,axis=0)*Julian_Gyr_in_seconds
            error_neg_x = np.nanstd(acc_neg_x_geom,axis=0)*Julian_Gyr_in_seconds
            error_neg_y = np.nanstd(acc_neg_y_geom,axis=0)*Julian_Gyr_in_seconds
            error_neg_z = np.nanstd(acc_neg_z_geom,axis=0)*Julian_Gyr_in_seconds
            F.write(" avg_neg_x_geomxx=%e\n avg_neg_x_geomyy=%e\n avg_neg_x_geomzz=%e\n avg_neg_y_geomxx=%e\n avg_neg_y_geomyy=%e\n avg_neg_y_geomzz=%e\n avg_neg_z_geomxx=%e\n avg_neg_z_geomyy=%e\n avg_neg_z_geomzz=%e\n error_neg_x_geomxx=%e\n error_neg_x_geomyy=%e\n error_neg_x_geomzz=%e\n error_neg_y_geomxx=%e\n error_neg_y_geomyy=%e\n error_neg_y_geomzz=%e\n error_neg_z_geomxx=%e\n error_neg_z_geomyy=%e\n error_neg_z_geomzz=%e\n "% (avg_neg_x[0], avg_neg_x[1], avg_neg_x[2], avg_neg_y[0], avg_neg_y[1], avg_neg_y[2], avg_neg_z[0], avg_neg_z[1], avg_neg_z[2], error_neg_x[0], error_neg_x[1], error_neg_x[2], error_neg_y[0], error_neg_y[1], error_neg_y[2], error_neg_z[0], error_neg_z[1], error_neg_z[2],))
            how_many_bad_voids = (np.where(np.isnan(acc_x_geom[:,0]))[0]).size
            how_many_good_voids = acc_x_geom[:,0].size - how_many_bad_voids
            F.write("CountedVoids_geom=%i\n" % how_many_good_voids)

        if(enable_pot_centre):
            print('potential centre:')
            avg_x = np.nanmean(acc_x_pot,axis=0)*Julian_Gyr_in_seconds
            avg_y = np.nanmean(acc_y_pot,axis=0)*Julian_Gyr_in_seconds
            avg_z = np.nanmean(acc_z_pot,axis=0)*Julian_Gyr_in_seconds
            error_x = np.nanstd(acc_x_pot,axis=0)*Julian_Gyr_in_seconds
            error_y = np.nanstd(acc_y_pot,axis=0)*Julian_Gyr_in_seconds
            error_z = np.nanstd(acc_z_pot,axis=0)*Julian_Gyr_in_seconds
            F.write(" avg_x_potxx=%e\n avg_x_potyy=%e\n avg_x_potzz=%e\n avg_y_potxx=%e\n avg_y_potyy=%e\n avg_y_potzz=%e\n avg_z_potxx=%e\n avg_z_potyy=%e\n avg_z_potzz=%e\n error_x_potxx=%e\n error_x_potyy=%e\n error_x_potzz=%e\n error_y_potxx=%e\n error_y_potyy=%e\n error_y_potzz=%e\n error_z_potxx=%e\n error_z_potyy=%e\n error_z_potzz=%e\n "% (avg_x[0], avg_x[1], avg_x[2], avg_y[0], avg_y[1], avg_y[2], avg_z[0], avg_z[1], avg_z[2], error_x[0], error_x[1], error_x[2], error_y[0], error_y[1], error_y[2], error_z[0], error_z[1], error_z[2],))
            avg_neg_x = np.nanmean(acc_neg_x_pot,axis=0)*Julian_Gyr_in_seconds
            avg_neg_y = np.nanmean(acc_neg_y_pot,axis=0)*Julian_Gyr_in_seconds
            avg_neg_z = np.nanmean(acc_neg_z_pot,axis=0)*Julian_Gyr_in_seconds
            error_neg_x = np.nanstd(acc_neg_x_pot,axis=0)*Julian_Gyr_in_seconds
            error_neg_y = np.nanstd(acc_neg_y_pot,axis=0)*Julian_Gyr_in_seconds
            error_neg_z = np.nanstd(acc_neg_z_pot,axis=0)*Julian_Gyr_in_seconds
            F.write(" avg_neg_x_potxx=%e\n avg_neg_x_potyy=%e\n avg_neg_x_potzz=%e\n avg_neg_y_potxx=%e\n avg_neg_y_potyy=%e\n avg_neg_y_potzz=%e\n avg_neg_z_potxx=%e\n avg_neg_z_potyy=%e\n avg_neg_z_potzz=%e\n error_neg_x_potxx=%e\n error_neg_x_potyy=%e\n error_neg_x_potzz=%e\n error_neg_y_potxx=%e\n error_neg_y_potyy=%e\n error_neg_y_potzz=%e\n error_neg_z_potxx=%e\n error_neg_z_potyy=%e\n error_neg_z_potzz=%e\n "% (avg_neg_x[0], avg_neg_x[1], avg_neg_x[2], avg_neg_y[0], avg_neg_y[1], avg_neg_y[2], avg_neg_z[0], avg_neg_z[1], avg_neg_z[2], error_neg_x[0], error_neg_x[1], error_neg_x[2], error_neg_y[0], error_neg_y[1], error_neg_y[2], error_neg_z[0], error_neg_z[1], error_neg_z[2],))
            how_many_bad_voids = (np.where(np.isnan(acc_x_pot[:,0]))[0]).size
            how_many_good_voids = acc_x_pot[:,0].size - how_many_bad_voids
            F.write("CountedVoids_pot=%i\n" % how_many_good_voids)
            acc_rad_med, acc_rad_sig, acc_rad_stderr_med = median_stats(acc_rad_non_nan)
            F.write("acc_rad_med = %.3f\n" % (acc_rad_med*Julian_Gyr_in_seconds))
            F.write("acc_rad_sig = %.3f\n" % (acc_rad_sig*Julian_Gyr_in_seconds))
            F.write("acc_rad_stderr_med = %.3f\n" % (acc_rad_stderr_med*Julian_Gyr_in_seconds))
            acc_tan_med, acc_tan_sig, acc_tan_stderr_med = median_stats(acc_tan_non_nan)
            F.write("acc_tan_med = %.3f\n" % (acc_tan_med*Julian_Gyr_in_seconds))
            F.write("acc_tan_sig = %.3f\n" % (acc_tan_sig*Julian_Gyr_in_seconds))
            F.write("acc_tan_stderr_med = %.3f\n" % (acc_tan_stderr_med*Julian_Gyr_in_seconds))

        if(enable_circum_centre):
            print('circumcentre:')
            avg_x = np.nanmean(acc_x_circ,axis=0)*Julian_Gyr_in_seconds
            avg_y = np.nanmean(acc_y_circ,axis=0)*Julian_Gyr_in_seconds
            avg_z = np.nanmean(acc_z_circ,axis=0)*Julian_Gyr_in_seconds
            error_x = np.nanstd(acc_x_circ,axis=0)*Julian_Gyr_in_seconds
            error_y = np.nanstd(acc_y_circ,axis=0)*Julian_Gyr_in_seconds
            error_z = np.nanstd(acc_z_circ,axis=0)*Julian_Gyr_in_seconds
            F.write(" avg_x_circxx=%e\n avg_x_circyy=%e\n avg_x_circzz=%e\n avg_y_circxx=%e\n avg_y_circyy=%e\n avg_y_circzz=%e\n avg_z_circxx=%e\n avg_z_circyy=%e\n avg_z_circzz=%e\n error_x_circxx=%e\n error_x_circyy=%e\n error_x_circzz=%e\n error_y_circxx=%e\n error_y_circyy=%e\n error_y_circzz=%e\n error_z_circxx=%e\n error_z_circyy=%e\n error_z_circzz=%e\n "% (avg_x[0], avg_x[1], avg_x[2], avg_y[0], avg_y[1], avg_y[2], avg_z[0], avg_z[1], avg_z[2], error_x[0], error_x[1], error_x[2], error_y[0], error_y[1], error_y[2], error_z[0], error_z[1], error_z[2],))
            avg_neg_x = np.nanmean(acc_neg_x_circ,axis=0)*Julian_Gyr_in_seconds
            avg_neg_y = np.nanmean(acc_neg_y_circ,axis=0)*Julian_Gyr_in_seconds
            avg_neg_z = np.nanmean(acc_neg_z_circ,axis=0)*Julian_Gyr_in_seconds
            error_neg_x = np.nanstd(acc_neg_x_circ,axis=0)*Julian_Gyr_in_seconds
            error_neg_y = np.nanstd(acc_neg_y_circ,axis=0)*Julian_Gyr_in_seconds
            error_neg_z = np.nanstd(acc_neg_z_circ,axis=0)*Julian_Gyr_in_seconds
            F.write(" avg_neg_x_circxx=%e\n avg_neg_x_circyy=%e\n avg_neg_x_circzz=%e\n avg_neg_y_circxx=%e\n avg_neg_y_circyy=%e\n avg_neg_y_circzz=%e\n avg_neg_z_circxx=%e\n avg_neg_z_circyy=%e\n avg_neg_z_circzz=%e\n error_neg_x_circxx=%e\n error_neg_x_circyy=%e\n error_neg_x_circzz=%e\n error_neg_y_circxx=%e\n error_neg_y_circyy=%e\n error_neg_y_circzz=%e\n error_neg_z_circxx=%e\n error_neg_z_circyy=%e\n error_neg_z_circzz=%e\n "% (avg_neg_x[0], avg_neg_x[1], avg_neg_x[2], avg_neg_y[0], avg_neg_y[1], avg_neg_y[2], avg_neg_z[0], avg_neg_z[1], avg_neg_z[2], error_neg_x[0], error_neg_x[1], error_neg_x[2], error_neg_y[0], error_neg_y[1], error_neg_y[2], error_neg_z[0], error_neg_z[1], error_neg_z[2],))
            how_many_bad_voids = (np.where(np.isnan(acc_x_circ[:,0]))[0]).size
            how_many_good_voids = acc_x_circ[:,0].size - how_many_bad_voids
            F.write("CountedVoids_circ=%i\n" % how_many_good_voids)

        print('print statistics for the different void centres')

    res.statistics(member_ids, tracers, pot_centre, circ_centre, geom_centre,eff_void_radius, num_voids)
    #res.void_pot_crosssection(tracers, member_ids, pots, OutputDir)
    res.plot_void_population(eff_void_radius)
    res.do_plots(member_ids, tracers, gadget_ids, pot_centre, circ_centre, geom_centre, eff_void_radius,
                 num_voids, halo_particle_lists, gal_ids, void_ids_of_gals, void_id_from_revolver, OutputDir)
    res.corrrel(num_voids, pots, member_ids, eff_void_radius, void_file)

    f= open("galpos.dat","w+")
    for i in range(0,len(G.Pos[:,0])):
        f.write("%.3f %.3f %.3f\n" % (G.Pos[i,0], G.Pos[i,1], G.Pos[i,2]))

    profile_object.disable()
    profile_object.print_stats()
