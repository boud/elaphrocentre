#!/usr/bin/env python

# theil_sen - Theil-Sen linear regression - robust linear fit
# (C) 2018-2020 Boud Roukema GPL-3+
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import sys
import numpy as np
from numpy.random import SeedSequence, default_rng
import matplotlib
import matplotlib.pyplot as plt

def theil_sen(x_in, y_in, N_max_sample = -1, fixed_seed = False):
    """
    Theil-Sen linear regression - robust linear fit
    e.g. https://en.wikipedia.org/w/index.php?title=Theil%E2%80%93Sen_estimator&oldid=865140889

    positional input parameters:
    * numpy arrays x_in (independent), y_in (dependent)

    keyword optional parameters:
    * N_max_sample: integer  (default = -1 = disabled)
        If N_max_sample > 0, then a maximum of N_max_sample pairs (randomly chosen)
        will be used for calculating slopes, instead of all pairs; degenerate
        pairs will be ignored, so the true number will usually be less than
        N_max_sample.

    * fixed_seed: boolean (default: False)
        Use a fixed seed for pseudo-random number generator?

    """

    ## TODO: error feedback for cases with two few points
    N_x = x_in.size;
    N_y = y_in.size;
    if (N_x != N_y):
        raise RuntimeError("theil_sen: Error: N_x != N_y:", N_x,N_y)

    x_local = np.tile(x_in, (N_x,1))
    y_local = np.tile(y_in, (N_x,1))

    if(N_max_sample <= 0):
        ## prepare to select the upper right triangle where i_col > i_row
        i_col, i_row = np.meshgrid(np.arange(N_x),np.arange(N_x))

        ## calculate x and y differences; and slopes
        dx = np.transpose(x_local) - x_local
        dy = np.transpose(y_local) - y_local
        indices = np.array(np.where(i_col > i_row))
        #print('indices = ',indices)
        #slopes_all = dy/dx
        #print('slopes_all = ',slopes_all)
        indices_zip = zip(indices[0],indices[1])
        slopes = np.array([])
        for ij in indices_zip:
            # ignore nans or \pm Inf from doubled x values
            #print('ij=',ij,'slopes_all[ij]= ',slopes_all[ij])
            slope = dy[ij]/dx[ij]
            if np.isfinite(slope):
                slopes = np.append(slopes,slope)
    else:
        if(fixed_seed):
            seeds = SeedSequence(7477)
            seed = seeds.spawn(2)
            rng = default_rng(seed[0])
        else:
            rng = default_rng()

        # Calculate slopes only for a random subsample
        indices1 = rng.integers(N_x, size=N_max_sample)
        indices2 = rng.integers(N_x, size=N_max_sample)
        #print('indices1[0] = ',indices1[0]) # debug only

        ## calculate x and y differences; and slopes
        indices_zip = zip(indices1,indices2)
        slopes = np.array([])
        for i12 in indices_zip:
            # ignore nans or \pm Inf from doubled x values
            if(x_in[i12[0]] != x_in[i12[1]]):
                slope = (y_in[i12[0]]-y_in[i12[1]])/ (x_in[i12[0]]-x_in[i12[1]])
                if np.isfinite(slope):
                    slopes = np.append(slopes,slope)

    #print('slopes=',slopes)
    pslope = np.median(slopes)

    ## median intercept
    pzeropoint = np.median(y_in - pslope* x_in)

    return (pslope, pzeropoint)

def theil_sen_robust_stderr(x_in, y_in,
                            N_max_sample = -1):
    """
    bootstrap estimate of uncertainty in Theil-Sen zeropoint

    positional input parameters:
    * numpy arrays x_in (independent), y_in (dependent)

    keyword optional parameters:
    * N_max_sample: integer  (by default disabled)
        If N_max_sample > 0, then a maximum of N_max_sample pairs (randomly chosen)
        will be used for calculating slopes, instead of all pairs; degenerate
        pairs will be ignored, so the true number will usually be less than
        N_max_sample.

    """

    ## TODO: error feedback for cases with two few points

    ## sigma is 1.4826 * median absolute deviation for Gaussian
    sig_from_MAD = 1.4826;

    N_boot = 100;
    ps = np.zeros(N_boot)
    pz = np.zeros(N_boot)
    N_x = x_in.size

    seeds = SeedSequence(4733)
    seed = seeds.spawn(2)
    rng = default_rng(seed[0])
    #rng = default_rng()

    for i in np.arange(N_boot):
        ## bootstrap indices - indices allowing resampling
        j = rng.integers(N_x, size=N_x)
        (ps[i], pz[i]) = ( theil_sen(x_in[j], y_in[j],
                                     N_max_sample = N_max_sample,
                                     fixed_seed = False) )

    ps_median = np.median(ps)
    pz_median = np.median(pz)
    ## robust estimate of variation
    sig_pslope = np.median(np.abs(ps-ps_median)) * sig_from_MAD;
    sig_pzeropoint = np.median(np.abs(pz-pz_median)) * sig_from_MAD;

    return sig_pslope, sig_pzeropoint


def test_theil_sen():
    ## interactive test parameters
    show_plot = True

    N_x_gauss = 50
    noise_amp = 1.0
    N_x_outlier = 10
    outl_noise_amp = 2.0

    N_x = N_x_gauss + N_x_outlier

    true_slope = np.pi
    true_zeropoint = np.e
    outlier_slope = -2.0
    outlier_zeropoint = 3.0

    ## gaussian scatter
    seeds = SeedSequence(4547)
    seed = seeds.spawn(2)
    rng = default_rng(seed[0])
    #rng = default_rng()

    # Choose some x values randomly in the range from 0 to 1
    x = rng.uniform(size=N_x)

    ## Create a linear relation with a little Gaussian noise
    ii_gauss = np.arange(N_x_gauss)
    y = np.zeros(N_x)
    y[ii_gauss] = (true_slope * x[ii_gauss] +
                   true_zeropoint +
                   rng.normal(size=N_x_gauss)*noise_amp)

    ## Add some outliers - with a different slope, zeropoint, noise, and add
    ## a square component:
    ii_outl = np.arange(N_x_gauss,N_x)
    y[ii_outl] = (outlier_slope*x[ii_outl] +
                  outlier_zeropoint -
                  (x[ii_outl]**2) +
                  rng.normal(size=N_x_outlier) * outl_noise_amp)

    if(show_plot):
        matplotlib.use('Agg')
        plt.figure()
        plt.scatter(x[ii_gauss],y[ii_gauss],marker='o',c='g',label='good')
        plt.scatter(x[ii_outl],y[ii_outl],marker='x',c='r',label='bad')
        plt.savefig('theil_sen_test.eps')
        plt.close()

    print("\n\ntrue: %g %g\n" % (true_slope,true_zeropoint))
    pp = np.polyfit(x, y, 1)
    print("polyfit: %g %g\n" % (pp[0], pp[1]))
    N_max_sample = 30
    pslope, pzeropoint = theil_sen(x, y,
                                   N_max_sample = N_max_sample,
                                   fixed_seed = True)
    print("theil-sen: %g %g\n" % (pslope, pzeropoint))

    sig_pslope, sig_pzeropoint = (
        theil_sen_robust_stderr(x,y, N_max_sample = N_max_sample) )
    print("sigma theil-sen: %g %g\n" % (sig_pslope, sig_pzeropoint))

    #plot([0 1], pslope.*[0 1] .+ pzeropoint,'-k;Theil--Sen;','linewidth',5)
    #plot([0 1], pp(1).*[0 1] .+ pp(2),'-b;polyfit;','linewidth',5)

    return


if __name__ == '__main__':

    pass_val = 0 # pass_val value of test: 0 = OK, 1 = fail

    test_theil_sen() # interactive

    print("pass_val value = ",pass_val)
    sys.exit(pass_val)

#end __main__
