#!/usr/bin/env python

# test_move_coord - test the move_coordinates method
# (C) 2020 Marius Peper, Boud Roukema GPL-3+
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as patches
#from mpl_toolkits.mplot3d import Axes3D
from extra_calculations import calculations

import sys

def make_plot(particles, centre, box_length, outfilename):
    fig = plt.figure()
    half_box = box_length/2.0
    
    ax = plt.subplot(221)  # 1 plot on the figure
    plt.scatter(particles[:,0], particles[:,1])
    plt.scatter(centre[0], centre[1], color='red')

    ax.set_xlabel("y in Mpc")
    ax.set_ylabel("x in Mpc")
    # XY plane: Display the covering space centred on the void, showing
    # four copies of the FD:
    ax.axis([centre[0]-box_length, centre[0]+box_length,
             centre[1]-box_length, centre[1]+box_length])

    # Show the FD centred on the void:
    FD_void = patches.Rectangle((centre[0]-half_box,centre[1]-half_box),
                                box_length,box_length,
                                linewidth=1, edgecolor='g',
                                facecolor='none')
    ax.add_patch(FD_void) 
    
    ax = plt.subplot(222)  # 1 plot on the figure
    plt.scatter(particles[:,0], particles[:,2])
    plt.scatter(centre[0], centre[2], color='red')
    
    ax.set_xlabel("z in Mpc")
    ax.set_ylabel("x in Mpc")
    ax.axis([centre[0]-box_length, centre[0]+box_length,
             centre[2]-box_length, centre[2]+box_length])

    # Show the FD centred on the void:
    FD_void = patches.Rectangle((centre[0]-half_box,centre[2]-half_box),
                                box_length,box_length,
                                linewidth=1, edgecolor='g',
                                facecolor='none')
    ax.add_patch(FD_void) 

    
    ax = plt.subplot(223)  # 1 plot on the figure
    plt.scatter(particles[:,1], particles[:,2])
    plt.scatter(centre[1], centre[2], color='red')

    ax.set_xlabel("y in Mpc")
    ax.set_ylabel("z in Mpc")
    ax.axis([centre[1]-box_length, centre[1]+box_length,
             centre[2]-box_length, centre[2]+box_length])
    # Show the FD centred on the void:
    FD_void = patches.Rectangle((centre[1]-half_box,centre[2]-half_box),
                                box_length,box_length,
                                linewidth=1, edgecolor='g',
                                facecolor='none')
    ax.add_patch(FD_void) 

    
    plt.savefig(outfilename)
    

if __name__ == '__main__':
    calc=calculations()

    global box_length
    # Particle locations in the default fundamental domain [0,box_length]^3
    particles = np.array([[7.5,0.8,6.1],[0.5,5.3,1.9],[5.5,9.1,1.4],[8.7,3.3,3.2]])
    # Void centres
    centres = np.array([[9.5,2.3,5.2],[4.4,3.5,4.3]])
    box_length = 10

    expected_moved_particles_0 = (
        np.array([[-2.,  -1.5,  0.9],
                  [ 1.,   3.,  -3.3],
                  [-4.,  -3.2, -3.8],
                  [-0.8,  1.,  -2. ]]) )
    expected_moved_particles_1 = (
        np.array([[ 3.1, -2.7,  1.8],
                  [-3.9,  1.8, -2.4],
                  [ 1.1, -4.4, -2.9],
                  [ 4.3, -0.2, -1.1]]) )
    
    moved_particles0=calc.move_coordinates(particles[:,0], particles[:,1], particles[:,2], centres[0], box_length)
    moved_particles1=calc.move_coordinates(particles[:,0], particles[:,1], particles[:,2], centres[1], box_length)
    moved_centre0=calc.move_coordinates(centres[0,0], centres[0,1], centres[0,2], centres[0], box_length)
    moved_centre1=calc.move_coordinates(centres[1,0], centres[1,1], centres[1,2], centres[1], box_length)

    print("particles=",particles)
    print("centres=",centres)
    print("moved_particles0=",moved_particles0)
    print("moved_particles1=",moved_particles1)
    print("moved_centre0=",moved_centre0)
    print("moved_centre1=",moved_centre1)

    make_plot(particles, centres[0], box_length, 'voidcentres_0.png')
    make_plot(particles, centres[1], box_length, 'voidcentres_1.png')
    make_plot(moved_particles0, moved_centre0, box_length, 'moved_voidcentres_0.png')
    make_plot(moved_particles1, moved_centre1, box_length, 'moved_voidcentres_1.png')

    pass_fail_code = 0

    tol = 1e-10
    if(np.max(np.abs(moved_centre0) > tol) or
       np.max(np.abs(moved_centre1) > tol)):
        # Signal an error in the void centres location with respect to themselves.
        print("Error detected in shifting void centres.")
        pass_fail_code += 1

    if( (np.max(np.abs(expected_moved_particles_0 - moved_particles0)) > tol) or
        (np.max(np.abs(expected_moved_particles_1 - moved_particles1)) > tol) ):
        print("Error detected in shifting particles centres.")
        print("case 0: ",expected_moved_particles_0 - moved_particles0)
        print("case 1: ",expected_moved_particles_1 - moved_particles1)
        pass_fail_code += 2
        
    if(0==pass_fail_code):
        print("Test_move_coord: check passed.")
    else:
        print("Test_move_coord: check FAILED.")
    
    sys.exit(pass_fail_code)
