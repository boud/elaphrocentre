import sys
import numpy as np
import matplotlib
matplotlib.use('Agg')
from scipy.optimize import curve_fit, OptimizeWarning
import matplotlib.pyplot as plt
import re # for regular expressions
import warnings

debug = True

#maximum_allowed_fit_amplitude = 0.1

def gaussian(x, mu, sig, A):
    """
    Gaussian model for the least-squares curve_fit algorithm
    """
    prefactor=1/(sig*np.sqrt(2*np.pi))
    exparg=(x-mu)/sig
    expfct=np.exp(-0.5*exparg*exparg)

    return A*prefactor*expfct


def read_parameter(filename, expl_string,
                   maximum_allowed_fit_amplitude = 100.0):
    """
    Read in the best-fit parameter derived with exptest.py and
    drop data where the method failed to converge.

    Parameters
    ----------
    filename: string
         File name containing a dataset
    maximum_allowed_fit_amplitude: float
         Fits will be considered unacceptable if amplitude is greater than this value

    returns
    -------
    good_params : np.array
    Array that holds the best-fit parameters derived by exptest.py
    """

    bad_params = np.array([[0., 0.], [0., 0.]])
    print("infall_stat: Will read: ",filename)
    param = np.loadtxt(filename)
    print("(param.shape)[0]=",(param.shape)[0])
    print("infall_stat: param = ",param)
    #Raise an error if the file has no data in it.
    if((param.shape)[0]<1):
        raise RuntimeError('No data found in ', filename,' .')

    #remove extreme values - probably bad fits or bad data
    if((param.ndim > 1)):
        nonzero_row_indices = np.where(param[:,0] != 0)
        if(not(nonzero_row_indices)):
            warnings.warn("A zero row was found in "+filename+" .")
            return bad_params
    else:
        warnings.warn("Only one line was found in "+filename+" .")
        return bad_params

    good_params = param[nonzero_row_indices]
    good_params = good_params[np.where(np.abs(good_params[:,0]) <=
                                       maximum_allowed_fit_amplitude)]

    if( (good_params.shape)[0] < 1 ):
        warnings.warn("No valid lines with amplitudes lower than "
                      +str(maximum_allowed_fit_amplitude)
                      +" were found in "+filename+" .")
        return bad_params

    else:
        outfile = "exp_fit_params_statistics_" + expl_string + ".dat"
        with open(outfile,"w+") as F:
            n_good_params = (good_params.shape)[0]
            # Count failed fits. The return value of np.where is not necessarily an array
            # as of numpy-1.17.2, so np.array is needed to guarantee that.
            failedfit_indices = np.array(np.where(param[:,0] == 0))
            n_bad_params_failedfit = (failedfit_indices.shape)[0]
            n_bad_params_threshold = (param.shape)[0] - n_good_params - n_bad_params_failedfit
            n_bad_total = (param.shape)[0] - n_good_params
            F.write("ParameterThresholdInput = %.2f\n" % maximum_allowed_fit_amplitude)
            F.write("GoodParameter = %i\n" % n_good_params)
            F.write("BadParameterThreshold = %i\n" % n_bad_params_threshold)
            F.write("BadParameterFailedFit = %i\n" % n_bad_params_failedfit)
            F.write("BadParameterTotal = %i\n" % n_bad_total)
        F.close()

    return good_params

def plot_histogram(filename_test, filename_compare, expl_string_data, expl_string_test, expl_string_compare,
                   maximum_allowed_fit_amplitude = 0.1):
    """
    Wrapper for find_mu which fits a Gaussian
    to the best-fit parameter of exptest.py.
    Plot the results.

    Parameters
    ----------
    filename_test: string
       File name containing a dataset
    filename_compare: string
       File name containing a dataset to compare
    expl_string_data: string
       explanatory string to identify for what data the best-fit parameter were derived
    expl_string_test: string
       explanatory string to describe the test data
    expl_string_compare: string
       explanatory string to describe the compare data
    maximum_allowed_fit_amplitude: float
         Fits will be considered unacceptable if amplitude is greater than this value


    returns
    -------
    """

    # We want to compare the distributions of two datasets. We want to compare the dataset 'compare' to the dataset 'test'

    params_test=read_parameter(filename_test, expl_string_test,
                               maximum_allowed_fit_amplitude = maximum_allowed_fit_amplitude)
    params_compare=read_parameter(filename_compare, expl_string_compare,
                                  maximum_allowed_fit_amplitude = maximum_allowed_fit_amplitude)

    logdata_test = np.log10(params_test[:,0])
    logdata_compare = np.log10(params_compare[:,0])
    #bins_voids = np.linspace(min(params_voids[:,0]),max(params_voids[:,0]),30)
    if(debug):
        print('filename_test = ',filename_test)
        print('params_test = ',params_test)
        print('logdata_test = ',logdata_test)
    bins_test = np.linspace(min(logdata_test),max(logdata_test),30)
    #bins_non_voids = np.linspace(min(params_non_voids[:,0]),max(params_non_voids[:,0]),30)
    bins_compare = np.linspace(min(logdata_compare),max(logdata_compare),30)
    #mu_voids0, sig_voids0, norm_voids0 = find_mu(params_voids[:,0],bins_voids)
    mu_test0, sig_test0, norm_test0 = find_mu(logdata_test,bins_test,"calculate mu_voids0")
    mu_compare0, sig_compare0, norm_compare0 = find_mu(logdata_compare,bins_compare,"calculate mu_non_voids0")
    #print("logdata_voids =",logdata_test)

    indices_valid_test = (np.array(np.where(np.isfinite(logdata_test))))[0]
    indices_valid_compare = (np.array(np.where(np.isfinite(logdata_compare))))[0]

    med_test0, sig_med_test0, stderr_med_test0 = median_stats(logdata_test[indices_valid_test])
    med_compare0, sig_med_compare0, stderr_med_compare0 = median_stats(logdata_compare[indices_valid_compare])

    plt.ylabel(r'$\mathrm{Counts}$')
    plt.xlabel(r'$\mathrm{log10(Infall Amplitude)}$')
    #plt.hist(params_non_voids[:,0], bins=bins_non_voids, color ='red', label='non voids')
    plt.hist(logdata_compare, bins=bins_compare, color ='red', label=expl_string_compare)
    #plt.plot(bins_compare, gaussian(bins_compare, mu_compare0, sig_compare0, norm_compare0),'y+', label="Gaussian" + expl_string_compare)
    plt.axvline(x=med_compare0, c='orange', label="Median " + expl_string_compare)
    # Better solution would be to plot a filled region to visualise the standard error.
    # The problem is that the filled region would be shown over the historgram.
    # A temporary solution is to plot two lines to indicate the standard error
    #plt.axvspan(med_compare0-sig_med_compare0,med_compare0+sig_med_compare0, color='lemonchiffon')
    #plt.hist(params_voids[:,0], bins=bins_voids, color ='green', label='voids')
    plt.axvline(x=med_compare0-sig_med_compare0, color='yellow')
    plt.axvline(x=med_compare0+sig_med_compare0, color='yellow')
    plt.hist(logdata_test, bins=bins_test, color ='green', label=expl_string_test)
    #plt.plot(bins_test, gaussian(bins_test, mu_test0, sig_test0, norm_test0),'b+', label="Gaussian " + expl_string_test)
    plt.axvline(x=med_test0, c='blue', label="Median " + expl_string_test)
    plt.axvline(x=med_test0-sig_med_test0, color='royalblue')
    plt.axvline(x=med_test0+sig_med_test0, color='royalblue')
    #plt.axvspan(med_test0-sig_med_test0,med_test0+sig_med_test0, color='lavender')
    plt.legend(loc='upper right', numpoints=1, labelspacing=0.1)
    plt.savefig(expl_string_data + "-amplitude-" + expl_string_test + "_" + expl_string_compare + ".eps")
    plt.close()

    # log10 void amplitude - log10 nonvoid amplitude
    delta_med0 = med_test0 - med_compare0
    # naive Gaussian interpretation of significance of (log10 void amplitude - log10 nonvoid amplitude)
    delta_med0_gauss_sig = (np.abs(delta_med0) /
                          np.sqrt(sig_med_test0**2 + sig_med_compare0**2))
    delta_med0_voids_percent_lower = 100.0*(1.0 - (10.0**(delta_med0)))

    with open(expl_string_data + "-amplitude-" + expl_string_test + "_" + expl_string_compare +".dat","w+") as F:
        F.write("ExpectationValueAmplitudeVoids = %.2e\n" % mu_test0)
        F.write("ExpectationValueAmplitudeNonVoids = %.2e\n" % mu_compare0)
        F.write("StandardDeviationAmplitudeVoids = %.2e\n" % sig_test0)
        F.write("StandardDeviationAmplitudeNonVoids = %.2e\n" % sig_compare0)
        F.write("MedianValueAmplitudeVoids = %.2f\n" % med_test0)
        F.write("MedianValueAmplitudeNonVoids = %.2f\n" % med_compare0)
        F.write("RobustErrorAmplitudeVoids = %.2f\n" % stderr_med_test0)
        F.write("RobustErrorAmplitudeNonVoids = %.2f\n" % stderr_med_compare0)

        F.write("MedianAmplitudeDiffLog = %.2f\n" % delta_med0)
        F.write("MedianAmplitudeSignif = %.1f\n" % delta_med0_gauss_sig)
        F.write("MedianAmplitudeVoidsPercentLower = %.2f\n" % delta_med0_voids_percent_lower)

    bins_test = np.linspace(min(params_test[:,1]),max(params_test[:,1]),30)
    bins_compare = np.linspace(min(params_compare[:,1]),max(params_compare[:,1]),30)
    mu_test1, sig_test1, norm_test1 = find_mu(params_test[:,1],bins_test,"calculate mu_voids1")
    mu_compare1, sig_compare1, norm_compare1 = find_mu(params_compare[:,1],bins_compare,"calculate mu_non_voids1")

    med_test1, sig_med_test1, stderr_med_test1 = median_stats(params_test[indices_valid_test])
    med_compare1, sig_med_compare1, stderr_med_compare1 = median_stats(params_compare[indices_valid_compare])

    plt.ylabel(r'$\mathrm{Counts}$')
    plt.xlabel(r'$\mathrm{Decay Rate}$')
    plt.hist(params_compare[:,1], bins=bins_compare, color ='red', label=expl_string_compare)
    #plt.plot(bins_compare, gaussian(bins_compare, mu_compare1, sig_compare1, norm_compare1),'y+', label="Gaussian" + expl_string_compare)
    plt.axvline(x=med_compare1, c='orange', label="Median " + expl_string_compare)
    plt.axvline(x=med_compare1-sig_med_compare1, color='yellow')
    plt.axvline(x=med_compare1+sig_med_compare1, color='yellow')
    #plt.axvspan(med_compare1-sig_med_compare1,med_compare1+sig_med_compare1, color='yellow', alpha=0.3, rasterized=True)
    plt.hist(params_test[:,1], bins=bins_test, color ='green', label=expl_string_test)
    #plt.plot(bins_test, gaussian(bins_test, mu_test1, sig_test1, norm_test1),'b+', label="Gaussian" + expl_string_test)
    plt.axvline(x=med_test1, c='blue', label="Median " + expl_string_test)
    plt.axvline(x=med_test1-sig_med_test1, color='royalblue')
    plt.axvline(x=med_test1+sig_med_test1, color='royalblue')
    #plt.axvspan(med_test1-sig_med_test1,med_test1+sig_med_test1, color='royalblue', alpha=0.3, rasterized=True)
    plt.legend(loc='upper right', numpoints=1, labelspacing=0.1)
    plt.savefig(expl_string_data + "-decay-" + expl_string_test + "_" + expl_string_compare + ".eps")
    plt.close()

    with open(expl_string_data + "-decay-" + expl_string_test + "_" + expl_string_compare +".dat","w+") as F:
        F.write("ExpectationValueDecayVoids = %.2e\n" % mu_test1)
        F.write("ExpectationValueDecayNonVoids = %.2e\n" % mu_compare1)
        F.write("StandardDeviationDecayVoids = %.2e\n" % sig_test1)
        F.write("StandardDeviationDecayNonVoids = %.2e\n" % sig_compare1)
        F.write("MedianValueDecayVoids = %.2e\n" % med_test1)
        F.write("MedianValueDecayNonVoids = %.2e\n" % med_compare1)
        F.write("RobustErrorDecayVoids = %.2e\n" % stderr_med_test1)
        F.write("RobustErrorDecayNonVoids = %.2e\n" % stderr_med_compare1)


def find_mu(params,bins,description):
    """
    Use scipy.optimize.curve_fit to fit a Gaussian
    to the best-fit parameter of exptest.py.

    Parameters
    ----------
    params : np.array
    Array of data to be fit by a Gaussian
    bins : np.array
    Array that defines the analysed bins

    returns
    -------
    popt : array([mu, sig, A])

    np.array([mu, sig, A]) minimizes the squared residuals of a Gaussian fit to the given distribution    """


    if(not(np.isfinite(np.max(params))) or
       not(np.isfinite(np.min(params))) or
       np.min(params) >= np.max(params)):
        warnings.warn("Cannot plot "+str(description)+" ; min,max="+str(np.min(params))+" "+str(np.max(params)))
        return 0.,0.,0.

    (counts, binedges) = np.histogram(params, range=(min(params), max(params)), bins=30)
    # expectation value
    #print("max(counts)=",max(counts))
    index = np.where(counts==np.max(counts))[0]
    print("index=",index)
    mu = bins[index][0]
    print("mu=",mu)
    error=np.sum(counts[:]-mu)*np.sum(counts[:]-mu,axis=0)/(np.maximum(1.0,counts.size-1.0))
    error=np.sqrt(error)
    print("error=",error)

    #popt,pcov = curve_fit(gaussian, bins, counts, p0=[mu,0.0005,0.0001], maxfev = 600000)
    try:
        popt,pcov = curve_fit(gaussian, bins, counts, p0=[mu,error,0.01], maxfev = 6000)
    except RuntimeError as e:
        #curve_fit fails so we set popt = [0., 0.]
        print("curve_fit to a Gaussian failed, returning [0,0] and continuing!")
        popt = [0., 0., 0.]
        if description:
            errorFile = re.sub("\s","_",description)+".err"
            with open(errorFile, 'a') as f:
                #print(e.args,file=f)
                f.write("%s\n" % e.args)
    except OptimizeWarning as e:
        #only catch optimization warning when the fit succeeds but fails
        #to find the covariance matrix. We are only interested in popt
        print(e.args)
        print("Still continuing!")

    print("popt=",popt)
    return popt[0], popt[1], popt[2]


def median_stats(input_array):
    """
    Return robust statistics of input_array

    med : median

    sig_median : Gaussian-comparable median absolute deviation

    stderr_median : standard error of the median
    """
    Gaussian_interpretation = 1.4826

    arr = np.array(input_array) # make sure that we have a numpysed array
    N = arr.size
    med = np.median(arr)
    sig_median = (Gaussian_interpretation *
                  np.median(np.abs(arr - med)))
    stderr_median = ( np.sqrt( np.sum((arr - med)**2)/
                               np.float(max(1,N-1)) )/
                      np.sqrt(np.float(max(1,N))) )
    return med, sig_median, stderr_median


if __name__ == '__main__':

    filename_test = sys.argv[1]
    filename_compare = sys.argv[2]
    expl_string_data = sys.argv[3]
    expl_string_test = sys.argv[4]
    expl_string_compare = sys.argv[5]

    plot_histogram(filename_test, filename_compare, expl_string_data, expl_string_test, expl_string_compare)
