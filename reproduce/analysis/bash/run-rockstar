#!/usr/bin/env bash

# run rockstar using parameters in Nbody-sim-params.conf
# Copyright (C) 2016-2020 Boud Roukema, Marius Peper, GPL-3+

# Run this in a directory in which you are happy to produce many output files.
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.


function write_rockstar_input {
    cat <<EOF
# FILE_FORMAT = "GADGET2" or "ART" or "ASCII" or "TIPSY" or "AREPO"
FILE_FORMAT = "GADGET2"

GADGET_MASS_CONVERSION = ${1}
GADGET_LENGTH_CONVERSION = 1
GADGET_SKIP_NON_HALO_PARTICLES = 0
MASS_DEFINITION = "${10}"

MIN_HALO_OUTPUT_SIZE = ${8}
MIN_HALO_PARTICLES = ${9}
FORCE_RES = ${5}
FOF_LINKING_LENGTH = ${6}
FOF_FRACTION = ${7}

PARALLEL_IO = 1
PARALLEL_IO_SERVER_ADDRESS = localhost

NUM_BLOCKS = 1

INBASE = "${3}"
FILENAME = "out_gadget<snap>"

NUM_SNAPS = ${2}

STARTING_SNAP = ${4}

NUM_WRITERS = 8
PERIODIC = 1

OUTBASE = "./"

FORK_READERS_FROM_WRITERS = 1
FORK_PROCESSORS_PER_MACHINE = 8

FULL_PARTICLE_CHUNKS = 8
EOF
}

function killall_rockstar {
    printf "Kill all currently running instances of rockstar.\n"
    user=whoami
    PID=$(ps -u $user -o pid,args | grep 'rockstar' | awk '{print $1}')
    kill $PID
}


function rockstar_n_out {
    N_SNAPS_ROCKSTAR=$(ls -1 ${SIM_OUT_DIR} | wc -l)
    # skip the first output
    printf "N_SNAPS_ROCKSTAR=${N_SNAPS_ROCKSTAR}\n"
}

function calc_force_res {
    MAX_RES=$(awk -v  a="${LEVELMAX}" 'BEGIN {print 2^a}')
    FORCE_RES=$(awk -v  a="${LBOX}" -v b="${MAX_RES}" 'BEGIN {print a / b }')
    printf "FORCE_RES=${FORCE_RES}\n"
}

printf "run-rockstar: Detect haloes in the N-body simulation\n"
if [ ! -d ${SIM_OUT_DIR} ]; then
    ## The makefile dependence on 'run-simulation' should prevent this
    ## situation from being reached, but it cannot hurt to have more checks
    ## rather than fewer.
    printf "Can't find a valid simulation, please make sure that \'project make run-simulation\' was executed before"
    exit 1
fi

## You can use this to kill orphan rockstar processes, but if you
## have independent installations of 'elaphrocentre' on the same
## machine, that are currently being run by the same user, then all
## the rockstar processes will be killed.
# killall_rockstar

## An alternative is to kill off the pid's listed in
## $(HALO_DIR)/rockstar_pid_maybe_active.

calc_force_res
rockstar_n_out
cd ${HALO_DIR}

write_rockstar_input 1.e10 ${N_SNAPS_ROCKSTAR} ${SIM_OUT_DIR} ${STARTING_SNAP} ${FORCE_RES} ${FOF_LINKING_LENGTH} ${FOF_FRACTION} ${MIN_HALO_OUTPUT_SIZE} ${MIN_HALO_PARTICLES} ${MASS_DEFINITION} > halos.cfg

while (!([ -e halos.cfg ])); do
    printf "Waiting for 'halos.cfg' to be created...\n"
    sleep 1;
done

printf "Initialise server for rockstar\n"
./rockstar -c halos.cfg &
sleep 2;

while (!([ -e auto-rockstar.cfg ])); do
    printf "Waiting for 'auto-rockstar.cfg' to be created...\n"
    sleep 1;
done

printf "Start up rockstar clients.\n"
./rockstar -c auto-rockstar.cfg

LAST_OUT=$((${N_SNAPS_ROCKSTAR} - 1))
if [ ! -f halos_${LAST_OUT}.0.ascii ]; then
    echo Something went wrong while searching for haloes.
    echo ABORT
    touch haloes-failed
    exit 1
fi
