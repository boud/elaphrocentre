# Detect voids in a simulation.
#
# (C) 2020 Boud Roukema, Marius Peper, GPL-3+
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

## Include the initial conditions parameters:
void_paramsfile = $(pconfdir)/detect-voids-params.conf
#sim_paramsfile = $(pconfdir)/init-conditions-params.conf
#include ${void_paramsfile}
#include ${sim_paramsfile}

voids_dir = $(badir)/voids
#init_dir = $(badir)/n-body-init
example_void_output_file = $(voids_dir)/outputpotcentres/zobov-Voids_potC_cat.txt
#revolver_namelist_output_file = $(voids_dir)/parameters/params.py
revolver_namelist_output_file = parameters/params.py
done_detect_voids_file = $(voids_dir)/done-detect-voids
void_elaphro_rad_precision = .1f

#aliases
.PHONY : revolver
revolver: detect-voids

# This is the main rule.  The `run-simulations` rule has to be run independently
# we don't want to accidentally override an existing N-body simulation.
.PHONY : detect-voids
detect-voids: $(done_run_simulation_file) $(example_void_output_file) $(mtexdir)/detect-voids.tex

### 	OLD: && cp -pvdr $(bsdir)/installed/Revolver $(voids_dir)
REVOLVER_PATH = $(ilibpydir)/Revolver
PYTHONPATH_NEW = $(REVOLVER_PATH):$(PYTHONPATH)

$(done_detect_voids_file): $(example_void_output_file)

$(example_void_output_file): $(done_run_simulation_file) $(void_paramsfile) $(sim_paramsfile)
	cp -pv $(bashdir)/run-revolver $(ibdir)
	mkdir -p $(voids_dir)
	cd $(voids_dir)
	rm -fv voids-failed
	for void_elaphro_rad in $(rad_frac_elaphro); do
	   void_rad=$$(printf "%$(void_elaphro_rad_precision)" $${void_elaphro_rad})
	   mkdir -p elaphro_rad_$${void_rad}
	   cd elaphro_rad_$${void_rad}
	   rm -fv Revolver_Ids.txt
	   N_CPUS_DEFAULT=$(NCPUS) \
	    SIM_OUT_DIR=$(sim_out_dir) LBOX=$(Lbox) \
	    VOID_DIR=$(voids_dir) N_CROOT=$(Ncroot) \
	    MIN_DENS_CUT=$(min_dens_cut) \
	    RAD_FRAC_CIRC=$(rad_frac_circ) \
	    RHO_EXCLUDE_WALLS=$(rho_exclude_walls) \
	    REVOLVER_PATH=$(REVOLVER_PATH) \
	    PYTHONPATH=$(PYTHONPATH_NEW) \
	    OUTPUT_CIRC=$(output_rad_fraction_circumcentre) \
	    OUTPUT_ELAPHRO=$(output_rad_fraction_elaphrocentre) \
	    OUTPUT_EXCLUDE_WALLS=$(output_rho_exclude_walls) \
	    RAD_FRAC_ELAPHRO=$${void_rad} \
	    run-revolver
	   cd ..
	   done
	rm -fv outputpotcentres
	ln -sv elaphro_rad_$${void_rad}/outputpotcentres/ .
	if ! ([ -e voids-failed ]) ; then
	   touch $(done_detect_voids_file)
	   printf "See $(example_void_output_file)\n"
	   printf "\nRevolver was successfully run. See the new files in $(voids_dir)/\n\n"
	else
	   rm $(done_detect_voids_file)
	   printf "Failed run of revolver.\n"
	   exit 1
	fi

$(mtexdir)/detect-voids.tex: $(done_detect_voids_file) $(void_paramsfile) $(sim_paramsfile)
        # This section produces a file with LaTeX macros for some of
        # the parameters in $(revolver_namelist_output_file). It presently
        # selects a small hardwired list of parameters rather than all
        # of them.  This section will be re-run if $(revolver_namelist_output_file) is
        # updated.
	printf "%% Automatically produced file.\n\n"
	printf "%% Automatically produced file.\n\n" > $@
	param_list_floatp2="box_length"
	param_list_floatp1="rad_frac_elaphro"
	param_list_int="nbins"
	for void_elaphro_rad in $(rad_frac_elaphro); do
	   void_rad=$$(printf "%$(void_elaphro_rad_precision)" $${void_elaphro_rad})
	   add=i$${add}
	   $(call create_tex_macro, $(voids_dir)/elaphro_rad_$${void_rad}/$(revolver_namelist_output_file), $${param_list_floatp2}, float, revolverparam$${add})
	   $(call create_tex_macro, $(voids_dir)/elaphro_rad_$${void_rad}/$(revolver_namelist_output_file), $${param_list_floatp1}, float.1, revolverparam$${add})
	   $(call create_tex_macro, $(voids_dir)/elaphro_rad_$${void_rad}/$(revolver_namelist_output_file), $${param_list_int}, int, revolverparam$${add})
	   done
	printf "\\\\newcommand{\\\\HowWeDetectedVoids}{This is how we detected voids.}\n\n" >> $@
	printf "\nThe LaTeX macro file $@ was created.\n\n"

# TODO: does clean the LaTeX macro belong into here?
clean-voids:
	rm -fv $(example_void_output_file)
	rm -fv $(voids_dir)/outputrawZOBOV/*
	rm -fr $(voids_dir)/outputpotcentres # without 'v' because too many files
	rm -fv $(done_detect_voids_file)
