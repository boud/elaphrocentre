# Analyse the simulation and create plots.
#
# (C) 2020 Boud Roukema, Marius Peper, GPL-3+
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

ana_paramsfile = $(pconfdir)/analyse-plot-params.conf
#sim_paramsfile = $(pconfdir)/init-conditions-params.conf
#void_paramsfile = $(pconfdir)/Void-finder-params.conf
#include ${void_paramsfile}
#include ${ana_paramsfile}
#include ${sim_paramsfile}

#include ${paramsfile} # included in initialize.mk

#ibdir = $(bsdir)/installed/bin
#haloes_dir = $(badir)/haloes
gal_dir_out = $(badir)/galaxies/sage_out
#voids_dir = $(badir)/voids
plot_dir = $(badir)/plots
fig_dir = $(texbdir)/fig
N_haloes_output_file = N_haloes.dat
Nvoids_plot_output_file = $(plot_dir)/Nvoids.dat
size_plot_output_file = galaxy_size.dat
elaphro_acc_output_file = elaphro-acc.dat
Infallrate_amplitude_output_file = infall-rate-amplitude-voids_non-voids.dat
Infallrate_decay_rate_output_file = infall-rate-decay-voids_non-voids.dat
Infallrate_void_stat_output_file = exp_fit_params_statistics_voids.dat
Infallrate_non_void_stat_output_file = exp_fit_params_statistics_non-voids.dat
Infallrate_voidgals_props_output_file = voidgals_props_TS.dat
Acceleration_voidgals_props_output_file = voidgals_acc_TS.dat
done_analyse_plot_file = $(plot_dir)/done-analyse-plot

py_library_files = $(pythondir)/exptest.py \
        $(pythondir)/extra_calculations.py \
        $(pythondir)/infall_stat.py \
        $(pythondir)/theil_sen_mod.py \
        $(pythondir)/theil_sen_2param_mod.py \
        $(pythondir)/plot_voidgals_properties.py

py_analyse_executables = $(pythondir)/test_move_coord.py \
        $(pythondir)/allresults_LSBGs.py \
        $(pythondir)/plot_centres_voidstat.py \
        $(pythondir)/plot_void_galaxies.py \

output_eps_files = number_counts \
	void_population \
	infall_frac_amp \
	infall_frac_amp_nofit \
	infall_frac_tau \
	infall_rreff_amp \
	infall_rreff_amp_nofit \
	infall_rreff_tau \
	galaxy_size_frac \
	galaxy_size_frac_nofit \
	galaxy_size_rreff \
	galaxy_Rvir_frac \
	galaxy_Rvir_rreff \
	galaxy_spin_frac \
	galaxy_spin_rreff \
        frac_rreff \
        frac_rreff_nofit \
        accel_rad \
        accel_tan \
        accel_tan_nofit \
        HaloMassFunction

#One table is probably enough.
output_table_dat = $(data-publish-dir)/voidgals_infall.dat

output_table_dat_basename = $(subst $(data-publish-dir)/,,$(output_table_dat))




REVOLVER_PATH = $(ilibpydir)/Revolver
LD_LIBRARY_PATH_PYTHON = $(REVOLVER_PATH)/python_tools:$(LD_LIBRARY_PATH)
IO_GADGET_WITH_PATH = $(REVOLVER_PATH)/python_tools/io_gadget.so

.PHONY : analyse-plot
analyse-plot: $(done_analyse_plot_file) $(mtexdir)/analyse-plot.tex

$(output_tables_dat): $(done_analyse_plot_file)

.PHONY : analyse-plot-tex
analyse-plot-tex: $(mtexdir)/analyse-plot.tex

$(done_analyse_plot_file): $(Nvoids_plot_output_file)


## Making plots requires that the full pipeline going through into the two sides of
## the fork:
## * galaxies made by gal-recipes.mk (create-galaxies); and
## * voids detected by detect-voids.mk (detect-voids).
## The plots can, in principle, run even if the simulations are missing.
$(Nvoids_plot_output_file): \
              $(done_create_galaxies_file) \
              $(done_detect_voids_file) \
	      $(void_paramsfile) \
	       | $(data-publish-dir)
	python --version
	cp -pv $(bashdir)/run-plots $(ibdir)
	cp -pv $(py_library_files) $(ilibpydir)
	cp -pv $(py_analyse_executables) $(ibdir)
	mkdir -p $(plot_dir)
	cd $(plot_dir)
	ls -l $(ibdir)/plot_centres_voidstat.py
	ls -l $(ibdir)/allresults_LSBGs.py
	rm -fv $(Nvoids_plot_output_file)
	mkdir -p $(fig_dir)
	for void_elaphro_rad in $(rad_frac_elaphro); do
	   void_rad=$$(printf "%$(void_elaphro_rad_precision)" $${void_elaphro_rad})
	   mkdir -p elaphro_rad_$${void_rad}
	   mkdir -p elaphro_rad_$${void_rad}/circcentre
	   mkdir -p elaphro_rad_$${void_rad}/geomcentre
	   rm -fv elaphro_rad_$${void_rad}/$$(basename $(Nvoids_plot_output_file))
	   cd elaphro_rad_$${void_rad}
	   GAL_OUT_DIR=$(gal_dir_out) HALO_DIR=$(haloes_dir) \
	    LBOX=$(Lbox) HUBBLE0=$(Hubble) SIM_OUT_DIR=$(sim_out_dir) \
	    TREEFILE=$(mergertree_output_file) \
	    VOID_DIR=$(voids_dir)/elaphro_rad_$${void_rad} \
	    PYTHONPATH=$(PYTHONPATH) \
	    LD_LIBRARY_PATH_PYTHON=$(LD_LIBRARY_PATH_PYTHON) \
	    IO_GADGET_WITH_PATH=$(IO_GADGET_WITH_PATH) \
	    PRINT_PNG_FILES=$(print_png_files) \
	    EL_ORIG_RADIUS=$(elaphrocentre_halo_original_radius_Mpc) \
	    NCROOT=$(Ncroot) \
	    MASSGAP_LOWER_LIMIT=$(massgap_lower_limit) \
	    MASSGAP_UPPER_LIMIT=$(massgap_upper_limit) \
	    HALO_IS_IN_VOID_MIN_FRACTION=$(halo_is_in_void_min_fraction) \
	    INNER_RAD_THRESHOLD=$(inner_rad_threshold) \
	    OUTER_RAD_THRESHOLD=$(outer_rad_threshold) \
	    RAD_FRAC=$${void_rad} \
	    SPLIT_POSITION_IN_VOID=$(split_position_in_void) \
	    CENTRE=$(centre) \
	    COMPARE_CENTRES=$(strip $(compare_centres)) \
	    NVOID_ANALYSE=$(nvoid_analyse) \
	    METADATA_CREATOR=$(package-name) \
	    run-plots
	   for fig in $(output_eps_files); do
	     cp -pv $${fig}.eps $(fig_dir)/$${fig}$${void_rad}.eps
	     done
	   cd ..
	   done
	ln -sv elaphro_rad_$${void_rad}/$$(basename $(Nvoids_plot_output_file)) .
	$(call print-copyright, $(output_table_dat), new)
	cat $(plot_dir)/elaphro_rad_$${void_rad}/$(output_table_dat_basename) >> \
	     $(output_table_dat)
	touch $(done_analyse_plot_file)
	printf "The plots were run successfully. See $(plot_dir) .\n"

$(mtexdir)/analyse-plot.tex: $(ana_paramsfile) $(sim_paramsfile) $(done_analyse_plot_file)
        # This section produces a file with LaTeX macros for some of
        # the parameters in $(size_plot_output_file). It presently
        # selects a BIG hardwired list of parameters rather than all
        # of them.  This section will be re-run if $(size_plot_output_file) is
        # updated. TODO: modularise this!
	printf "%% Automatically produced file.\n\n"
	printf "%% Automatically produced file.\n\n" > $@
	param_list_N_haloes="N_haloes_final_time"
	param_list_size_float="(|error\-)r_avg\-(|non)voids"
	param_list_spin_f4="(|error\-)spin_avg\-(|non)voids"
	param_list_rvir_f1="(|error\-)rvir_avg\-(|non)voids"
	param_list_size_int="N_gals N_void-gals N_non-void-gals "
	param_list_acc="(avg|error)_[xyz]_(dens|geom|pot|circ)([xyz])\3|acc_(rad|tan)_"
	param_list_acc_int="CountedVoids_(dens|geom|pot|circ)"
	param_list_infall_amp="(((Expectation|Median)Value)|(Standard|Robust)(Deviation|Error)Amplitude(|Non)Voids|MedianAmplitudeD)"
	param_list_infall_ampf1="MedianAmplitudeSignif"
	param_list_infall_ampf0="MedianAmplitudeVoids"
	param_list_infall_dec="((Expectation|Median)Value)|(Standard|Robust)(Deviation|Error)Decay(|Non)Voids"
	param_list_fit_exclusions_int="GoodParameter BadParameter"
	param_list_fit_exclusions_float="ParameterThresholdInput"
	param_list_halo_mass_f1="halo_mass"
	param_list_formation_time_f1="formation_time"
	param_list_paramsfile_exp="massgap"
	param_list_paramsfile_float="elaphrocentre_halo_original_radius_Mpc halo_is_in_void_min_fraction inner_rad_threshold outer_rad_threshold rad_frac"
	param_list_voidgals_props_float="(infall_(amp|tau)|gal(size))_(|twopar_)(frac|reldist|Rvir)"
	param_list_voidgals_props_float_f0="(gal(Rvir))_(frac|reldist|Rvir)_[a-z_]*zero|(gal(Rvir))_(frac|Rvir)_[a-z_]*slope"
	param_list_voidgals_props_float_f1="(gal(Rvir))_(reldist)_[a-z_]*slope"
	param_list_voidgals_props_float_f3="((gal(spin))_(frac|reldist|Rvir))|frac_rreff"
	param_list_voidgals_circ_geom_float="galsize_reldist"
	param_list_voidgals_circ_geom_float_f0="galRvir_reldist[a-z_]*zero"
	param_list_voidgals_circ_geom_float_f1="galRvir_reldist[a-z_]*slope"
	param_list_voidgals_circ_geom_float_f3="galspin_reldist"
	param_list_voidaccel_props_float_f3="acc_(rad|tan)_"
	param_list_paramsfile_str="centre_adjective"
	for void_elaphro_rad in $(strip $(rad_frac_elaphro)); do
	   void_rad=$$(printf "%$(void_elaphro_rad_precision)" $${void_elaphro_rad})
	   add=i$${add}
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/$(N_haloes_output_file), $${param_list_N_haloes}, int, )
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/$(size_plot_output_file), $${param_list_size_float}, float, PlotsSize$${add})
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/galaxy_spin.dat, $${param_list_spin_f4}, float.4, Spin$${add})
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/galaxy_rvir.dat, $${param_list_rvir_f1}, float.1, Rvir$${add})
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/$(size_plot_output_file), $${param_list_size_int}, int, PlotsSize$${add})
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/$(elaphro_acc_output_file), $${param_list_acc}, float, ElaphroAcc$${add})
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/$(elaphro_acc_output_file), $${param_list_acc_int}, int, ElaphroAcc$${add})
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/$$(basename $(Nvoids_plot_output_file)), Nvoids, int, nvoids$${add})
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/$(Infallrate_amplitude_output_file), $${param_list_infall_amp}, float, Infall)
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/$(Infallrate_amplitude_output_file), $${param_list_infall_ampf1}, float.1, Infall)
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/$(Infallrate_amplitude_output_file), $${param_list_infall_ampf0}, float.0, Infall)
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/$(Infallrate_decay_rate_output_file), $${param_list_infall_dec}, float, Infall)
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/$(Infallrate_void_stat_output_file), $${param_list_fit_exclusions_int}, int, VoidFit$${add})
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/$(Infallrate_void_stat_output_file), $${param_list_fit_exclusions_float}, float.0, VoidFit$${add})
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/$(Infallrate_non_void_stat_output_file), $${param_list_fit_exclusions_int}, int, NonVoidFit$${add})
	   $(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/$(Infallrate_non_void_stat_output_file), $${param_list_fit_exclusions_float}, float.0, NonVoidFit$${add})
	   $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_props_float_f0}, float.0)
	   $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_props_float_f1}, float.1)
	   $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_props_float_f3}, float.3)
	   $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_props_float}, float)
	   $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_circ_geom_float}, float, Elaphcen)
	   $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_circ_geom_float_f0}, float.0, Elaphcen)
	   $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_circ_geom_float_f1}, float.1, Elaphcen)
	   $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_circ_geom_float_f3}, float.3, Elaphcen)
	   if [ x"$(strip $(compare_centres))" = xYES ]; then
	     $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/circcentre/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_circ_geom_float}, float, Circcen)
	     $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/circcentre/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_circ_geom_float_f0}, float.0, Circcen)
	     $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/circcentre/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_circ_geom_float_f1}, float.1, Circcen)
	     $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/circcentre/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_circ_geom_float_f3}, float.3, Circcen)
	     $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/geomcentre/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_circ_geom_float}, float, Geomcen)
	     $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/geomcentre/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_circ_geom_float_f0}, float.0, Geomcen)
	     $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/geomcentre/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_circ_geom_float_f1}, float.1, Geomcen)
	     $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/geomcentre/$(Infallrate_voidgals_props_output_file), $${param_list_voidgals_circ_geom_float_f3}, float.3, Geomcen)
	   fi
	   $(call create_tex_macro, $(plot_dir)//elaphro_rad_$${void_rad}/$(Acceleration_voidgals_props_output_file), $${param_list_voidaccel_props_float_f3}, float.3)
	   done
	$(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/halo_masses.dat, $${param_list_halo_mass_f1}, float.1)
	$(call create_tex_macro, $(plot_dir)/elaphro_rad_$${void_rad}/formation_time.dat, $${param_list_formation_time_f1}, float.1)
	$(call create_tex_macro, $(ana_paramsfile), $${param_list_paramsfile_exp}, exp)
	$(call create_tex_macro, $(ana_paramsfile), $${param_list_paramsfile_float}, float)
	$(call create_tex_macro, $(ana_paramsfile), $${param_list_paramsfile_str}, str)


clean-analyse-plot:
	rm -frv $(badir)/plots
	rm -fv $(done_analyse_plot_file) $(Nvoids_plot_output_file)
	rm -fv $(data-publish-dir)/*


###full-calculations: $(example_sims_output_file) $(done_run_simulation_file) $(done_detect_haloes_file)

## Run all three independent parts of the calculations.
##
## The simulations part needs $(example_sims_output_file) to be given
## explicitly, because that is not run automatically by later steps in the
## pipeline. The $(done_run_simulation_file) target will wait until
## the simulations have been run before being completed, unless there
## is a timeout.

.PHONY : full-calculations
full-calculations: $(example_sims_output_file) $(done_run_simulation_file) \
              $(done_create_galaxies_file) \
              $(done_detect_voids_file)

## Run the calculations and make the plots.
.PHONY : full-pipeline
full-pipeline: $(example_sims_output_file) $(done_run_simulation_file) \
              $(done_create_galaxies_file) \
              $(done_detect_voids_file) \
              $(done_analyse_plot_file)
