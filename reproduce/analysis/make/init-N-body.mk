# Initialise the simulations.
#
# (C) 2020 Boud Roukema, Marius Peper, GPL-3+
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

## TODO: remove these - apparently they are set by initialize.mk
## mtexdir     = $(texdir)/macros
## pconfdir = reproduce/analysis/config

## Set up macros from parameter values.

ibdir = $(bsdir)/installed/bin
init_dir = $(badir)/n-body-init
sim_paramsfile = $(pconfdir)/init-conditions-params.conf
example_initcond_output_file = $(init_dir)/ic_velcz
done_init_conditions_file = $(init_dir)/done-init-conditions

#include ${sim_paramsfile} # included earlier by top-make.mk

# aliases
.PHONY : mpgrafic
mpgrafic: init-conditions
.PHONY : init-cond
init-cond: init-conditions

# This is the main rule. It says that if initial conditions are
# wanted, then at least one of the mpgrafic output files has to exist
# and a TeX file with macros with the input parameter values has to
# exist. A simple sanity check (TODO: that should later be moved to verify.mk!)
# is also required to be run.
.PHONY : init-conditions
init-conditions: $(example_initcond_output_file) $(mtexdir)/init-N-body.tex $(init_dir)/mpgrafic-sanity-check

# Before running the sanity check, first make sure that the file has been created
$(init_dir)/mpgrafic-sanity-check: $(example_initcond_output_file)
	byte_count=$$(wc -c $(example_initcond_output_file)|awk '{print $$1}')
	expected_byte_count=$$(echo "52" |awk -v N=$(Ncroot) '{print $$1+8*N+4*N*N*N}')
	if [ "$${byte_count}" -eq "$${expected_byte_count}" ]; then
	   printf "$(example_initcond_output_file) has the expected byte count.\n"
	   touch $(init_dir)/mpgrafic-sanity-check
	else
	   printf "Warning: $(example_initcond_output_file) is $${byte_count} bytes but $${expected_byte_count} was expected.\n"
	   rm -f $(init_dir)/mpgrafic-sanity-check
	   exit 1
	fi

$(done_init_conditions_file): $(example_initcond_output_file)

$(example_initcond_output_file): $(sim_paramsfile)
        # This section runs mpgrafic using the parameters from
        # $(sim_paramsfile). It will be re-run if $(sim_paramsfile) is
        # updated.
	printf "This is init-N-body.mk starting from "
	/bin/pwd && printf "\n"
	echo $(init_dir)
	echo $(ibdir)
	mkdir -pv $(init_dir)
	cp -pv $(bashdir)/run-mpgrafic $(installdir)/bin/
	cd $(init_dir)
	echo "PATH=$${PATH}"
	run-mpgrafic $(Lbox) $(Ncroot) $(NCPUS) $(Seed) $(Hubble) $(OmegaM) $(OmegaL)
	cd $(badir)
	touch $(done_init_conditions_file)
	printf "\nMpgrafic output files are to be found in $(init_dir).\n\n"

$(mtexdir)/init-N-body.tex: $(sim_paramsfile) $(done_init_conditions_file)
        # This section produces a file with LaTeX macros for the
        # parameters from $(sim_paramsfile). It will be re-run if
        # $(sim_paramsfile) is updated.
	printf "%% Automatically produced file.\n\n"
	printf "%% Automatically produced file.\n\n" > $@
	param_list_float="OmegaM OmegaL Hubble"
	param_list_int="Ncroot NCPUS Seed Lbox time_start time_end time_step"
	$(call create_tex_macro, $(sim_paramsfile), $${param_list_float}, float.1)
	$(call create_tex_macro, $(sim_paramsfile), $${param_list_int}, int)
	printf "\nThe LaTeX macro file $(mtexdir)/init-N-body.tex was created.\n\n"

clean-init-conditions:
	rm -f $(example_initcond_output_file) $(init_dir)/mpgrafic-sanity-check \
	  $(done_init_conditions_file)
