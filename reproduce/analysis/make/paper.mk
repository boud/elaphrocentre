# Build the final PDF paper/report.
#
# Copyright (C) 2018-2021 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Makefile.  If not, see <http://www.gnu.org/licenses/>.




# LaTeX macros for paper
# ----------------------
#
# To report the input settings and results, the final report's PDF (final
# target of this project) uses macros generated from various steps of the
# project. All these macros are defined through `$(mtexdir)/project.tex'.
#
# `$(mtexdir)/project.tex' is actually just a combination of separate files
# that keep the LaTeX macros related to each workhorse Makefile (in
# `reproduce/src/make/*.mk'). Those individual macros are pre-requisites to
# `$(mtexdir)/verify.tex' which will check them before starting to build
# the paper. The only workhorse Makefile that doesn't need to produce LaTeX
# macros is this Makefile (`reproduce/src/make/paper.mk').
#
# This file is thus the interface between the analysis/processing steps and
# the final PDF: when we get to this point, all the processing has been
# completed.
#
# Note that if you don't want the final PDF and just want the processing
# and file outputs, you can give any value other than 'yes' to
# 'pdf-build-final' in `reproduce/analysis/config/pdf-build.conf'.
$(mtexdir)/project.tex: $(mtexdir)/verify.tex

        # If no PDF is requested, or if LaTeX isn't available, don't
        # continue to building the final PDF. Otherwise, merge all the TeX
        # macros into one for building the PDF.
	@if [ -f .local/bin/latex ] && [ x"$(pdf-build-final)" != x ]; then

          # Put a LaTeX input command for all the necessary macro files.
          # 'hardware-parameters.tex' is created in 'configure.sh'.
	  projecttex=$(mtexdir)/project.tex
	  rm -f $$projecttex
	  for t in $(subst paper,,$(makesrc)) hardware-parameters; do
	    echo "\input{tex/build/macros/$$t.tex}" >> $$projecttex
	  done

	  # Highlight recent text changes if explicitly requested
	  if [ "x$(highlightnew)" = x1 ]; then
	    printf "\\\\toggletrue{postrefereeB}   %% highlight postreferee changes\n" >> $(mtexdir)/project.tex
	  else
	    printf "\\\\togglefalse{postrefereeB}   %% ordinary text for postreferee changes\n" >> $(mtexdir)/project.tex
	  fi

	  # Remove some fits in figure plots if explicitly requested
	  if [ "x$(removesomefits)" = x1 ]; then
	    printf "\\\\toggletrue{removesomefits}   %% remove some fits as requested by the referee\n" >> $(mtexdir)/project.tex
	  else
	    printf "\\\\togglefalse{removesomefits}   %% include all fits to avoid selection bias\n" >> $(mtexdir)/project.tex
	  fi

          # Possibly print results for all three centre definitions
	  if [ x"$(strip $(compare_centres))" = xYES ]; then
	    echo "\toggletrue{CompareCentres}" >> $(mtexdir)/project.tex
	  else
	    echo "\togglefalse{CompareCentres}" >> $(mtexdir)/project.tex
	  fi
	else
	  echo
	  echo "-----"
	  echo "The processing has COMPLETED SUCCESSFULLY! But the final "
	  echo "LaTeX-built PDF paper will not be built."
	  echo
	  if [ x$(more-on-building-pdf) = x1 ]; then
	    echo "To build the PDF, make sure that the 'pdf-build-final' "
	    echo "variable has a value of 'yes' (it is defined in this file)"
	    echo "    reproduce/analysis/config/pdf-build.conf"
	    echo
	    echo "If you still see this message, there was a problem with "
	    echo "building LaTeX within the project. You can re-try building"
	    echo "it when you have internet access with the two commands below:"
	    echo "    $ rm .local/version-info/tex/texlive*"
	    echo "    $./project configure -e"
	  else
	    echo "For more, run './project make more-on-building-pdf=1'"
	  fi
	  echo
	  echo "" > $@
	fi





# The bibliography
# ----------------
#
# We need to run the `bibtex' program on the output of LaTeX to generate the
# necessary bibliography before making the final paper. So we'll first have
# one run of LaTeX (similar to the `paper.pdf' recipe), then `bibtex'.
#
# `$(mtexdir)/project.tex' is an order-only-prerequisite for
# `paper.bbl'. TODO: We need to see whether or not an update in project.tex
# should automatically require an update of `paper.bbl'.

# The full `paper.tex' file is needed as a prerequisite here, because if it
# has been modified, then the list of references used (or not used), which
# is stored in `paper.bbl', needs to be updated. Similarly, a change in
# `tex/src/references.tex' requires the bibliography to be re-built.
$(texbdir)/paper.bbl: tex/src/references.tex $(mtexdir)/dependencies-bib.tex \
                      paper.tex \
                      | $(mtexdir)/project.tex
        # If `$(mtexdir)/project.tex' is empty, don't build PDF.
	@macros=$$(cat $(mtexdir)/project.tex)
	if [ x"$$macros" != x ]; then
	  # We'll run LaTeX first to generate the `.bcf' file (necessary
	  # for `biber') and then run `biber' to generate the `.bbl' file.
	  p=$$(pwd)
	  export TEXINPUTS=$$p:
	  export BIBINPUTS=$$p:
	  cp -v tex/src/mnras_openaccess.bst $(texbdir)/
	  ## Do some tidying to handle the MNRAS .bst file:
	  cat tex/src/references.tex $(mtexdir)/dependencies-bib.tex \
	    > ${texbdir}/references_tmp.bib
	  $(bashdir)/capitalise_de.sh $(texbdir)/references
	  cd $(texbdir)
	  latex $$p/paper.tex
	  bibtex paper
	fi





# The final paper
# ---------------
#
# Run LaTeX in the `$(texbdir)' directory so all the intermediate and
# auxiliary files stay there and keep the top directory clean. To be able
# to run everything cleanly from there, it is necessary to add the current
# directory (top project directory) to the `TEXINPUTS' environment
# variable.
#
# To satisfy ArXiv standards of long-term sustainability, any figure
# files in the package should be postscript (optionally with text elements
# stored in corresponding LaTeX files). As of early 2020, this requires
# the latex + dvips + ps2pdf pipeline.
paper.pdf: $(mtexdir)/project.tex paper.tex $(texbdir)/paper.bbl

        # If `$(mtexdir)/project.tex' is empty, don't build the PDF.
	@macros=$$(cat $(mtexdir)/project.tex)
	if [ x"$$macros" != x ]; then

          # Go into the top TeX build directory and make the paper.
	  p=$$(pwd)
	  export TEXINPUTS=$$p:
	  cd $(texbdir)
	  #lualatex -shell-escape -halt-on-error $$p/paper.tex
	  printf "Check typesetting program availability:\n" ;
	  which latex; which dvips; which ps2pdf
	  latex $$p/paper.tex && latex $$p/paper.tex && latex $$p/paper.tex
	  dvips paper -o paper-tmp.eps
	  ps2pdf paper-tmp.eps $@

          # Come back to the top project directory and copy the built PDF
          # file here.
	  cd "$$p"
	  cp $(texbdir)/$@ $(final-paper)

	fi

elaphrocentre.pdf: paper-full.pdf
	cp -pv paper-full.pdf elaphrocentre.pdf


# Combined bbl-free .tex file for the paper
# ---------------
#
# To avoid requiring journal editors to run bibtex, biblatex or biber,
# include the bbl source in a combined .tex file.
#
# TODO: This also has to create a tarball with the postscript figure files
# for convenience for uploading to ArXiv or journals.
paper-full.pdf: $(texbdir)/paper-full.tex
	p=$$(pwd)
	cd $(texbdir)
	latex paper-full.tex && latex paper-full.tex && latex paper-full.tex
	#This rule is hacked for one paper; it will need to be made more general.
	eps_files=$$(latex paper-full.tex |grep -o "<[^ >]*eps>" |sed -e 's/[<>]//g'|tr '\n' ' ')
	cls_file=mnras.cls
	cp -pv $$(find $(installdir) -name $${cls_file}) .
	tar -cv -f $(project-package-name)-journal.tar.gz paper-full.tex $${eps_files} $${cls_file}
	#
	dvips paper-full -o paper-tmp.eps
	ps2pdf paper-tmp.eps $@
        # Come back to the top project directory and copy the built paper-full
        # file here.
	cd $$p
	cp -pv $(texbdir)/$@ .
	cp -pv $(texbdir)/$(project-package-name)-journal.tar.gz .


$(texbdir)/paper-full.tex: $(mtexdir)/project.tex paper.tex $(texbdir)/paper.bbl

        # If `$(mtexdir)/project.tex' is empty, don't build the bbl-free file.
	@macros=$$(cat $(mtexdir)/project.tex)
	if [ x"$$macros" != x ]; then
	  printf "\nGenerating paper-full.tex ...\n"

	  # Go into the top TeX build directory and make the paper.
	  #
	  # Any URL with a single % UTF-8 encoded character has the % escaped with
	  # a backslash for LaTeX usage. TODO: This sed action should probably
	  # be modularised for better maintenance if it is to be used beyond
	  # astronomy research papers.
	  p=$$(pwd)
	  export TEXINPUTS=$$p:
	  cd $(texbdir)
          # Replace any URL-encoded strings such as %26 (but not \%26) by
          # \%26 to protect them for LaTeX. This is especially needed for A&A URLs,
          # such as http://cdsads.u-strasbg.fr/abs/2011A%26A...533A..11R.
	  sed -e 's/\({http[^ \]*\)\(%\)/\1\\%/g' paper.bbl > paper.bbl.bak
	  latexpand --expand-bbl paper.bbl.bak $$p/paper.tex | \
	    sed -e 's/\\bibliographystyle[^ }]*}//' > paper-full.tex
	  rm -f paper-full-de.tex
	  $$p/$(bashdir)/uncapitalise_De.sh paper-full
	  mv -v paper-full-de.tex paper-full.tex

          # Come back to the top project directory and copy the built paper-full
          # file here.
	  #cd $$p
	  #cp -v $(texbdir)/$@ .
	fi


clean-paper:
        # Remove intermediate LaTeX output files that can cause errors
        # if they represent out-dated information:
	rm -f $(texbdir)/*.aux $(texbdir)/*.bbl $(texbdir)/*.blg
        # Remove LaTeX macros that are automatically extracted from
        # galaxy formation pipeline configuration files. We first
        # add the `extglob` option to bash, so that `!` can be used
        # to exclude the `dependencies*` files, which are generated
        # during the configure step when installing software. Normally,
        # `!` has a different special meaning in bash.
	shopt -s extglob
	rm -f $(badir)/tex/macros/!(hardware-*|dependencies.tex|dependencies-bib.tex)

PHONY: clean-paper
