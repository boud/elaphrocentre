# Create a merger tree history.
#
# (C) 2020 Boud Roukema, Marius Peper, GPL-3+
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

## Include the initial conditions parameters:
#include reproduce/analysis/config/Nbody-sim-params.conf

#ibdir = $(bsdir)/installed/bin
#haloes_dir = $(badir)/haloes
#tree_dir = $(badir)/mergertrees
#init_dir = $(badir)/n-body-init
## example output files
#mergertree_output_file = $(tree_dir)/trees/tree_0_0_0.dat
converter_output_file = $(tree_dir)/converter_out/lhalotree.bin.0
done_ctrees_file = $(tree_dir)/done-create-ctrees
done_create_mergertree_file = $(tree_dir)/done-create-mergertree

# aliases
.PHONY : ctrees
ctrees: create-mergertree

# This is the main rule.

.PHONY : create-mergertree
create-mergertree: $(done_detect_haloes_file) $(example_haloes_output_file) $(done_ctrees_file) $(converter_output_file)

$(done_create_mergertree_file): $(converter_output_file)

$(done_ctrees_file): $(mergertree_output_file)

$(mergertree_output_file): $(done_detect_haloes_file) $(example_haloes_output_file)
        # TODO: the gen_merger_cfg.pl (rockstar) and
        # do_merger_tree_np.pl (ctrees) scripts should be able to find
        # the four executables `gravitational_consistency_no_periodic`
        # ... without needing them to be symbolically linked; sym
        # linking is a temporary hack.
	rm -frv $(tree_dir)/hlists
	rm -frv $(tree_dir)/outputs
	rm -frv $(tree_dir)/converter_out
	rm -frv $(tree_dir)/trees
	mkdir -p $(tree_dir)
	if ! [ -e $(tree_dir)/run-ctrees ]; then \
	   ln -s $$(/bin/pwd)/$(bashdir)/run-ctrees $(tree_dir)/; fi
	cd $(tree_dir)
	rm -fv ctrees-failed
	for binary in \
	   gravitational_consistency_no_periodic \
	   find_parents_and_cleanup_no_periodic \
	   resort_outputs \
	   assemble_halo_trees; do
	   if ! [ -e ./$${binary} ]; then
	      ln -s $(ibdir)/$${binary} ./ ; fi ;
	      done
	/bin/pwd
	BIN_DIR=$(ibdir) HALO_DIR=$(haloes_dir) \
	 TREE_DIR=$(tree_dir) \
	 ./run-ctrees
	cd $(tree_dir)
	rm -fr trees hlists outputs
	ln -sv $(haloes_dir)/trees ./
	ln -sv $(haloes_dir)/hlists ./
	ln -sv $(haloes_dir)/outputs ./
	if ! ([ -e ctrees-failed ]) ; then
	   touch $(done_ctrees_file)
	   printf "Ctrees (consistent-trees) was successfully run. "
	   printf "See new files in $(haloes_dir)/ .\n"
	else
	   rm $(done_ctrees_file)
	   printf "Failed run of ctrees.\n"
	   exit 1
	fi

# If mergertree_output_file has changed, then the converter should be run again.
$(converter_output_file): $(mergertree_output_file)
	cp -pv $(bashdir)/run-converter $(ibdir)
	cd $(tree_dir)
	rm -fv converter-failed
	rm -fr converter_out
	mkdir converter_out
	cp -pv $(ibdir)/convert_trees_to_lhalo .
	HALO_DIR=$(haloes_dir) \
	 run-converter
	if ! ([ -e converter-failed ]) ; then
	   touch $(done_create_mergertree_file)
	   printf "ConvertCTrees was successfully run. "
	   printf "See new files in $(haloes_dir)/ .\n"
	else
	   rm $(done_create_mergertree_file)
	   printf "Failed run of ConvertCTrees.\n"
	   exit 1
	fi

#TODO: I left this open because neither Ctrees not ConvertCTrees are using any input parameters
$(mtexdir)/create-trees.tex:
	printf "%% Automatically produced file.\n" >> $@
	printf "\\\\newcommand{\\\\HowWeCreatedTrees}{This is how we created merger history trees.}\n\n" >> $@

clean-mergertree:
	rm -fv $(mergertree_output_file) $(converter_output_file) $(done_create_mergertree_file)
	rm -frv $(badir)/mergertrees
