#!/usr/bin/env python

##    Copyright (C) 2018, 2019 Boud Roukema
##    Copyright (C) 2020 Marius Peper, Boud Roukema
##
##    This program is free software; you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation; either version 3, or (at your option)
##    any later version.
##
##    This program is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with this program; if not, write to the Free Software Foundation,
##    Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
##
##    See also http://www.gnu.org/licenses/gpl.html

import numpy as np
from setuptools import version
if (version.pkg_resources.parse_version(np.__version__) >= version.pkg_resources.parse_version("1.17.0")):
    np_modern = True
    # as recommended at https://numpy.org/devdocs/reference/random/index.html
    from numpy.random import default_rng
else:
    np_modern = False

import matplotlib
## The 'Agg' backend might help with thread safety:
##  https://matplotlib.org/3.2.1/faq/howto_faq.html
matplotlib.use('Agg')
import matplotlib.pyplot as plt

##    lowdensity_pairs - select visually similar subset of overdense point distribution
##
## DESCRIPTION: For input arrays x_in, y_in, return a subset that
## avoids plotting visually redundant points that a human viewer will
## not be able to distinguish from one another.

## INPUTS:
## x_in, y_in = input X, Y arrays
## x1, x2 = X limits
## y1, y2 = Y limits
## N_uniform = number of points to plot if distribution is statistically
## uniform; fewer than N_uniform points will almost always be selected for any
## non-grid-like distribution.
## N_xy = resolution in X and Y directions, copied to N_x = N_y
##
## OUTPUTS:
## x_out, y_out = output X, Y arrays that are a subset of x_in, y_in
## N_out = number of output points.

class calc:

    def lowdensity_pairs(self,x_in, y_in, x1, x2, y1, y2, N_uniform, N_xy):
        ## Resolution for density analysis; squares of size
        ## (x2-x1)*(y2-y1)/(N_x*N_y) are analysed.
        N_x = N_xy
        N_y = N_xy

        # Check that the input arrays are not multi-dimensional
        if(len(x_in.shape) > 1 or len(y_in.shape) > 1):
            raise ValueError("x_in or y_in must be an one-dimensional array.")

        ## Set up internal xx, yy as numpy row vectors and store info on how
        ## to output the results.
        xx = np.array(x_in)
        yy = np.array(y_in)
        N_in = len(xx)

        ## Set up ix, iy indices.
        ix = ( np.floor( (N_x * (xx - x1)/(x2-x1)) ) ).astype(int)
        iy = ( np.floor( (N_y * (yy - y1)/(y2-y1)) ) ).astype(int)
        #print("np.max(ix), np.max(iy) = ", np.max(ix), np.max(iy))

        ## Set up selection function: by default all points are un-selected.
        good = np.zeros(N_in).astype(int)
        good_0 = np.zeros(N_in).astype(int)
        good_1 = np.ones(N_in).astype(int)

        if(np_modern):
            rng = default_rng()

        ## Nested (slow) 'for' loop: select full sets at low density, and
        ## smaller subsets at high density.
        N_crit = np.maximum(N_uniform/(N_x*N_y),1)
        for jx in range(0,N_x):
            for jy in range (0,N_y):
                # Set up a selection function (1 or 0) for the plottable
                # points within the jx-th, jy-th small box.
                i_select_all = ((jx == ix) * (jy == iy))
                N_found = np.count_nonzero(i_select_all)
                if(N_found <= N_crit):
                    # Set a value of 1 for each input point in this low density region:
                    good_lowdensity = np.where(i_select_all, good_1, good_0)
                    good = good + good_lowdensity
                else:
                    # We want to select from the high density region with
                    # the following probability:
                    prob_threshold = N_crit/float(max(N_found,1))
                    if(np_modern):
                        prob_test = rng.random(N_in)
                    else:
                        prob_test = np.random.random_sample(N_in)
                    good_highdensity = np.where((i_select_all * (prob_test < prob_threshold)), good_1, good_0)
                    good = good + good_highdensity

        ## return the selection
        x_out = xx[np.nonzero(good)]
        y_out = yy[np.nonzero(good)]

        N_out = len(x_out)

        return x_out, y_out, N_out


    def test_lowdensity_pairs(self):
        from matplotlib import rc
        rc('text', usetex=True)

        N_in = 10000
        N_uniform = 100000
        N_xy = 200
        verbose = True

        single_plot = False

        x1 = -4.0
        x2 = 4.0
        y1 = -4.0
        y2 = 4.0

        marker_size = 3

        ## initialise a big 2D data set with low density and high density regions
        if(np_modern):
            rng = default_rng()
            x_in = rng.standard_normal(N_in)
            y_in = x_in + 0.1 * rng.standard_normal(N_in);
        else:
            x_in = np.random.randn(N_in)
            y_in = x_in + 0.1 * np.random.randn(N_in);

        ## select low density subsample
        x_out, y_out, N_out = self.lowdensity_pairs(x_in, y_in, x1, x2, y1, y2, N_uniform, N_xy);

        ## plot of full data
        if(single_plot):
            ax1 = plt.subplot(1, 2, 1)
        else:
            # 2020-05-18 A known usability bug in matplotlib requires the
            # use of subplots() if you wish to close the file and not
            # create uselessly big figure files, e.g.
            # https://stackoverflow.com/questions/9622163/save-plot-to-image-file-instead-of-displaying-it-using-matplotlib
            fig1, ax1 = plt.subplots(1, 1)
        plt.scatter(x_in,y_in, color='blue', s=marker_size)
        plt.axis([x1, x2, y1, y2])
        plt.title("original")
        if(not(single_plot)):
            plt.savefig("lowdensity_orig.eps")
            plt.close(fig1)
        if(verbose):
            print("N_in = ",N_in)

        ## plot the low density subsample
        #plot(x_out,y_out,'.', 'markersize',3,'color',[0.7 0.0 0.3])
        if(single_plot):
            ax2=plt.subplot(1, 2, 2)
        else:
            fig2, ax2 = plt.subplots(1, 1)
        plt.axis([x1, x2, y1, y2])
        plt.scatter(x_out,y_out, color='blue', s=marker_size)
        plt.title("reduced")
        if(not(single_plot)):
            plt.savefig("lowdensity_lowd.eps")
            plt.close
        else:
            plt.savefig("lowdensity.eps")
        if(verbose):
            print("N_out = ",N_out)


if __name__ == '__main__':

    calc=calc()
    calc.test_lowdensity_pairs()
